<?
/**
* @package S2dio CMS http://s2dio.com.ua
* @copyright Авторские права (C) 2011 Stupak Oleg. Все права защищены.
* @license Лицензия http://www.gnu.org/copyleft/gpl.html GNU/GPL, смотрите LICENSE.php
* S2dio CMS! - свободное программное обеспечение. Эта версия может быть изменена
* в соответствии с Генеральной Общественной Лицензией GNU, поэтому возможно
* её дальнейшее распространение в составе результата работы, лицензированного
* согласно Генеральной Общественной Лицензией GNU или других лицензий свободных
* программ или программ с открытым исходным кодом.
* Для просмотра подробностей и замечаний об авторском праве, смотрите файл COPYRIGHT.php.
*/


// Конфигурация БД
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 's2dio.local';
 
// Шаблоны
$theme_backend = 's2tem';
$theme_frontend = 'lysogor';
 
// Отладачная информация
$debug = FALSE;


// >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> 

ob_start();

// Классы
require_once 'includes/lib/mysql.class.php';
require_once 'includes/lib/pages.class.php';
require_once 'includes/lib/photos.class.php';
require_once 'includes/lib/config.class.php';
require_once 'includes/lib/validate.class.php';
require_once 'includes/lib/images.class.php';
require_once 'includes/lib/function.php';
require_once 'includes/lib/smarty/Smarty.class.php';

  
// Запуск классов
$s2cms = new Smarty;
$Validate = new Validate();
$Images = new Images();

// Настройки
define('ADM', 1); // Если 1 то frontend
define('PATH', "/"); 
define('PREF', 's2cms_'); // Префикс таблиц
 
 
 // Админ. Шаблон
if(ADM == 1){
$theme = $theme_frontend;
}else{
$theme = $theme_backend;
}

$s2cms->assign("theme", $theme);	// Админ. Шаблон


// >>> Не трогать дальше >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> 

// Подключение к БД
$db = new sql_db($dbhost, $dbuser, $dbpass, $dbname, false);
header('Content-type: text/html; charset=utf-8');
mysql_query("set names utf8 collate utf8_general_ci");
if(!$db->db_connect_id) {
	die("<center>Подключение к серверу базы данных не доступно</center>");
}




// Настройки системы
$act = validate($_GET['act'], 60);
$page = validate($_GET['page'], 60);
$mod = validate($_GET['mod'], 60);
$url= validate($_GET['url'], 60);
//$id_item= validate($_GET['id_item'], 60);
  
 

$Config = new Config();
$Config = $Config->ConfigView();
$row = $db->sql_fetchrow($Config);
$s2cms->assign("title_no", $row['title']);
$s2cms->assign("keywords_no", $row['keywords']);
$s2cms->assign("description_no", $row['description']);
$s2cms->compile_dir = 'templates/templates_c/';
$work_site = $row['work_site']; // 1 - on, 0 - off
$to_email = $row['email']; // E-mail администратора
$s2cms->assign("close_text", $row['close_text']);


// Постраничная навигация
$num_page = $row['page']; // Количество вывода елементов
if(empty($page)) $page = 1;
$begin = ($page - 1)*$num_page;
 
 
//------------ Мультиязычность
$lg = "ru";
include 'includes/lib/languages/'.$lg.'/lang.php';
include 'includes/lib/languages/lang.php';
$s2cms->assign("lg", $lg);
 

// Алерты
$done_add = "Успешно добавлено!";
$done_edit = "Успешно сохранено!";
$done_delete = "Успешно удалено!";
$done_delete_cat = "Нельзя удалить раздел содержащий записи!";

ob_end_clean();
?>