<?
/**
* @package S2dio CMS http://s2dio.com.ua
* @copyright Авторские права (C) 2011 Stupak Oleg. Все права защищены.
* @license Лицензия http://www.gnu.org/copyleft/gpl.html GNU/GPL, смотрите LICENSE.php
* S2dio CMS! - свободное программное обеспечение. Эта версия может быть изменена
* в соответствии с Генеральной Общественной Лицензией GNU, поэтому возможно
* её дальнейшее распространение в составе результата работы, лицензированного
* согласно Генеральной Общественной Лицензией GNU или других лицензий свободных
* программ или программ с открытым исходным кодом.
* Для просмотра подробностей и замечаний об авторском праве, смотрите файл COPYRIGHT.php.
*/
define('SECURITY', true);
session_start();

// Засекаем время для вычисления отладочной информации
$time_start = microtime(true);


include 'config.php';
if ($work_site == "0") { $s2cms->display($theme."/close_site.tpl"); exit(); } // Включает/выключает сайт
if($_GET['act'] == "error") {$s2cms->display($theme."/eror_404.tpl"); exit();} // Ошибка 404
$s2cms->template_dir = 'templates/'.$theme; // Папка с шаблоном



if(isset($_GET['mod'])) {

  	$mod = validate($_GET['mod'], 60);
	// Засекаем время для вычисления времени выполнения
	$start = microtime(true);
	switch($mod) {        
        case $mod: include 'modules/'.$mod.'/view.php'; break;
	}
	$end = microtime(true);
	// ====================================================
	 
} else {
       
		$Pages = new Pages(); 
	 
	    // Засекаем время для вычисления времени выполнения запроса к БД 
	    $start = microtime(true);
	    $Pages = $Pages->ViewIndex();
        $end = microtime(true);
        // ====================================================
		
		$row = $db->sql_fetchrow($Pages); 
			 
        $s2cms->assign("pageIndex", $row['page']);
		$s2cms->assign("id", $row['id_item']);
		$s2cms->assign("name", $row['name']);
		$s2cms->assign("url", $row['url']);
		$s2cms->assign("title", $row['meta_title']);
		$s2cms->assign("keywords", $row['meta_keywords']);
		$s2cms->assign("description", $row['meta_description']);
	
		$s2cms->assign("text", $row['text']);
		$s2cms->assign("page", "pages/view.tpl");
		$s2cms->display("main.tpl");

}

// Отладочная информация

if($debug == true)
{
    print "<!--\r\n";
	$i = 0;
    $sql_time = 0;
 
	$time_end = microtime(true);
	$exec_time = $time_end-$time_start;
    $sql_time = $end-$start;
	
      
  	if(function_exists('memory_get_peak_usage'))
	print "использование памяти: ".memory_get_peak_usage()." bytes\r\n";  
	print "время генерации страницы: ".$exec_time." seconds\r\n";  
	print "SQL запросы и загрузки модулей: ".$sql_time." seconds\r\n";  
	print "PHP время генерации: ".($exec_time-$sql_time)." seconds\r\n";  
	print "-->";
 
}
 
  
?>