<?
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 


/**
 * склонение числительных
 * @param string $num   число  
 * @param string $stem    OCHOBA  
 * @param string  $e1, $e2, $e3    окончания разных форм  
*/

function word ($num /* число */ , $stem /* OCHOBA */, $e1, $e2, $e3 /* окончания разных форм */)
{
/* проверка на третий тип */
if (
/* проверяем предпоследнюю цифру */
((intval($num / 10) % 10) == 1) ||
/* и последнюю */

(($num % 10) >= 5)
) return $stem . $e3;

/* проверка на первый тип */
if (($num % 10) == 1) return $stem . $e1;

/* проверка на ноль */
if (($num % 10) == 0) return $stem . $e3;

/* остается только второй тип, можно не проверять */
return $stem . $e2;
}
		

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Обрезать строку до заданной длины
 * @param string $lenght
 * @param string $str
 * @return html
 */
    function cutStr($str, $lenght = 250) {
        $str = strip_tags($str, '<a>');
			$splitted = preg_split('/(<.+?>)/', $str, null, PREG_SPLIT_DELIM_CAPTURE+PREG_SPLIT_NO_EMPTY);
			$splittedCount = count($splitted);

			for($i = 0; $i < $splittedCount; $i ++) {
				if ($splitted [$i] [0] == '<') {
					continue;
				}
				$splitted [$i] = wordwrap($splitted [$i], $lenght, '~');
			}
			$result = implode('', $splitted); 
			
			if(strpos($result, "~")>0) {
				$str_cut = substr($result, 0, strpos($result, "~"));
				$str_cut .= ' ...';
				return $str_cut;
			}
            else return $result;
    } 
 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Построчная навигация
 **/
function page($page, $num_page, $s2cms, $table, $where = 0) {
	$sql = mysql_query("SELECT COUNT(*) FROM $table WHERE  $where");
	$num = mysql_result($sql, 0);
	$number = (int)($num/$num_page);
	if((float)($num/$num_page) - $number != 0) $number++;

		for($i = 1; $i < $page; $i++) {

           $nav_page[] = array("i" => $i, "page" => $page);

		}

		for($i = $page; $i <= $number; $i++) {

            $nav_page2[] = array("i" => $i, "page" => $page);

		}

$s2cms->assign("nav_page", $nav_page);
$s2cms->assign("nav_page2", $nav_page2);
 

}

 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Отправка почты
 **/
function SendMail($to_email, $email, $content) {

mail($to_email, "Сообщение с сайта ".$_SERVER['SERVER_NAME']."", $content, "MIME-Version: 1.0\n".
	"Content-type: text/html; charset=utf-8\n".
	"From: ".$email."\n".
	"X-Mailer: PHP/".phpversion());

}

 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Дата
 **/
function date_view($date) {

	$date = getdate(strtotime($date));
	$month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
	return $date['mday'].' '.$month[$date['mon']-1].' '.$date['year'];

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Валидация
 **/
function validate($info, $length = 0) {
	$info = trim($info);
	$info = htmlspecialchars($info, ENT_QUOTES);
	$info = mysql_real_escape_string($info);
	if($length != 0) {
		$info = substr($info, 0, $length);
	}
    return $info;

}

 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Транслит и формирование ссылки
 **/
function translit($content) {

	$transA = array('А' => 'a', 'Б' => 'b', 'В' => 'v', 'Г' => 'h', 'Ґ' => 'g', 'Д' => 'd', 'Е' => 'e', 'Ё' => 'jo', 'Є' => 'e', 'Ж' => 'zh', 'З' => 'z', 'И' => 'i', 'І' => 'i', 'Й' => 'i', 'Ї' => 'i', 'К' => 'k', 'Л' => 'l', 'М' => 'm', 'Н' => 'n', 'О' => 'o', 'П' => 'p', 'Р' => 'r', 'С' => 's', 'Т' => 't', 'У' => 'u', 'Ў' => 'u', 'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c', 'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'sz', 'Ъ' => '', 'Ы' => 'y', 'Ь' => '', 'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya');
	$transB = array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'є' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'і' => 'i', 'й' => 'i', 'ї' => 'i', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sz', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '&quot;' => '', '&amp;' => '', 'µ' => 'u', '№' => '');
	$content = trim(strip_tags($content));
	$content = strtr($content, $transA);
	$content = strtr($content, $transB);
	/*$content = preg_replace("/\s+/ms", "_", $content);
	$content = preg_replace("/[ ]+/", "_", $content);
	$content = preg_replace("/[^a-z0-9_]+/mi", "", $content);*/
	$content = preg_replace("/\s+/ms", "-", $content);
	$content = preg_replace("/[ ]+/", "-", $content);
	$content = preg_replace("/[^a-z0-9-]+/mi", "", $content);
	$content = stripslashes($content);
	return $content;

}

 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * формирование URL 
**/
function url($url) {
	$url = $url.".html";
	return $url;
}


 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Массовое удаление
 **/
function delete($type) {
	
 
      if(!empty($type))
      {
     // Начинаем формировать переменную, содержащую этот список
     // в формате "(3,5,6,7)"
     $query = "(" ;
     foreach($type as $val) $query.= "$val,";
     // Удаляем последнюю запятую, заменяя ее закрывающей скобкой)
     $query = substr($query, 0, strlen($query) - 1 ). ")" ;
     
     }
	return $query;
}



 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Определение браузера
 */
function is_mobile_browser(){

	$user_agent = $_SERVER['HTTP_USER_AGENT']; 
	$http_accept = $_SERVER['HTTP_ACCEPT'];
	
	if(stristr($user_agent, 'windows') && !stristr($user_agent, 'windows ce'))
		return false;
	
	if(eregi('windows ce|iemobile|mobile|symbian|mini|wap|pda|psp|up.browser|up.link|mmp|midp|phone|pocket', $user_agent))
		return true;

	if(stristr($http_accept, 'text/vnd.wap.wml') || stristr($http_accept, 'application/vnd.wap.xhtml+xml'))
		return true;
		
	if(!empty($_SERVER['HTTP_X_WAP_PROFILE']) || !empty($_SERVER['HTTP_PROFILE']) || !empty($_SERVER['X-OperaMini-Features']) || !empty($_SERVER['UA-pixels']))
		return true;

	$agents = array(
	'acs-'=>'acs-',
	'alav'=>'alav',
	'alca'=>'alca',
	'amoi'=>'amoi',
	'audi'=>'audi',
	'aste'=>'aste',
	'avan'=>'avan',
	'benq'=>'benq',
	'bird'=>'bird',
	'blac'=>'blac',
	'blaz'=>'blaz',
	'brew'=>'brew',
	'cell'=>'cell',
	'cldc'=>'cldc',
	'cmd-'=>'cmd-',
	'dang'=>'dang',
	'doco'=>'doco',
	'eric'=>'eric',
	'hipt'=>'hipt',
	'inno'=>'inno',
	'ipaq'=>'ipaq',
	'java'=>'java',
	'jigs'=>'jigs',
	'kddi'=>'kddi',
	'keji'=>'keji',
	'leno'=>'leno',
	'lg-c'=>'lg-c',
	'lg-d'=>'lg-d',
	'lg-g'=>'lg-g',
	'lge-'=>'lge-',
	'maui'=>'maui',
	'maxo'=>'maxo',
	'midp'=>'midp',
	'mits'=>'mits',
	'mmef'=>'mmef',
	'mobi'=>'mobi',
	'mot-'=>'mot-',
	'moto'=>'moto',
	'mwbp'=>'mwbp',
	'nec-'=>'nec-',
	'newt'=>'newt',
	'noki'=>'noki',
	'opwv'=>'opwv',
	'palm'=>'palm',
	'pana'=>'pana',
	'pant'=>'pant',
	'pdxg'=>'pdxg',
	'phil'=>'phil',
	'play'=>'play',
	'pluc'=>'pluc',
	'port'=>'port',
	'prox'=>'prox',
	'qtek'=>'qtek',
	'qwap'=>'qwap',
	'sage'=>'sage',
	'sams'=>'sams',
	'sany'=>'sany',
	'sch-'=>'sch-',
	'sec-'=>'sec-',
	'send'=>'send',
	'seri'=>'seri',
	'sgh-'=>'sgh-',
	'shar'=>'shar',
	'sie-'=>'sie-',
	'siem'=>'siem',
	'smal'=>'smal',
	'smar'=>'smar',
	'sony'=>'sony',
	'sph-'=>'sph-',
	'symb'=>'symb',
	't-mo'=>'t-mo',
	'teli'=>'teli',
	'tim-'=>'tim-',
	'tosh'=>'tosh',
	'treo'=>'treo',
	'tsm-'=>'tsm-',
	'upg1'=>'upg1',
	'upsi'=>'upsi',
	'vk-v'=>'vk-v',
	'voda'=>'voda',
	'wap-'=>'wap-',
	'wapa'=>'wapa',
	'wapi'=>'wapi',
	'wapp'=>'wapp',
	'wapr'=>'wapr',
	'webc'=>'webc',
	'winw'=>'winw',
	'winw'=>'winw',
	'xda-'=>'xda-'
	);
	
	if(!empty($agents[substr($_SERVER['HTTP_USER_AGENT'], 0, 4)]))
    	return true;
}



 
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Проверяет наличие переменной $var во входных параметрах
     * @param string $var
     * @param string $type = int | str | html
     * @param string $default
    $inCore->request('category_id', 'html', ''); //Получаем данные 
     */
   function request($var, $type='str', $default=false){
        if (isset($_REQUEST[$var])){
            switch($type){
                case 'int':   return (int)$_REQUEST[$var]; break;
                case 'str':   if ($_REQUEST[$var]) { return $this->strClear($_REQUEST[$var]); } else { return $default; } break;
                case 'email': if(preg_match("/^([a-zA-Z0-9\._-]+)@([a-zA-Z0-9\._-]+)\.([a-zA-Z]{2,4})$/i", $_REQUEST[$var])){ return $_REQUEST[$var]; } else { return $default; } break;
                case 'html':  if ($_REQUEST[$var]) { return $this->strClear($_REQUEST[$var], false); } else { return $default; } break;
                case 'array': if (is_array($_REQUEST[$var])) { return $_REQUEST[$var]; } else { return $default; } break;
                case 'array_int': if (is_array($_REQUEST[$var])) { foreach($_REQUEST[$var] as $k=>$i){ $arr[$k] = (int)$i; } return $arr; } else { return $default; } break;
                case 'array_str': if (is_array($_REQUEST[$var])) { foreach($_REQUEST[$var] as $k=>$s){ $arr[$k] = $this->strClear($s); } return $arr; } else { return $default; } break;
            }
        } else {
            return $default;
        }
    }
	
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Разбивает текст на слова и делает каждое слово ссылкой, добавляя в его начало $link
 * @param string $link
 * @param string $text
 * @return html
 */
 function getMetaSearchLink($link, $text){
    $html='';
    $text = strip_tags($text);
    $text = trim($text);
    if (!strstr($text, ',')){
        $html .= '<a href="'.$link.urlencode($text).'">'.$text.'</a>';
    } else {
        $text = str_replace(', ', ',', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#8217;', "'", $text);
        $text = str_replace('&quot;', '"', $text);
        $words = array();
        $words = explode(',', $text);

        $n=0;
        foreach($words as $key=>$value){
            $n++;
            $value = strip_tags($value);
            $value = str_replace("\r", '', $value);
            $value = str_replace("\n", '', $value);
            $value = trim($value, ' .');
            $html .= '<a href="'.$link.urlencode($value).'">'.$value.'</a>';
            if ($n<sizeof($words)) { $html .= ', '; } else { $html .= '.'; }
        }

    }

    return $html;
}

 
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Устанавливает кукис посетителю
     * @param string $name
     * @param string $value
     * @param int $time
     */
   function setallCookie($name, $value, $time){
        setcookie('S2dioCMS['.$name.']', $value, $time, '/');        
    }	
	

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Удаляет кукис пользователя
     * @param string $name
     */
    function unsetCookie($name){
        setcookie('S2dioCMS['.$name.']', '', time()-3600, '/');
    }

	
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Возвращает значение кукиса
     * @param string $name
     * @return string || false
     */
   function getCookie($name){
        if (isset($_COOKIE['S2dioCMS'][$name])){
            return $_COOKIE['S2dioCMS'][$name];
        } else {
            return false;
        }
    }

	
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Подключает внешний файл
     * @param string $lib
     */
   function includeFile($file){
        include_once PATH.$file;
    }
	
 
	
	
/*---------------------------------------------------------
Функция генерации рандомного числа относительно входящего
параметра
---------------------------------------------------------*/
function SRnd($t = "a0AA0"){
    $tar=array("NUMBER"=>array(48,57),"V_REG"=>array(65,90),"N_REG"=>array(97,122));
    for($i=0;$i<strlen($t);$i++){
        $char=ord(substr($t,$i,1));
        if($char>=$tar["NUMBER"][0] && $char<=$tar["NUMBER"][1]){
            $result.=chr(rand($tar["NUMBER"][0],$tar["NUMBER"][1]));
        }elseif($char>=$tar["V_REG"][0] && $char<=$tar["V_REG"][1]){
            $result.=chr(rand($tar["V_REG"][0],$tar["V_REG"][1]));
        }elseif($char>=$tar["N_REG"][0] && $char<=$tar["N_REG"][1]){
            $result.=chr(rand($tar["N_REG"][0],$tar["N_REG"][1]));
        }
    }
    return $result;
}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Удаляет теги script iframe style meta
     * @param string $string
     * @return bool
	 badTagClear($article['description'])
     */
    function badTagClear($string){

        $bad_tags = array (
            "'<script[^>]*?>.*?</script>'si",
            "'<style[^>]*?>.*?</style>'si",
            "'<meta[^>]*?>'si",
            '/<iframe.*?src=(?!"http:\/\/www\.youtube\.com\/embed\/|"http:\/\/vkontakte\.ru\/video_ext\.php\?).*?>.*?<\/iframe>/i',
            '/<iframe.*>.+<\/iframe>/i'
        );

        $string = preg_replace($bad_tags, '', $string);

        return $string;
        
    }
	
	
	
	
	
	
?>


 