<?
 
session_start();
$count=4;	/* РєРѕР»РёС‡РµСЃС‚РІРѕ СЃРёРјРІРѕР»РѕРІ */
$width=100; /* С€РёСЂРёРЅР° РєР°СЂС‚РёРЅРєРё */
$height=32; /* РІС‹СЃРѕС‚Р° РєР°СЂС‚РёРЅРєРё */
$font_size_min=22; /* РјРёРЅРёРјР°Р»СЊРЅР°СЏ РІС‹СЃРѕС‚Р° СЃРёРјРІРѕР»Р° */
$font_size_max=22; /* РјР°РєСЃРёРјР°Р»СЊРЅР°СЏ РІС‹СЃРѕС‚Р° СЃРёРјРІРѕР»Р° */
$font_file="./Comic_Sans_MS.ttf"; /* РїСѓС‚СЊ Рє С„Р°Р№Р»Сѓ */
$char_angle_min=-15; /* РјР°РєСЃРёРјР°Р»СЊРЅС‹Р№ РЅР°РєР»РѕРЅ СЃРёРјРІРѕР»Р° РІР»РµРІРѕ */
$char_angle_max=10;	/* РјР°РєСЃРёРјР°Р»СЊРЅС‹Р№ РЅР°РєР»РѕРЅ СЃРёРјРІРѕР»Р° РІРїСЂР°РІРѕ */
$char_angle_shadow=5;	/* СЂР°Р·РјРµСЂ С‚РµРЅРё */
$char_align=27;	/* РІС‹СЂР°РІРЅРёРІР°РЅРёРµ СЃРёРјРІРѕР»Р° РїРѕ-РІРµСЂС‚РёРєР°Р»Рё */
$start=7;	/* РїРѕР·РёС†РёСЏ РїРµСЂРІРѕРіРѕ СЃРёРјРІРѕР»Р° РїРѕ-РіРѕСЂРёР·РѕРЅС‚Р°Р»Рё */
$interval=16;	/* РёРЅС‚РµСЂРІР°Р» РјРµР¶РґСѓ РЅР°С‡Р°Р»Р°РјРё СЃРёРјРІРѕР»РѕРІ */
$chars="0123456789"; /* РЅР°Р±РѕСЂ СЃРёРјРІРѕР»РѕРІ */
$noise=10; /* СѓСЂРѕРІРµРЅСЊ С€СѓРјР° */

$image=imagecreatetruecolor($width, $height);

$background_color=imagecolorallocate($image, 255, 255, 255); /* rbg-С†РІРµС‚ С„РѕРЅР° */
$font_color=imagecolorallocate($image, 32, 64, 96); /* rbg-С†РІРµС‚ С‚РµРЅРё */

imagefill($image, 0, 0, $background_color);

$str="";

$num_chars=strlen($chars);
for ($i=0; $i<$count; $i++)
{
	$char=$chars[rand(0, $num_chars-1)];
	$font_size=rand($font_size_min, $font_size_max);
	$char_angle=rand($char_angle_min, $char_angle_max);
	imagettftext($image, $font_size, $char_angle, $start, $char_align, $font_color, $font_file, $char);
	imagettftext($image, $font_size, $char_angle+$char_angle_shadow*(rand(0, 1)*2-1), $start, $char_align, $background_color, $font_file, $char);
	$start+=$interval;
	$str.=$char;
}

if ($noise)
{
	for ($i=0; $i<$width; $i++)
	{
		for ($j=0; $j<$height; $j++)
		{
			$rgb=imagecolorat($image, $i, $j);
			$r=($rgb>>16) & 0xFF;
			$g=($rgb>>8) & 0xFF;
			$b=$rgb & 0xFF;
			$k=rand(-$noise, $noise);
			$rn=$r+255*$k/100;
			$gn=$g+255*$k/100;
			$bn=$b+255*$k/100;
			if ($rn<0) $rn=0;
			if ($gn<0) $gn=0;
			if ($bn<0) $bn=0;
			if ($rn>255) $rn=255;
			if ($gn>255) $gn=255;
			if ($bn>255) $bn=255;
			$color=imagecolorallocate($image, $rn, $gn, $bn);
			imagesetpixel($image, $i, $j , $color);
		}
	}
}

$_SESSION["captcha"]=$str;

if (function_exists("imagepng"))
{
	header("Content-type: image/png");
	imagepng($image);
}
elseif (function_exists("imagegif"))
{
	header("Content-type: image/gif");
	imagegif($image);
}
elseif (function_exists("imagejpeg"))
{
	header("Content-type: image/jpeg");
	imagejpeg($image);
}
imagedestroy($image);

?>
