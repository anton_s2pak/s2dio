<?
 
 

if(!defined("SECURITY")) die('Hacker? :)');

class Photos {

   public $table_cat = 'photos_cat';
   public $table = 'photos';

   public function __construct() {
	  global $db;
      $this->db = $db;
   }

   
  public function ViewPhotoLast() {
	  $SQL = "SELECT i.id_item, i.name, i.id_category, i.image, i.width, i.height, cat.name as name_cat, cat.url as url
				FROM ".PREF."".$this->table." i
				LEFT JOIN ".PREF."".$this->table_cat." cat ON i.id_category = cat.id_item WHERE i.id_category = 1 
				ORDER BY i.name DESC";
	  $rs = $this->db->sql_query($SQL);
	  return $rs;
   }
   
   public function ViewCat() {
	 /*  echo  $this->sort;*/
      $SQL = "SELECT * FROM ".PREF."".$this->table_cat." WHERE id_item != 1 AND id_item != 2 AND id_item != 3 ORDER BY `order` DESC";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }
   public function ViewAdminCat() {
	 /*  echo  $this->sort;*/
      $SQL = "SELECT * FROM ".PREF."".$this->table_cat." ORDER BY id_item ASC";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }
   
 #Выборка всех статей с постраничной навигацией и название категории
  public function ViewCatOne() {
        $SQL = "SELECT i.id_item, i.name, i.id_category, i.image, i.width, i.text, i.height, cat.name as name_cat, cat.url as url, cat.text as text_cat
				FROM ".PREF."".$this->table." i
				LEFT JOIN ".PREF."".$this->table_cat." cat ON cat.id_item = i.id_category  AND  cat.url = '".$this->url."'  WHERE i.id_item != 1 AND i.id_item != 2 AND i.id_item != 3 AND cat.url IS NOT NULL
			    ORDER BY i.name DESC";
	  $rs = $this->db->sql_query($SQL);
      return $rs;
   }  

 #Выборка фотографий для страницы контактов
  public function ViewFeedback() {
        $SQL = "SELECT i.id_item, i.name, i.id_category, i.image, i.width, i.height, cat.name as name_cat, cat.url as url, cat.text as text_cat
				FROM ".PREF."".$this->table." i
				LEFT JOIN ".PREF."".$this->table_cat." cat ON cat.id_item = i.id_category  WHERE i.id_category = 2
			    ORDER BY i.name DESC";
	  $rs = $this->db->sql_query($SQL);
      return $rs;
  }
  #Выборка фотографий для страницы контактов
  public function ViewInfo() {
        $SQL = "SELECT i.id_item, i.name, i.id_category, i.image, i.width, i.height, cat.name as name_cat, cat.url as url, cat.text as text_cat
				FROM ".PREF."".$this->table." i
				LEFT JOIN ".PREF."".$this->table_cat." cat ON cat.id_item = i.id_category  WHERE i.id_category = 3
			    ORDER BY i.name DESC";
	  $rs = $this->db->sql_query($SQL);
      return $rs;
  }

   public function View() {
      $this->db->sql_query("UPDATE ".PREF."".$this->table." SET views = views + 1 WHERE id_item = '".$this->id_item."'");
      $SQL = "SELECT id_item, id_category, name, image, text, com_on, meta_title, meta_keywords, meta_description, views, date FROM ".PREF."".$this->table." WHERE  id_item = '".$this->id_item."' ORDER BY name DESC LIMIT 1";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   public function ViewAdmin() {
      $SQL = "SELECT id_item, id_category, name, com_on, image FROM ".PREF."".$this->table." WHERE id_category = '".$this->id_category_get."' ORDER BY name DESC";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   public function Add() {
      $SQL = "INSERT ".PREF."".$this->table." SET    name = '".$this->name."', id_category = '".$this->id_category."', image = '".$this->image."', width = '".$this->width."', height = '".$this->height."', text = '".$this->text."', com_on = '".$this->com_on."', meta_title = '".$this->title."', meta_keywords = '".$this->keywords."', meta_description = '".$this->description."', date = '".$this->date."'";
      $rs = $this->db->sql_query($SQL);
   }

   public function ViewEdit() {
      $SQL = "SELECT id_item, id_category, name, text, image, com_on, meta_title, meta_keywords, meta_description  FROM ".PREF."".$this->table." WHERE   id_item = '".$this->id_item."' LIMIT 1";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   public function Edit() {
      $SQL = "UPDATE ".PREF."".$this->table." SET id_category = '".$this->id_category."', name = '".$this->name."', text = '".$this->text."', com_on = '".$this->com_on."', meta_title = '".$this->title."', meta_keywords = '".$this->keywords."', meta_description = '".$this->description."' WHERE id_item = '".$this->id_item."'";
      $rs = $this->db->sql_query($SQL);
   }

   public function EditImg() {
      $SQL = "UPDATE ".PREF."".$this->table." SET image = '".$this->image."' WHERE id_item = '".$this->id_item."'";
      $rs = $this->db->sql_query($SQL);
   }

   public function Del() {
      $SQL = "DELETE FROM ".PREF."".$this->table." WHERE id_item = ".$this->id_item." LIMIT 1";
      $rs = $this->db->sql_query($SQL);
   }

   public function CatAdd() {
      $SQL = "INSERT ".PREF."".$this->table_cat." SET    name = '".$this->name."', url = '".$this->url."', image = '".$this->image."', text = '".$this->text."', date = '".$this->date."'";
      $rs = $this->db->sql_query($SQL);
   }

   public function CatDel() {
      $SQL = "DELETE FROM ".PREF."".$this->table_cat." WHERE id_item = '".$this->id_item."' LIMIT 1";
      $rs = $this->db->sql_query($SQL);
   }

   public function CatViewEdit() {
      $SQL = "SELECT id_item, name, `order`, text, url, image FROM ".PREF."".$this->table_cat." WHERE    id_item = '".$this->id_item."' LIMIT 1";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   public function CatEdit() {
      $SQL = "UPDATE ".PREF."".$this->table_cat." SET name = '".$this->name."', url = '".$this->url."', `order` = '".$this->order."', text = '".$this->text."' WHERE id_item = '".$this->id_item."'";
      $rs = $this->db->sql_query($SQL);
   }

   public function CatEditImg() {
      $SQL = "UPDATE ".PREF."".$this->table_cat." SET image = '".$this->image."' WHERE id_item = '".$this->id_item."'";
      $rs = $this->db->sql_query($SQL);
   }

}

?>