<?


if(!defined("SECURITY")) die('Hacker? :)');

class sql_db {

	var $db_connect_id;
	var $query_result;
	var $row = array();
	var $rowset = array();
	var $num_queries = 0;

	function __construct($sqlserver, $sqluser, $sqlpassword, $database, $persistency = true) {

		$this->persistency = $persistency;
		$this->user = $sqluser;
		$this->password = $sqlpassword;
		$this->server = $sqlserver;
		$this->dbname = $database;

		if($this->persistency) {
			$this->db_connect_id = @mysql_pconnect($this->server, $this->user, $this->password);
		} else {
			$this->db_connect_id = @mysql_connect($this->server, $this->user, $this->password);
		}
		if($this->db_connect_id) {
			if($database != "") {
				$this->dbname = $database;
				$dbselect = @mysql_select_db($this->dbname);
				if(!$dbselect) {
					@mysql_close($this->db_connect_id);
					$this->db_connect_id = $dbselect;
				}
			}
			return $this->db_connect_id;
		} else {
			return false;
		}
	}

	function sql_close() {
		if($this->db_connect_id) {
			if($this->query_result)	{
				@mysql_free_result($this->query_result);
			}
			$result = @mysql_close($this->db_connect_id);
			return $result;
		} else {
			return false;
		}
	}

	function sql_query($query = "", $transaction = FALSE) {
		unset($this->query_result);
		if($query != "") {
			$this->num_queries++;
			$this->query_result = @mysql_query($query, $this->db_connect_id);
		}
		if($this->query_result) {
			unset($this->row[$this->query_result]);
			unset($this->rowset[$this->query_result]);
			return $this->query_result;
		} else {
			return ($transaction == END_TRANSACTION) ? true : false;
		}
	}

	function sql_numrows($query_id = 0) {
		if(!$query_id) {
			$query_id = $this->query_result;
		}
		if($query_id) {
			$result = @mysql_num_rows($query_id);
			return $result;
		} else {
			return false;
		}
	}

	function sql_fetchrow($query_id = 0, $f = 0) {
		if(!$query_id) {
			$query_id = $this->query_result;
		}
		if($query_id) {
			if (!$f){
				$this->row[$query_id] = @mysql_fetch_array($query_id);
			} else {
				$this->row[$query_id] = @mysql_fetch_row($query_id);
			}
			return $this->row[$query_id];
		} else {
			return false;
		}
	}

	function sql_fetchrowset($query_id = 0)	{
		if(!$query_id) {
			$query_id = $this->query_result;
		}
		if($query_id) {
			unset($this->rowset[$query_id]);
			unset($this->row[$query_id]);
			while($this->rowset[$query_id] = @mysql_fetch_array($query_id)) {
				$result[] = $this->rowset[$query_id];
			}
			return $result;
		} else {
			return false;
		}
	}

}

?>