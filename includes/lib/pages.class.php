<?
if(!defined("SECURITY")) die('Hacker? :)');

class Pages {

   public $table = 'pages';
   public $table_structure = 'structure'; 
   public $table_menu = 'menu'; 

   public function __construct() {
	  global $db;
      $this->db = $db;
   }

      //Показать все  варианты меню
    public function ViewCat() {
      $SQL = "SELECT id_menu, name FROM ".PREF."".$this->table_menu." ORDER BY id_menu ASC";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }
   
      //Добавить меню
   public function AddMenu() {
      $SQL = "INSERT ".PREF."".$this->table_menu." SET name = '".$this->namemenu."'";
      $rs = $this->db->sql_query($SQL);
 	  return $rs;
   }
   
   
     //Удалить  меню
   public function DelMenu() {
      $SQL = "DELETE FROM ".PREF."".$this->table_menu." WHERE id_menu = ".$this->id_menu." LIMIT 1";
      $rs = $this->db->sql_query($SQL);
   }
   
   
   //Показать пункты меню на сайте
   public function ViewStructure() {
      $SQL = "SELECT id_item, enabled, menu, parent, url, name, pos FROM ".PREF."".$this->table_structure." WHERE  enabled = '1' ORDER BY pos";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

 
  //Возврашает Заголовок - название меню 
    public function ViewHead() {
      $SQL = "SELECT * FROM ".PREF."".$this->table_menu." WHERE id_menu = '".$this->id_menu."'";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }
  
  
//Вывод деревовиднго меню нерграниченой вложености в Админке
    public function ViewAdminStr() {

      $SQL = "SELECT id_item, enabled, menu, parent, url, name, pos  FROM ".PREF."".$this->table_structure." WHERE  menu = '".$this->id_menu."' ORDER BY pos ASC";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   
 //Добавить пункт меню 
   public function AddStructure() {

      $MAX = $this->db->sql_fetchrow($this->db->sql_query("SELECT MAX(pos) + 1 FROM ".PREF."".$this->table_structure));

      $SQL = "INSERT ".PREF."".$this->table_structure." SET   menu = '".$this->id_menu."', name = '".$this->name."', url = '".$this->url."', enabled = '".$this->enabled."', parent = '".$this->id_str."', pos = '". $MAX[0] ."'";
      $rs = $this->db->sql_query($SQL);
 	  return $rs;
   }

   
  //Открыть пункт меню для редактирования
   public function ViewEditStructure() {
      $SQL = "SELECT id_item, url, parent, name, enabled FROM ".PREF."".$this->table_structure." WHERE   menu = '".$this->id_menu."' AND id_item = '".$this->id_item."' LIMIT 1";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   
   //Редактировать пункт  меню
   public function EditStructure() {
      $SQL = "UPDATE ".PREF."".$this->table_structure." SET name = '".$this->name."', url = '".$this->url."', enabled = '".$this->enabled."', parent = '".$this->id_str."' WHERE  id_item = '".$this->id_item."'";
      $rs = $this->db->sql_query($SQL);
   }

   
     //Удалить  пункт меню
   public function DelStructure() {
      $SQL = "DELETE FROM ".PREF."".$this->table_structure." WHERE id_item = ".$this->id_item." LIMIT 1";
      $rs = $this->db->sql_query($SQL);
   }
   

   //Показать главную страницу
   public function ViewIndex() {
     
      $SQL = "SELECT * FROM ".PREF."".$this->table." WHERE page = '1'  ORDER BY id_item DESC LIMIT 1";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }
   

    //Просмотр конкретной страницы
     public function View() {
	  $SQL = "SELECT  name, url, text, com_on, meta_title, meta_keywords, meta_description, page FROM ".PREF."".$this->table." WHERE   url = '".$this->url."'";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }


   //Просмотр всех страниц
   public function ViewAll() {
      $SQL = "SELECT id_item,  url, name, com_on FROM ".PREF."".$this->table."  ".$this->sort." ";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   //Добавить страницу
   public function Add() {
      $SQL = "INSERT ".PREF."".$this->table." SET   name = '".$this->name."', url = '".$this->url."', com_on = '".$this->com_on."', page = '".$this->page."',  text = '".$this->text."', meta_title = '".$this->title."', meta_keywords = '".$this->keywords."', meta_description = '".$this->description."'";
      $rs = $this->db->sql_query($SQL);
   }

   
   //Откорыть Редактирование страницы
   public function ViewEdit() {
      $SQL = "SELECT id_item,  url, name, text, com_on, page, meta_title, meta_keywords, meta_description FROM ".PREF."".$this->table." WHERE   id_item = '".$this->id_item."' LIMIT 1";
      $rs = $this->db->sql_query($SQL);
      return $rs;
   }

   //Обнавление отредактированой страницы
   public function Edit() {
      $SQL = "UPDATE ".PREF."".$this->table." SET name = '".$this->name."', url = '".$this->url."', text = '".$this->text."', page = '".$this->page."', com_on = '".$this->com_on."', meta_title = '".$this->title."', meta_keywords = '".$this->keywords."', meta_description = '".$this->description."' WHERE id_item = '".$this->id_item."'";
      $rs = $this->db->sql_query($SQL);
   }

   //Удаление страницы
   public function Del() {
      $SQL = "DELETE FROM ".PREF."".$this->table." WHERE id_item = ".$this->id_item." LIMIT 1";
      $rs = $this->db->sql_query($SQL);
   }
   
   
}

?>