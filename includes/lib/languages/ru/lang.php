<?
 

$lang=array(

//Page
'nav' => "Меню",

//News
'news' => "Новини",
'all_news' => "Всі новини",

//Articles
'articles' => "Статті",
'all_articles' => "Всі статті",
'views' => "Переглядів:",

//Faq
'faq' => "FAQ",

//Feedback
'feedback' => "Контактна інформація",
'priem'  => "Запис на прийом",
'feedback_name' => "Ваше ім&#146;я:",
'feedback_email' => "E-mail:",
'feedback_message' => "Повідомлення:",
'feedback_button' => "Відправити",
'feedback_send' => "Ваше повідомлення успішно відправлено!",

);

?>