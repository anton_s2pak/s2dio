<?
$s2cms->assign("nav", $lang['nav']);

//News
$s2cms->assign("news", $lang['news']);
$s2cms->assign("all_news", $lang['all_news']);

//Articles
$s2cms->assign("articles", $lang['articles']);
$s2cms->assign("all_articles", $lang['all_articles']);
$s2cms->assign("views", $lang['views']);

//Faq
$s2cms->assign("faq", $lang['faq']);

//Feedback
$s2cms->assign("feedback", $lang['feedback']);
$s2cms->assign("feedback_name", $lang['feedback_name']);
$s2cms->assign("feedback_email", $lang['feedback_email']);
$s2cms->assign("feedback_message", $lang['feedback_message']);
$s2cms->assign("feedback_button", $lang['feedback_button']);
$s2cms->assign("feedback_send", $lang['feedback_send']);

?>