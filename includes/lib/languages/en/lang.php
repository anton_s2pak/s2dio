<?
/*
============================================
 Lati Content Management System
--------------------------------------------
 Copyright (c) 2010 Tanasov Andrey
--------------------------------------------
 E-mail: la5erdance@yandex.ru
--------------------------------------------
 Данный код защищен авторскими правами
============================================
*/

$lang=array(

//Page
'nav' => "Menu",

//News
'news' => "News",
'all_news' => "All news",

//Articles
'articles' => "Articles",
'all_articles' => "All articles",
'views' => "Views:",

//Faq
'faq' => "FAQ",

//Feedback
'feedback' => "Feedback",
'feedback_name' => "Your name:",
'feedback_email' => "E-mail:",
'feedback_message' => "Message:",
'feedback_button' => "Send",
'feedback_send' => "Your message is successfully send!",

);

?>