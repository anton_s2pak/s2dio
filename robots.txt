User-agent: *

Disallow: /admin/
Disallow: /includes/
Disallow: /modules/

Sitemap: sitemap.xml.gz