<?
 $Photos = new Photos();
if ($act == "view_cat") {
	$Photos = $Photos->ViewCat();
	$num = 0;
	$i = 0;
	while($row = $db->sql_fetchrow($Photos)) {
		$num = $db->sql_numrows($db->sql_query("SELECT * FROM ".PREF."photos WHERE id_category = '".$row['id_item']."'"));
		$view_cat[] = "{ src: '/uploads/photos/big/". $row["image"] ."', thumb: '/uploads/photos/small/". $row["image"] ."' , width : ". $row["width"] .", height : ". $row["height"] .", prop : ". $row["width"]/$row["height"] .", id : 'slide-". $i++ ."'}";
	}
	$view_cat = '['.join(',', $view_cat_one).']';
    $s2cms->assign("title", "Фотогалерея");
    $s2cms->assign("view_cat", $view_cat);
	$s2cms->assign("page", "photos/view_cat.tpl");
 	$s2cms->display("main.tpl");

}



if ($act == "view_cat_one") {
    $Photos->url = validate($_GET['url'], 160);
	$Photos = $Photos->ViewCatOne();
    $num = $db->sql_numrows($Photos);
	$i = 0;

	while($row = $db->sql_fetchrow($Photos)) {
        $prop = $row["width"]/$row["height"];
        if (!is_numeric($prop)) continue;
		$view_cat_one[] = "{ src: '/uploads/photos/big/". $row["image"] ."', thumb: '/uploads/photos/small/". $row["image"] ."' , width : ". $row["width"] .", height : ". $row["height"] .", prop : ".$prop .", id : 'slide-". $i++ ."'}";
        $text = $row['text_cat'];
	}
	$view_cat_one = '['.join(',', $view_cat_one).']';
	page($page, $num_page, $s2cms, PREF."photos", "id_category = '".$id_category."'");
	$s2cms->assign("num", $num);
    $s2cms->assign("title", "Фотогалерея");
    $s2cms->assign("text", $text);
    $s2cms->assign("view_cat_one", $view_cat_one);
	$s2cms->assign("page", "photos/view_cat_one.tpl");
 	$s2cms->display("main.tpl");
}




if ($act == "view") { 
    $Photos->id_item = validate($_GET['id_item'], 60);
	$id_item = validate($_GET['id_item'], 60);
	$url = validate($_GET['url'], 160);
	
    $Photos = $Photos->View();
    $row = $db->sql_fetchrow($Photos);


	$sql_next = $db->sql_query("SELECT i.id_item, cat.url as url FROM ".PREF."photos i 
								LEFT JOIN ".PREF."photos_cat  cat ON cat.id_item = i.id_category  AND  cat.url = '".$url."' 
								WHERE i.id_item < '".$id_item."'  ORDER BY i.id_item DESC"); 
	$row_next = $db->sql_fetchrow($sql_next);
	 
				
	$sql_prev = $db->sql_query("SELECT i.id_item, cat.url as url FROM ".PREF."photos i 
								LEFT JOIN ".PREF."photos_cat  cat ON cat.id_item = i.id_category  AND  cat.url = '".$url."' 
								WHERE i.id_item > '".$id_item."'  ORDER BY i.id_item ASC");
	$row_prev = $db->sql_fetchrow($sql_prev);
	
	
	if($row_next['url'] && $row_next['id_item']){
	$nexturl = $row_next['url'].'/'.$row_next['id_item'].'.html';
	}
	if($row_prev['url'] && $row_prev['id_item']){
	$prevurl = $row_prev['url'].'/'.$row_prev['id_item'].'.html';
	}
	
 
    $s2cms->assign("next", $nexturl);
    $s2cms->assign("prev", $prevurl);
	
	$s2cms->assign("name", $row['name']);
	$s2cms->assign("text", $row['text']);
	$s2cms->assign("image", $row['image']);
    $s2cms->assign("views", $row['views']);
    $s2cms->assign("com_on", $row['com_on']);
	$s2cms->assign("date", date_view($row["date"]));
    $s2cms->assign("title", $row['meta_title']);
    $s2cms->assign("keywords", $row['meta_keywords']);
    $s2cms->assign("description", $row['meta_description']);
	$s2cms->assign("page", "photos/view.tpl");
 	$s2cms->display("main.tpl");

}

?>