<?
$Photos = new Photos();
require 'includes/lib/botobor.php';

$html = '<form action="" method="post" id="contact-form">
                    <div class="highlighter"><input type="text" id="name" name="name" class="input-text" value="Имя" /></div>
                    <div class="highlighter"><input type="text" id="email" name="email" class="input-text" value="E-mail" /></div>
                    <div class="highlighter"><input type="text" id="subject" name="subject" class="input-text" value="Тема сообщения" /></div>
                    <div class="highlighter"><textarea id="msg" name="msg">Текст сообщения</textarea></div>
                    <div class="buttons-set" style="padding: 10px 0;"><button style="max-width: 330px; width: 100%;" type="submit" class="button"><span><span>Отправить</span></span></button></div>
                </form>';
$bform = new Botobor_Form($html);
$bform->setLifetime(60);
$bform->setDelay (5);
$html = $bform->getCode();


if($act == "view") {
    $Photos->url = validate($_GET['url'], 160);
    $Photos = $Photos->ViewFeedback();
    $i = 0;
    while($row = $db->sql_fetchrow($Photos)) {
        $prop = $row["width"]/$row["height"];
        if (!is_numeric($prop)) continue;
        $view_feedback[] = "{ src: '/uploads/photos/big/". $row["image"] ."', thumb: '/uploads/photos/small/". $row["image"] ."' , width : ". $row["width"] .", height : ". $row["height"] .", prop : ". $prop .", id : 'slide-". $i++ ."'}";
        $text = $row["text_cat"];
    }
    $view_feedback = '['.join(',', $view_feedback).']';

	$s2cms->assign("title", "Обратная связь");
    $s2cms->assign("text", $text);
    $s2cms->assign("form", $html);
    $s2cms->assign("view_feedback", $view_feedback);
	$s2cms->assign("page", "feedback/view.tpl");
	$s2cms->display("main.tpl");

}

if($act == "send") {
    if (Botobor_Keeper::get()->isRobot())
    {
        echo json_encode(array("msg"=>"Ошибка! Попробуйте заполнить форму ещё раз.","type"=>"error")); exit();
    } else{
        echo json_encode(array("msg"=>"Ваше сообщение успешно отправлено","type"=>"success"));
        $name = validate($_POST['name'], 200);
        $email = validate($_POST['email'], 200);
        $subject = validate($_POST['subject'], 500);
        $msg = validate($_POST['msg'], 3000);

        $content = "<div style='border: 1px solid #D2E9FF;'>
                <div style='background: #D2E9FF; font-size: 16px'>&nbsp;Сообщение с сайта ".$_SERVER['SERVER_NAME']."</div>
                <div style='padding: 5px'>
                    Имя: $name<br>
                    E-mail: $email <br>
                    Тема: $subject <br>
                    Сообщение: $msg <br>
                    IP-адрес отправителя: $_SERVER[REMOTE_ADDR]
                </div>
                </div>";
        SendMail($to_email, $email, $content);
    }
}
?>