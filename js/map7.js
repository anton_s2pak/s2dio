 
 
// Creating a handler for the event window.onLoad
YMaps.jQuery(function() {
	// Create an instance of maps and linking it to create a container
		var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);
	 		
		//Месторасположение  
		var location_all = $('.block_q').find('#location').text(YMaps.location.city +" ("+YMaps.location.region+")");
		$('.loc_val').val(YMaps.location.region);  
		
		
		
			
        map.setCenter(new YMaps.GeoPoint(51, 56),3);     // Центр карты
        map.addControl(new YMaps.Zoom());
        map.setMinZoom(3);
        map.setMaxZoom(9);
		//map.addControl(new YMaps.ScaleLine());
 
		map.addControl( new YMaps.SearchControl({
            resultsPerPage: 5,  // Количество объектов на странице
            useMapBounds: 1     // Объекты, найденные в видимой области карты
                                // будут показаны в начале списка


 

        }));
		 
		
		
		
	// Styles polygonal cities of the first list
	var style = new YMaps.Style();
	style.polygonStyle = new YMaps.PolygonStyle();
	style.polygonStyle.fill = true;
	style.polygonStyle.outline = true;
	style.polygonStyle.strokeWidth = 1;
	style.polygonStyle.strokeColor = "00CC00";
	style.polygonStyle.fillColor = "0cf30c99";
	style.balloonContentStyle = new YMaps.BalloonContentStyle(
	new YMaps.Template("<b>$[name]</b><br />$[description]<br />$[voteup]<br />$[votedown]<br /><a href=/create_order/index.php$[dlink]>Оформить заказ</a>")
	);

	// Styles polygonal cities of the second list
	var style2 = new YMaps.Style();
	style2.polygonStyle = new YMaps.PolygonStyle();
	style2.polygonStyle.fill = true;
	style2.polygonStyle.outline = true;
	style2.polygonStyle.strokeWidth = 1;
	style2.polygonStyle.strokeColor = "FFCC00";
	style2.polygonStyle.fillColor = "ffe50899";
	style2.balloonContentStyle = new YMaps.BalloonContentStyle(
	new YMaps.Template("<b>$[name]</b><br />$[description]<br />$[voteup]<br />$[votedown]<br /><a href=/create_order/index.php$[dlink]>Оформить заказ</a>")
	);
	
 

	var styleNone=new YMaps.Style();
	styleNone.polygonStyle = new YMaps.PolygonStyle();
	styleNone.polygonStyle.fill = true;
	styleNone.polygonStyle.outline = true;
	styleNone.polygonStyle.strokeWidth = 1;
	styleNone.polygonStyle.strokeColor = "ffc7bf";
	styleNone.polygonStyle.fillColor = "ffdcd799";
		
		
//----------------Регионы Росиии--------------------------------------------------------------------------------------------	

	YMaps.Regions.load("ru", function(state, response) {
	if (state == YMaps.State.SUCCESS) {
	       data_obj(response, RegionMAP);
   } else {
			alert("Во время выполнения запроса произошла ошибка: "+ response.error.message);
		}
	});
	
//----------------Регионы Украины-------------------------------------------------------------------------------------------	

	YMaps.Regions.load("ua", function(state, response) {
	if (state == YMaps.State.SUCCESS) {
	       data_obj(response, RegionMAP);
   } else {
			alert("Во время выполнения запроса произошла ошибка: "+ response.error.message);
		}
	});	
	
	
	
	
function data_obj(response, RegionMAP){
//------------------------------------------------------------------------------------------------------------------------------------------------------				
       var gbgOptions = {
						hasBalloon: false,
						hasHint: false
						};
		
		var key=0;
		
		for	(var k3 in response._objects){			
			var nameReg=response._objects[k3].name;
			var reg=response.filter(function (obj) {return obj.name == nameReg;})[0];
			key=0;
			for (var k2 in RegionMAP){
				if(RegionMAP[k2]['NAME']==reg.name){
					key=k2;
					break;
					}
			}
			
			for(var ireg in reg.metaDataProperty.encodedShapes){
				MypolReg=YMaps.Polygon.fromEncodedPoints(reg.metaDataProperty.encodedShapes[ireg]['coords'],reg.metaDataProperty.encodedShapes[ireg]['levels']);
				map.addOverlay(MypolReg);
				if(key&&ireg==0){
					//----
					var X=0,Y=0;
					points=MypolReg.getPoints();
					for(k=0; k<points.length; k++)
						{
						X+=points[k].getX();
						Y+=points[k].getY();
						}
					MypolReg.centerPoly=new YMaps.GeoPoint(X/points.length,Y/points.length);
					//-------
					MypolReg.name = RegionMAP[key]['NAME'];
					MypolReg.description = RegionMAP[key]['TOWN'];
					MypolReg.dlink = RegionMAP[key]['URL'];
					MypolReg.voteup = RegionMAP[key]['voteup'];
					MypolReg.votedown = RegionMAP[key]['votedown'];
				   //-------
					if(RegionMAP[key]['COVER']==100) MypolReg.setStyle(style);
					else if(RegionMAP[key]['COVER']==50) MypolReg.setStyle(style2);
					
					
					if(RegionMAP[key]['COVER']>0){
						YMaps.Events.observe(MypolReg,MypolReg.Events.DblClick,function(target){
										target.openBalloon();
								});
				
						}		
					}
				else 
					{
					MypolReg.name = reg.name;
					MypolReg.setStyle(styleNone);	
					}
				}	
			}
		//------------------------------------------------------------------------------------------------------------------------------------------------------
}


 
	map.redraw();	
	});
 