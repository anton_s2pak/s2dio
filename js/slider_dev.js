window.hasCanvas = !!document.createElement('canvas').getContext;

(function() {
    var lastTime = 0;
    window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
            timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };

    window.cancelAnimationFrame = function(id) {
        window.clearTimeout(id);
    };
}());

if (!Math.sign){
    Math.sign = function(n){
        return (typeof n === "number") ? (n >= 0) ? 1 : -1 : null;
    };
};


var _preloadCanvas = function(_parnt, _d){
    if (!_parnt) return;
    if (hasCanvas){
        this.canvas = $('<canvas />',{id : 'preloader-canvas'}).attr({'width': 60, 'height' : 60}).appendTo($(_parnt));
        this.ctx = this.canvas.get(0).getContext('2d');
    }
    this.a = 0;
    this.d = 2*17/_d || 0.02;
    this.t = false;
}// <!>

_preloadCanvas.prototype = {
    drawCircle : function(){
        this.ctx.beginPath();
        this.ctx.arc(30, 30, 22.5, 0, Math.PI*2, true);
        this.ctx.lineWidth = 8;
        this.ctx.strokeStyle = 'rgba(125, 125, 125, 0.5)';
        this.ctx.stroke();
    },
    drawLoader : function(){
        this.ctx.beginPath();
        this.ctx.arc(30, 30, 22.5, Math.PI*(this.a - 0.5), Math.PI*this.a, true);
        this.ctx.lineWidth = 8;
        this.ctx.strokeStyle = 'rgba(255, 255, 255, 1)';
        this.ctx.stroke();
        this.a = this.a < 2 ? this.a + this.d : 0;
    },
    animate: function(){
        if (hasCanvas){
            this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
            this.drawCircle();
            this.drawLoader();
        };
        this.t = requestAnimationFrame($.proxy(this.animate, this));
    },
    start : function(){
        if (this.t) return;
        this.t = requestAnimationFrame($.proxy(this.animate, this));
    },
    stop : function(){
        cancelAnimationFrame(this.t);
        this.t = false;
    },
    clear: function(){
        if (hasCanvas){
            this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
            this.drawCircle();
            this.a = 0;
        }
        this.stop();
    }
}; //<!>

k=0;

var _timeCanvas = function(_parnt, _d){
    if (!_parnt) return;
    if (hasCanvas){
        this.canvas = $('<canvas />');
        this.canvas.attr({width : 26, height : 26, id : 'timeline-canvas'}).appendTo($(_parnt));
        this.ctx = this.canvas.get(0).getContext('2d');
    };
    this.a = 0;
    this.time = _d;
    this.d = 60*0.32/_d || 0.02;
    this.t = false;
}// <!>

_timeCanvas.prototype = {
    drawCircle : function(){
        this.ctx.beginPath();
        this.ctx.arc(13, 13, 10, 0, Math.PI*2, true);
        this.ctx.lineWidth = 4;
        this.ctx.strokeStyle = 'rgba(125, 125, 125, 0.5)';
        this.ctx.stroke();
    },
    drawTimline : function(){
        if (hasCanvas){
            this.ctx.beginPath();
            this.ctx.arc(13, 13, 10, Math.PI*this.a - Math.PI*0.5,  -Math.PI*0.5, true);
            this.ctx.lineWidth = 4;
            this.ctx.strokeStyle = 'rgba(255, 255, 255, 1)';
            this.ctx.stroke();
        };
        if (this.a >= 2 || this.a + this.d < 0) {
            this.animate = function(){return false};
            this.clear();
            this.func();
        } else{this.a += this.d;}
    },
    an : function(delay){
        var self = this;
        this.animate = function(delay){
            if (hasCanvas){
                self.ctx.clearRect(0, 0, self.canvas.get(0).width, self.canvas.get(0).height);
                self.drawCircle();
            };
            self.drawTimline();
            self.t = window.hasCanvas ? requestAnimationFrame($.proxy(self.animate, self)) : window.setTimeout(func, delay);
        };
        this.animate(delay);
    },
    start : function(func, delay){
        this.an(delay);
        if (func && typeof func === "function") this.func = func;
    },
    stop : function(){
        cancelAnimationFrame(this.t);
        this.t = false;
        this.animate = function(){this.t = false; return false};
    },
    clear: function(){
        this.stop();
        if (hasCanvas){
            this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
            this.drawCircle();
            this.a = 0;
        };
    }
}; //<!>

(function($){

    lysogor = window.lysogor || {};

    lysogor.placeholder = function(){
        $('input[type="text"], textarea').bind('click focus blur', function(e){
            if((e.type == 'focus' || e.type == 'click') && $(this).val() == this.defaultValue){
                $(this).val('');
            } else if(e.type == 'blur' && $(this).val() == ''){
                $(this).val(this.defaultValue);
            };
        });
    };// <!>

    lysogor.navSl = function(el){
        if (!el) return;
        var $_links = $('> li', el);
        $_links.hover(
            function(){
                $('> a > span:first-child', this).stop().animate({'margin-top' : 0}, 300);
            },
            function(){
                $('> a > span:first-child', this).stop().animate({'margin-top' : -54}, 300);
            }
        );
    }; // <!>

    lysogor.ieFix = function(){
        $('input[type="text"], textarea')	.focus(function(){$(this).addClass('input-focused')})
            .blur(function(){$(this).removeClass('input-focused')});
        var _window = $(window),
            _body = $('body');
    }; // <!>

    lysogor.scroll = function(){
        var el = $('.main.contact-form .b-center');
        if (!el || el.length <= 0) return;
        var	pos = el.offset().top,
            defH = el.height(),
            padding = 55,
            resizer = function(){
                var winH = $('.slider-wrapper').height(),
                    h = winH - pos - padding;
                h = (h <= defH) ? h : defH;
                el.height(h)
            };
        resizer();
        $(window).bind(lysogor.eventResize, resizer);
    }; //<!>

    lysogor.resizer = function( ){
        var contentView = $('.slider-wrapper'),
            defProp = 1920/1200 || 1.6,
            win = {w : $(window).width(), h: $(window).height() - 30};
        contentView.width(Math.ceil(win.h*defProp)).height(win.h);
        $(window).bind(lysogor.eventResize, function(){
            win.w = $(window).width();
            win.h = $(window).height() - 30;
            contentView.width(Math.ceil(win.h*defProp)).height(win.h);
        }); // <!>
    }; //<!>

    var shuffle = function(o){
        for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };

    lysogor.slider = function(element, options){
        var $this = $(element),
            slideImgs = window.slidesJson || false;
        if (!slideImgs) return;
        var settings = $.extend({
            time: 700,
            delay: 4000,
            arrows: false,
            pagination: false,
            thumbs: true,
            thumbFadeTime: 100,
            thumbSlideTime: 500,
            circle: true,
            timer: true,
            stopTimerBtn: true,
            startPos: 0,
            randonize: false,
            preloadIco: true
        }, options || {});
        var $slides, $thumbs, $thWrapper , $slWrapper = $this.parent('.slider-wrapper'), $thHolder,
            slidesLength = slideImgs.length, thumb = {w : 56, h: 54}, thumbContW = thumb.w*slidesLength,
            $preloader = $('#preloader'), win = {w : $(window).width(), h: $(window).height() - 30},
            timer, $wrapper, currSlideIndx = -1, thumbIndx = -1,
            defProp = 1920/1200 || 1.6, $nav = $('.nav'), navW = $nav.outerWidth(),
            siblingsElmsW = $('#logo').outerWidth(true) +  $('.menu').outerWidth(true),
            thumbHW = navW - siblingsElmsW - 78;
        if (thumbHW >= thumbContW) $('.btn-prev').add($('.btn-next')).addClass('btn-hidden');
        $(document.documentElement).addClass('lysogor-slider');
        $.data(document.body, 'isPaused', false);

        settings.preloadIco && (lysogor.slider.preloadCnvs = new _preloadCanvas($preloader, 2000));

        //private methods

        // create pagination
        var _paginaConstructor = function(){
            var i, html = [];
            html.push('<div class="pagination">');
            for (i = 0; i < slidesLength; i++){
                html.push('<a href="javascript: void(0)" class="pagina'+ (i == settings.startPos ? ' current' : '') +'">' + (i + 1) + '</a>');
            };
            html.push('</div>');
            return html.join('');
        }; // <!>

        // create thumbnails
        var _thumbConstructor = function(){
            var i, html = [];
            html.push('<div class="b-thumbnls">');
            if (slidesLength > 1) html.push('<a class="btn-stop" href="javascript: void(0)"><!-- --></a><a class="btn-prev btn-hidden" href="javascript:void(0);">&lsaquo;</a>');
            html.push('<div class="b-thumbnls-holder"><div class="b-thumbnls-inner">');
            for (i = 0; i < slidesLength; i++){
                html.push('<a class="thumb'+ (i == settings.startPos ? ' current' : '') +' '+ slideImgs[i].id +'"></a>');
            };
            html.push('</div></div>');
            if (slidesLength > 1) html.push('<a class="btn-next" href="javascript:void(0);">&rsaquo;</a>');
            html.push('</div>');
            return html.join('');
        }; // <!>

        // create arrows
        var _arrowsConstructor = function(){
            var html = '<a href="javascript: void(0);" class="arrow-left arrows"><span>&lsaquo;</span></a><a href="javascript: void(0);" class="arrow-right arrows"><span>&rsaquo;</span></a>';
            return html;
        }; // <!>

        var _imageResizer = function(img, w, h, prop){
            if (!img || !w || !h || !prop || typeof prop !== "number") return;
            var _w, _h, _mL, _mT,
                newSize = {
                    w:              Math.round(win.w/prop),
                    h:              Math.round(win.h*prop)
                };
            _h = win.h;
            _w = newSize.h;
            _mL = -0.5*_w;

            img.css({
                'height':           _h,
                'width':            _w,
                'margin-left':      _mL
            });
        } //<!>

        var _preloader = function(img, callback){
            if (!img || typeof img !== 'object') return false;
            if (callback && typeof callback === 'function' && img.loaded){
                _imageResizer(img.img, img.width, img.height, img.prop);
                callback();
                return;
            };
            var $img = $('<img />');
            img.img = $img;
            _imageResizer($img, img.width, img.height, img.prop);
            if ($slides && $slides.length > 0)
                $slides = $slides.add($img);
            else $slides = $img;
            $preloader.show();
            settings.preloadIco && lysogor.slider.preloadCnvs.start();
            $img.attr({'src': img.src + '?' + +new Date(), 'id' : img.id}).load(function(){
                $preloader.hide();
                settings.preloadIco && lysogor.slider.preloadCnvs.clear();
                img.loaded = true;
                if(callback && typeof callback === 'function') callback();
            });
            $this.append($img);
            return $img;
        }; //<!>

        var _thumbPreloader = function(){
            if (thumbIndx == slidesLength - 1){
                $('.thumbnails').addClass('loaded');
            };
            if (thumbIndx >= slidesLength - 1) return false;
            thumbIndx++;
            var $img = $('<img />'),
                img = slideImgs[thumbIndx];
            $img.attr({'src': img.thumb + '?' + new Date().getTime()}).height(thumb.h).load(function(){
                $thumbs.eq(thumbIndx).append($img);
                $(this).fadeIn(settings.thumbFadeTime, function(){_thumbPreloader()});
            });
            return $img;
        }; //<!>

        var _clearTimer = function(){
            lysogor.slider.timeCnvs.clear();
        }; //<!>

        var _setTimer = function(func, delay){
            if (!func || !delay || typeof func !== 'function' || typeof delay !== 'number' || $.data(document.body, 'isPaused')) return;
            lysogor.slider.timeCnvs.start(func, delay);
            $.data(document.body, 'isPaused', false);
        }; //<!>

        var _recalcPosOnResize = function(){
            if (!$thWrapper) return;
            var delta,
                pos = parseInt($thWrapper.css('left')) || 0,
                navW = $nav.outerWidth();
            thumbHW = Math.round(navW - siblingsElmsW - 78);
            if (thumbHW < 24) $('.b-thumbnls').hide();
            else {
                $('.b-thumbnls').show();
                $thHolder.width(thumbHW);
                $('.btn-prev').add($('.btn-next')).removeClass('btn-hidden');
                if (pos >= 0) $('.btn-prev').addClass('btn-hidden');
                if (Math.abs(pos) >= thumbContW - thumbHW)  $('.btn-next').addClass('btn-hidden');
                if (thumbHW >= thumbContW) $('.btn-prev').add($('.btn-next')).addClass('btn-hidden');
                if (Math.abs(pos) > thumbContW - thumbHW) {
                    delta = thumbHW - thumbContW;
                    $thWrapper.css({'left':  delta});
                }
            };
        }; // <!>

        var _thumbSlide = function(o){
            if (typeof o !== 'number' || $thWrapper.is(':animated')) return;
            var shift = thumbHW,
                delta,
                pos = parseInt($thWrapper.css('left')) || 0;
            if (o > 0){
                delta = pos - shift;
                if (pos - shift < thumbHW - thumbContW){
                    if (Math.abs(thumbHW - thumbContW - pos) >= thumbHW) return;
                    else delta = thumbHW - thumbContW;
                };
            } else{
                delta = pos + shift;
                if (pos + shift > 0){
                    if (Math.abs(pos) >= shift) return;
                    else delta = 0;
                };
            };
            $('.btn-prev').add($('.btn-next')).removeClass('btn-hidden');
            if (delta >= 0) $('.btn-prev').addClass('btn-hidden');
            if (Math.abs(delta) >= thumbContW - thumbHW)  $('.btn-next').addClass('btn-hidden');
            if (thumbHW >= thumbContW) $('.btn-prev').add($('.btn-next')).addClass('btn-hidden');
            $thWrapper.stop(true, true).animate({'left':  delta}, settings.thumbSlideTime);
        }; //<!>

        var _thumbSlideNext = function(e){
            _thumbSlide(1);
            if(e) e.preventDefault();
        }; //<!>

        var _thumbSlidePrev = function(e){
            _thumbSlide(0);
            if(e) e.preventDefault();
        }; //<!>

        // change slide
        var _setSlide = function(page){
            if (typeof page !== 'number' || page > slidesLength - 1 || page < 0) return;
            if (settings.pagination){
                $('.pagina:eq('+ page +')', $this).addClass('current').siblings().removeClass('current');
            };
            var callback = function(){
                if (currSlideIndx !== page){
                    currSlideIndx = page;
                    $slides.filter('#'+ slideImgs[currSlideIndx].id).addClass('current').css('opacity', 0).animate({'opacity' : 1}, settings.time).siblings().removeClass('current').animate({'opacity' : 0}, settings.time);
                    if (settings.thumbs) $thumbs.eq(currSlideIndx).addClass('current').siblings().removeClass('current');
                }
                _setTimer(_nextSlideHandler, settings.delay + settings.time);
            };
            _preloader(slideImgs[page], callback);
//             console.timeEnd('time');
//             console.time('time');
        }; // <!>

        var _paginationClickHandler = function(e){
            _setSlide($(this).index());
            e.preventDefault();
        }; // <!>

        var _nextSlideHandler = function(){
            if ($slides.is(':animated')) return;
            var current = currSlideIndx + 1;
            if (settings.circle && current >= slidesLength){
                current = 0;
                $thWrapper.stop(true, true).animate({'left':  0}, settings.thumbSlideTime);
            };
            if ($thumbs.eq(current).position().left + thumb.w > Math.abs(parseInt($thWrapper.css('left'))) + thumbHW){
                _thumbSlideNext();
            }
            _setSlide(current);
        }; //<!>

        var _prevSlideHandler = function(){
            if ($slides.is(':animated')) return;
            var current = currSlideIndx - 1;
            if (settings.circle && current < 0){
                current = slidesLength - 1;
            };
            _setSlide(current);

        }; //<!>

        var _thumbClickHandler = function(e){
            if ($slides.is(':animated')) return;
            var curIndx = $(this).index(),
                offset =  $thumbs.eq(curIndx).offset() || 0,
                pos = parseInt($thWrapper.css('left')) || 0;
            if (Math.abs(offset.left + thumb.w) >= win.w) _thumbSlideNext();
            else if (offset.left < 0) _thumbSlidePrev();
            lysogor.slider.timeCnvs.clear();
            _setSlide(curIndx);
            if(e) e.preventDefault();
        }; //<!>

        var _init = function(){
            if(settings.pagination || settings.thumbs){
                $wrapper = $('<div class="slider-controls"></div>');
                $this.after($wrapper);
            };

            if (settings.pagination){
                $wrapper.append(_paginaConstructor());

                // set handlers
                $('.pagination', $this).on('click', 'a', _paginationClickHandler);
            };
            if (settings.arrows){
                $slWrapper.append(_arrowsConstructor());

                // set handlers
                $slWrapper.on('click', '.arrow-right', _nextSlideHandler);
                $slWrapper.on('click', '.arrow-left', _prevSlideHandler);
            };

            if (settings.thumbs){
                $(_thumbConstructor()).insertBefore('#logo');
                $slWrapper.height(win.h);
                $thumbs = $('.nav .thumb');
                $thWrapper = $thumbs.parent();
                $thHolder = $thWrapper.parent();
                _recalcPosOnResize();
                _thumbPreloader();
                // set handlers
                $slWrapper.on('click', '.thumb', _thumbClickHandler);
            };

            if (settings.stopTimerBtn){
                // set handlers
                $slWrapper.on('click', '.btn-stop', function(){
                    var _this = $(this);
                    if (!$.data(document.body, 'isPaused')){
                        $.data(document.body, 'isPaused', true);
                        _clearTimer();
                        _this.addClass('paused');
                    } else{
                        $.data(document.body, 'isPaused', false);
                        _setTimer(_nextSlideHandler, settings.delay);
                        _this.removeClass('paused');
                    }})
                    .on('click', '.btn-next', _thumbSlideNext)
                    .on('click', '.btn-prev', _thumbSlidePrev);

                lysogor.slider.timeCnvs = new _timeCanvas('.btn-stop', settings.delay);
            };


            settings.randonize && shuffle && shuffle(slideImgs);
            _setSlide(settings.startPos);

            $(window).bind(lysogor.eventResize, function(){
                win.w = $(window).width();
                win.h = $(window).height() - 30;
                _recalcPosOnResize();
                $slides.each(function(){
                    var page = $(this).attr('id').replace(/\D*/, '');
                    _imageResizer(slideImgs[page].img, slideImgs[page].width, slideImgs[page].height, slideImgs[page].prop);
                });
            }); // <!>



            /*  $(window).bind('blur', function(){
             _clearTimer();
             })

             $(window).bind('focus', function(){
             _setTimer(_nextSlideHandler, settings.delay);
             })*/
        }; // <!>


        //public methods

        this.setSlide = function(page){
            _setSlide(page);
        }; // <!>

        _init();
    };

    $.fn.Slider = function(options)
    {
        return this.each(function()
        {
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data('slider')) return;

            // pass options to plugin constructor
            var range = new lysogor.slider(this, options);

            // store plugin object in this element's data
            element.data('slider', lysogor.slider);
        });
    };
    $.fn.extend({
        setSlide: function(page){
            $(this).data('slider').setSlide();
        }
    });

    lysogor.tabs = function(el, options){
        if (!el) return;
        this.el = jQuery(el);
        this.settings = jQuery.extend(true, {
            startIndx : 0
        }, options || {});

        this.init();
    }; // <!>

    lysogor.tabs.prototype = {
        init : function(){
            this.items = jQuery('.b-tab-item', this.el);
            this.count = this.items.length;

            if(window.location.hash){
                this.get(window.location.hash, false);
            } else {
                this.get(this.settings.startIndx);
            };

            var self = this;
            $(window).bind('hashchange', function(e){
                self.get(window.location.hash);
                e.preventDefault();
            });
        },
        get : function(id, scroll){
            if (typeof id === 'number' && id >= 0 && id < this.count){
                this.items.eq(id).addClass('b-tab-item-active').siblings('.b-tab-item').removeClass('b-tab-item-active');
                if (scroll) this.scrollTo(id);
                return;
            } else if(typeof id === 'string'){
                var item = this.items.filter(id);
                if (!item.length) {
                    this.get(this.settings.startIndx);
                    return;
                };
                this.get((item.index()), scroll); // We have to substract 1 because of first element is <ul> with tab-headers
                return;
            };
            return;
        },
        scrollTo : function(id){
            jQuery(document.documentElement).scrollTop(this.items.eq(id).offset().top);
        }

    }; // <!>


    lysogor.popup = function(id, options){
        this.el = jQuery(id);
        if (!this.el || this.el.length < 1) return;
        this.settings = jQuery.extend({
            maskId :          '#mask',
            defaultClass :    '.lysogor-popup',
            closeBtn :        '.close',
            onBeforeShow :     null,
            onAfterShow :      null,
            onBeforeHide :     null,
            onAfterHide :      null
        }, options);
        this.closeBtn = jQuery(this.settings.closeBtn);
        (function(self){
            var dd = self.getDeafaultDimensions();
            self.el.width(dd.el.w);
            self.recalcPosition();
            self.closeBtn.bind('click', jQuery.proxy(self.hide, self));
            jQuery(window)  .bind('resize load', jQuery.proxy(self.recalcPosition, self))
                .scroll(jQuery.proxy(self.onScrollHandler, self));
            jQuery(self.settings.maskId).bind('click', jQuery.proxy(self.hideAll, self));
        })(this);

    }; // <!>
    lysogor.popup.prototype = {
        show: function(e){
            this.settings.onBeforeShow && this.settings.onBeforeShow();
            this.el.css({left: '50%', top: '50%'});
            jQuery(this.settings.maskId).show();
            if(e){
                e.preventDefault();
                e.stopPropagation();
            };
            this.settings.onAfterShow && this.settings.onAfterShow();
        },
        hide: function(e){
            this.settings.onBeforeHide && this.settings.onBeforeHide();
            this.el.css({left: -9999, top: -9999});
            jQuery(this.settings.maskId).hide();
            if(e){
                e.preventDefault();
                e.stopPropagation();
            };
            this.settings.onAfterHide && this.settings.onAfterHide();
        },
        hideAll: function(e){
            this.settings.onBeforeHide && this.settings.onBeforeHide();
            jQuery(this.settings.defaultClass).css({left: -9999, top: -9999});
            jQuery(this.settings.maskId).hide();
            if(e){
                e.preventDefault();
                e.stopPropagation();
            };
            this.settings.onAfterHide && this.settings.onAfterHide();
        },
        recalcPosition: function(){
            var d = this.getDimensions();
            this.el.css({
                marginLeft:     d.pos.dx,
                marginTop:      d.pos.dy,
                height:         d.el.h
            });
        },
        onScrollHandler: function(){
            var sL = jQuery(document.documentElement).scrollLeft(),
                d = this.getDimensions(),
                dd = this.getDeafaultDimensions,
                elW = dd.el.w + dd.el.pw;
            if (d.win.w < elW && sL <= elW - d.win.w){
                this.el.css({
                    marginLeft:     d.pos.dx - sL
                });
            };
        },
        getDeafaultDimensions: function(){
            if (!this.el) return;
            var el = {
                w: this.el.width(),
                h: this.el.height(),
                ow: this.el.outerWidth(),
                oh: this.el.outerHeight()
            };
            return this.getDeafaultDimensions = {
                el: {
                    w: el.w,
                    h: el.h,
                    pw: el.ow - el.w,
                    ph: el.oh - el.h,
                    dx: -0.5*el.ow,
                    dy: -0.5*el.oh
                }
            };
        },
        getDimensions: function(){
            var dd = this.getDeafaultDimensions,
                win = {
                    w: jQuery(window).width(),
                    h: jQuery(window).height()
                },
                pos = {
                    x: 0.5*win.w + dd.el.dx,
                    y: 0.5*win.h + dd.el.dy
                };
            return {
                el: {
                    w: pos.x < 0 ? win.w - dd.el.pw : dd.el.w,
                    h: pos.y < 0 ? win.h - dd.el.ph : 'auto'
                },
                pos: {
                    x: pos.x,
                    y: pos.y,
                    dx: pos.x < 0 ? dd.el.dx - pos.x : dd.el.dx,
                    dy: pos.y < 0 ? dd.el.dy - pos.y : dd.el.dy
                },
                win: {
                    w: win.w,
                    h: win.h
                }
            };
        },
        getPosition: function(){
            var offset = this.el.offset();
            return  {
                t: Math.floor(offset.top),
                l: Math.floor(offset.left)
            };
        },
        setShift: function(x, y){
            if (typeof x !== 'number' || typeof y !== 'number') return;
            this.el.css({marginLeft: x, marginTop: y});
        }
    }; // <!>



    $(document).ready(function(){
        lysogor.isiPad = /ipad/.test(navigator.userAgent.toLowerCase());
        lysogor.isAndroid = /android/.test(navigator.userAgent.toLowerCase());
        lysogor.isBlackberry = /blackberry/.test(navigator.userAgent.toLowerCase());
        lysogor.isIPhone = /iphone/.test(navigator.userAgent.toLowerCase());
        lysogor.isTapDevice = lysogor.isiPad || lysogor.isAndroid || lysogor.isBlackberry || lysogor.isIPhone;
        lysogor.eventResize = lysogor.isTapDevice ? 'orientationchange' : 'resize';
        lysogor.resizer();
        lysogor.placeholder();

        lysogor.isIe = $.browser.msie && $.browser.version <= 8 ? true : false;
        if(lysogor.isIe) lysogor.ieFix();

        lysogor.navSl('.nav .menu');

        if (window.info){
            $('#slider').Slider({
                arrows: false,
                pagination: false,
                time: 1000,
                delay: 6000,
                thumbs: false,
                thumbSlideTime: false,
                randonize : true,
                preloadIco : false
            })} else if (!window.portfolio){
            $('#slider').Slider({
                arrows: false,
                pagination: false,
                time: 1000,
                delay: 3000,
                thumbs: false,
                thumbSlideTime: false,
                randonize : true
            });
        } else{
            $('#slider').Slider({
                arrows: false,
                pagination: false,
                time: 1000,
                delay: 3000
            });
        };

        $('.lysogor-popup-link').each(function(){
            var self = $(this);
            if (self.data('lysogor.popup')) return;
            var el = $('+ .lysogor-popup', self),
                popup = new lysogor.popup(el);
            if (el.length < 1) return;
            self.bind('click', $.proxy(popup.show, popup));
            self.data('lysogor.popup', popup);
        });
    });

    $(window).load(lysogor.resizer);
})(jQuery);



/**
 * Cookie plugin
 *

 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};



/*Проверки*/
//===================================================================================


function highlight(what){ eval("document.getElementById('"+what+"').style.border='1px solid #be1010'"); }
function normalize(what){ eval("document.getElementById('"+what+"').style.border='1px solid #7f9db9'"); }


function Priem(form){
    var ok=true;
    if(form.name.value=="") { highlight('name'); ok=false; } else { normalize('name'); }
    if(form.tel.value=="" || !/^[+]?[-0-9\(\) ]{11,20}/i.test(form.tel.value)) { highlight('tel'); ok=false; } else { normalize('tel'); }
    if(form.datapr.value=="") { highlight('datapr'); ok=false; } else { normalize('datapr'); }
    return ok;
}

function Feedback(e){
    var ok = true,
        form = this,
        name = $("#name", this),
        email = $("#email", this),
        subject = $("#subject", this),
        msg = $("#msg", this);
    if(!name.val() || name.val() == name[0].defaultValue || name.val() > 120) { highlight('name'); ok = false; } else { normalize('name'); }
    if(!email.val() || email.val() == email[0].defaultValue || !/^[a-z0-9\._-]+@[a-z0-9\._-]+\.[a-z]{2,6}$/i.test(email.val())) { highlight('email'); ok = false; } else { normalize('email'); }
    if(!subject.val() || subject.val() == subject[0].defaultValue || subject.val() > 500) { highlight('subject'); ok = false; } else { normalize('subject'); }
    if(!msg.val() || msg.val() == msg[0].defaultValue || msg.val() > 3000) { highlight('msg'); ok = false; } else { normalize('msg'); }
    if (ok){
        $.post("/contacts/send/",
            $('#contact-form').serialize(),
            function(data){
                alert(data.msg);
                if (data.type == "success"){
                    name.val(name[0].defaultValue);
                    email.val(email[0].defaultValue);
                    subject.val(subject[0].defaultValue);
                    msg.val(msg[0].defaultValue);
                }
            },
            "json"
        );
    };

    e.preventDefault();
    return false;
}

$(document).ready(function(){
    $('#contact-form, #contact-form button').bind('submit', Feedback);
});