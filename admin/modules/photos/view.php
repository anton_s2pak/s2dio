<?
$Photos = new Photos();
$Photos->id_item = validate($_GET['id_item'], 60);
$id_item = validate($_GET['id_item'], 60);
$Photos->id_category_get = validate($_GET['id_category'], 60);
$id_category = validate($_GET['id_category'], 60);
$Photos->name = validate($_POST['name'], 120);
$Photos->id_category = validate($_POST['id_category'], 120);
$Photos->com_on = validate($_POST['com_on'], 3);
$Photos->text = $_POST['text'];
$Photos->url = $_POST['url'];
$Photos->order = $_POST['order'];
$Photos->title = validate($_POST['title'], 3000);
$Photos->keywords = validate($_POST['keywords'], 3000);
$Photos->description = validate($_POST['description'], 3000);
$Photos->date = date("Y-m-d H:i:s");
$Photos->lang = $lg;


$dropdown = validate($_POST['dropdown'], 7); //Массовое удаление


//Cортировка
if ($dropdown == "sort") {
    $Photos->sort = "ORDER BY name ASC";
} elseif ($dropdown == "unsort") {
    $Photos->sort = "ORDER BY name DESC";
} elseif ($dropdown == "sortid") {
    $Photos->sort = "ORDER BY id_item  ASC";
}


/***************************************************Отображение каталога*******************************************/
if ($act == "view_cat") {

    $Photos = $Photos->ViewAdminCat();
    while ($row = $db->sql_fetchrow($Photos)) {

        $num = $db->sql_numrows($db->sql_query("SELECT * FROM " . PREF . "photos WHERE id_category = '" . $row['id_item'] . "'"));
        $view_cat[] = array(
            "id_item" => $row["id_item"],
            "url" => $row["url"],
            "name" => $row["name"],
            "image" => $row["image"],
            "date" => $row["date"],
            "text" => $row["text"],
            "url" => $row["url"] . '.html',
            "order" => $row["order"],
            "num" => $num
        );

    }


    // Удалить сразу несколько записей можно
    if ($dropdown == "del") {
        //Отправляем в функцию
        $query = delete($_POST['type']);
        // Завершаем формирование SQL-запроса на удаление
        $SQLS = "DELETE FROM " . PREF . "photos_cat WHERE id_item IN " . $query;
        $db->sql_query($SQLS);
        $Validate->Locate("/admin/photos/" . $id_category . "/", 0, $done_delete);
    }

    $s2cms->assign("num", $num);
    $s2cms->assign("view_cat", $view_cat);
    $s2cms->assign("page", $theme . "/photos/view_cat.tpl");
    $s2cms->display($theme . "/main.tpl");

}


/***************************************************Добавление каталога*******************************************/
if ($act == "add_cat") {
    if (isset($_POST['name'])) {

        if (!empty($_FILES['image']['name'])) {
            $file = $_FILES['image']['tmp_name'];
            $filename = $_FILES['image']['name'];
            $type = strtolower(substr($filename, 1 + strrpos($filename, ".")));
            $img = 's2_' . time() . '.' . $type;
            $save = copy($file, "../uploads/photos/big/" . $img);

            //http://recens.ru/php/resize_and_crop.html
            $Images->Crop('../uploads/photos/big/' . $img, '../uploads/photos/crop/' . $img);
            $Images->Resize('../uploads/photos/crop/' . $img, '../uploads/photos/small/' . $img, 54, 54);
            $Images->Del('../uploads/photos/crop/' . $img);
            $Photos->image = $img;
        }

        $Photos = $Photos->CatAdd();
        $Validate->Locate("/admin/photos/", 0, $done_add);
    }

}

/***************************************************Редактирование каталога*******************************************/

if ($act == "edit_cat") {

    if (isset($_POST['name'])) {

        if (!empty($_FILES['image']['name'])) {

            #Проверка и удаление староого изображения
            # Берем из базы раздел
            $sql = $db->sql_query("SELECT * FROM " . PREF . "photos_cat WHERE id_item = '" . $id_item . "' ORDER BY  image");
            $photos = $db->sql_fetchrow($sql);


            if (!empty($photos['image'])) {

                $file_small = "../uploads/photos/small/" . $photos['image'];
                $file_crop = "../uploads/photos/crop/" . $photos['image'];

                # Если только этот товар использует этот файл картинки, удаляем файл

                if (is_file($file_small) && is_file($file_crop)) ;
                unlink($file_small);
                unlink($file_crop);

            }


            $file = $_FILES['image']['tmp_name'];
            $filename = $_FILES['image']['name'];
            $type = strtolower(substr($filename, 1 + strrpos($filename, ".")));
            $img = 's2_' . time() . '.' . $type;
            $save = copy($file, "../uploads/photos/big/" . $img);

            $Images->Crop('../uploads/photos/big/' . $img, '../uploads/photos/crop/' . $img);
            $Images->Resize('../uploads/photos/crop/' . $img, '../uploads/photos/small/' . $img, 54, 54);
            $Images->Del('../uploads/photos/crop' . $img);
            $Photos->image = $img;
            $Photos_img = $Photos->CatEditImg();
        }
        $Photos = $Photos->CatEdit();
        $Validate->Locate("/admin/photos/cat/edit/" . $id_item . "/", 0, $delete_edit);

    } else {

        $Photos = $Photos->CatViewEdit();
        $row = $db->sql_fetchrow($Photos);

    }

    $s2cms->assign("url", $row['url']);
    $s2cms->assign("text", $row['text']);
    $s2cms->assign("order", $row['order']);
    $s2cms->assign("id_item", $row['id_item']);
    $s2cms->assign("name", $row['name']);
    $s2cms->assign("image", $row['image']);
    $s2cms->assign("page", $theme . "/photos/edit_cat.tpl");
    $s2cms->display($theme . "/main.tpl");

}


/***************************************************Удаление каталога*******************************************/


if ($act == "delete_cat") {

    # Проверка на  наличие записей в категории
    $sql = $db->sql_query("SELECT * FROM " . PREF . "photos WHERE id_category = '" . $id_item . "'");
    $photos_del_num = $db->sql_numrows($sql);


    # Если есть записи выводим сообщение
    if ($photos_del_num > 0) {
        $Validate->Locate("/admin/photos/", 1, $done_delete_cat);
    } else {
        # Берем из базы раздел
        $sql = $db->sql_query("SELECT * FROM " . PREF . "photos_cat WHERE id_item = '" . $id_item . "' ORDER BY  image");
        $photos = $db->sql_fetchrow($sql);


        if (!empty($photos['image'])) {

            $file_small = "../uploads/photos/small/" . $photos['image'];
            $file_big = "../uploads/photos/big/" . $photos['image'];

            # Если только этот товар использует этот файл картинки, удаляем файл

            if (is_file($file_small) && is_file($file_big)) ;
            unlink($file_small);
            unlink($file_big);

        }


        $Photos = $Photos->CatDel();
        $Validate->Locate("/admin/photos/", 0, $done_delete);

    }

}


if ($act == "view") {

    $Photos = $Photos->ViewAdmin();
    $num = $db->sql_numrows($Photos);
    while ($row = $db->sql_fetchrow($Photos)) {
        $view[] = array(
            "id_item" => $row["id_item"],
            "name" => $row["name"],
            "image" => $row["image"],
            "com_on" => $row["com_on"],
            "id_category" => $row["id_category"]
        );
    }

    if (!isset($view)){
        $cat = $_GET["id_category"];
        $s2cms->assign("cat", $cat);
    }

    $s2cms->assign("num", $num);
    $s2cms->assign("view", $view);
    $s2cms->assign("page", $theme . "/photos/view.tpl");
    $s2cms->display($theme . "/main.tpl");

}


if ($act == "add") {
    if (isset($_POST['id_category']) || isset($_POST['name']) || isset($_POST['image'])) {

        $file = $_FILES['image']['tmp_name'];
        $filename = $_FILES['image']['name'];
        if ($_FILES['image']['error'] === 0){
            $type = strtolower(substr($filename, 1 + strrpos($filename, ".")));
            $img = 's2_' . time() . '.' . $type;
            $save = copy($file, "../uploads/photos/big/" . $img);

            $Images->Crop('../uploads/photos/big/' . $img, '../uploads/photos/crop/' . $img);
            $Images->Resize('../uploads/photos/crop/' . $img, '../uploads/photos/small/' . $img, 54, 54);
            $Images->Del('../uploads/photos/crop/' . $img);

            list($width, $height) = getimagesize("../uploads/photos/big/" . $img);
            $Photos->height = $height;
            $Photos->width = $width;
            $Photos->image = $img;
            if (is_numeric($height) && is_numeric($width) && $height > 0 && $width > 0){
                $Photos = $Photos->Add();
                $Validate->Locate("/admin/photos/", 0, $done_add);
            } else{
                $Images->Del('../uploads/photos/small/' . $img);
                $Images->Del('../uploads/photos/big/' . $img);
            }
        }

    } else {

        $Photos = $Photos->ViewAdminCat();
        while ($row = $db->sql_fetchrow($Photos)) {

            $view_cat[] = array(
                "id_item" => $row["id_item"],
                "name" => $row["name"]
            );

        }

        if (isset($_GET['cat'])){
            $cat = $_GET['cat'];
            $s2cms->assign("cat", $cat);
        }
        $s2cms->assign("view_cat", $view_cat);
        $s2cms->assign("page", $theme . "/photos/addedit.tpl");
        $s2cms->display($theme . "/main.tpl");


    }
}


if ($act == "edit") {

    if (isset($_POST['id_category']) || isset($_POST['name'])) {

        if (!empty($_FILES['image']['name'])) {


            # Проверка и удаление староого изображения
            # Берем из базы раздел
            $sql = $db->sql_query("SELECT * FROM " . PREF . "photos_cat WHERE id_item = '" . $id_item . "' ORDER BY  image");
            $photos = $db->sql_fetchrow($sql);


            if (!empty($photos['image'])) {

                $file_small = "../uploads/photos/small/" . $photos['image'];
                $file_crop = "../uploads/photos/crop/" . $photos['image'];

                # Если только этот товар использует этот файл картинки, удаляем файл

                if (is_file($file_small) && is_file($file_crop)) ;
                unlink($file_small);
                unlink($file_crop);

            }
            #----------------------------------------------------------------#

            $file = $_FILES['image']['tmp_name'];
            $filename = $_FILES['image']['name'];
            $type = strtolower(substr($filename, 1 + strrpos($filename, ".")));
            $img = 's2_' . time() . '.' . $type;
            $save = copy($file, "../uploads/photos/big/" . $img);

            $Images->Crop('../uploads/photos/big/' . $img, '../uploads/photos/crop/' . $img);
            $Images->Resize('../uploads/photos/crop/' . $img, '../uploads/photos/small/' . $img, 54, 54);
            $Images->Del('../uploads/photos/crop/' . $img);
            $Photos->image = $img;
            $Photos_img = $Photos->EditImg();
        }

        list($width, $height) = getimagesize("../uploads/photos/big/" . $img);
        $Photos->height = $height;
        $Photos->width = $width;
        $Photos->image = $img;
        $Photos = $Photos->Edit();
        $Validate->Locate("/admin/photos/edit/" . $id_category . "/" . $id_item . "/", 0, $done_edit);

    } else {

        $Photos_cat = $Photos->ViewAdminCat();
        while ($row = $db->sql_fetchrow($Photos_cat)) {

            $view_cat[] = array(
                "id_item" => $row["id_item"],
                "name" => $row["name"]
            );

        }

        $Photos = $Photos->ViewEdit();
        $row = $db->sql_fetchrow($Photos);

    }

    $s2cms->assign("view_cat", $view_cat);
    $s2cms->assign("id_category_get", validate($_GET['id_category'], 60));
    $s2cms->assign("name", $row['name']);
    $s2cms->assign("text", $row['text']);
    $s2cms->assign("image", $row['image']);
    $s2cms->assign("com_on", $row['com_on']);
    $s2cms->assign("title", $row['meta_title']);
    $s2cms->assign("keywords", $row['meta_keywords']);
    $s2cms->assign("description", $row['meta_description']);
    $s2cms->assign("page", $theme . "/photos/addedit.tpl");
    $s2cms->display($theme . "/main.tpl");

}


if ($act == "delete") {
    # Берем из базы товар, походу вычисляя количество товаров, использующих эту же картинку

    $sql = $db->sql_query("SELECT * FROM " . PREF . "photos WHERE id_item = '" . $id_item . "' ORDER BY image");
    $photos = $db->sql_fetchrow($sql);


    if (!empty($photos['image'])) {

        $file_small = "../uploads/photos/small/" . $photos['image'];
        $file_big = "../uploads/photos/big/" . $photos['image'];
        $file_crop = "../uploads/photos/crop/" . $photos['image'];

        #  удаляем файл
        if (is_file($file_small) && is_file($file_big) && is_file($file_crop)) ;
        unlink($file_small);
        unlink($file_big);
        unlink($file_crop);

    }
    $Photos = $Photos->Del();
    $Validate->Locate("/admin/photos/" . $photos['id_category'] . "/", 0, $done_delete);

}

?>