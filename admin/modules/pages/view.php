<?
 	$Pages = new Pages();
	//Передача значений в класс
	$Pages->id_item = validate($_GET['id_item'], 60);
	$Pages->id_menu = validate($_GET['id_menu'], 2);
    $Pages->namemenu = validate($_POST['menu'], 30);
    $Pages->name = validate($_POST['name'], 120);
    $Pages->url = validate($_POST['url'], 120);
    $Pages->text = $_POST['text'];
	$Pages->com_on = validate($_POST['com_on'], 3);
	$Pages->title = validate($_POST['title'], 3000);
	$Pages->keywords = validate($_POST['keywords'], 3000);
	$Pages->description = validate($_POST['description'], 3000);
	$Pages->id_str = validate($_POST['id_str'], 6);
	$Pages->enabled = validate($_POST['enabled'], 1);
	$Pages->page = validate($_POST['page'], 1);
	 
    $Pages->lang = $lg;
	
	
	$id_category = validate($_GET['id_category'], 60);
	$id_item = validate($_GET['id_item'], 60);
	$menu = validate($_GET['id_menu'], 3);
    $pageon = validate($_POST['pageon'], 1); //Если есть создать новую страницу
	$dropdown = validate($_POST['dropdown'], 7); //Массовое удаление
	
/***************************************************СТРАНИЦЫ*******************************************/	
//Просмотр всех страниц  
if($act == "view") {

     //сортировка
      if($dropdown == "sort") { 
	  $Pages->sort = "ORDER BY name ASC";
	  }elseif($dropdown == "unsort") {
	  $Pages->sort = "ORDER BY name DESC";
	  }elseif($dropdown == "sortid") {
	  $Pages->sort = "ORDER BY id_item  ASC";
	  }

  $Pages = $Pages->ViewAll();
	while($row = $db->sql_fetchrow($Pages)) {

		$view[] = array(
						"id_item" => $row["id_item"],
						"enabled" => $row["enabled"],
						"url" => url($row["url"]),
						"name" => $row["name"],
						"com_on" => $row["com_on"]

		);

	}
    $s2cms->assign("view", $view);
    $s2cms->assign("page", $theme."/pages/view.tpl");
	$s2cms->display($theme."/main.tpl");

}

 
//Добавление страниц
if($act == "add") {
	if(isset($_POST['name']) || isset($_POST['text'])) {
		$Pages = $Pages->Add();
		$Validate->Locate("/admin/pages/", 0, $done_add);
		 }
	$s2cms->assign("head_title", "Добавить страницу");	 
    $s2cms->assign("page", $theme."/pages/addedit.tpl");
	$s2cms->display($theme."/main.tpl");

}





//Редактирование страницы
if($act == "edit") {
	if(isset($_POST['name']) || isset($_POST['text'])) {
		$Pages = $Pages->Edit();
    	$Validate->Locate("/admin/pages/edit/".$id_item."/", 0, $done_edit);
	} else {
		$Pages = $Pages->ViewEdit();
		$row = $db->sql_fetchrow($Pages);
	}
	
						
	$s2cms->assign("indexPage", $row["page"]);
    $s2cms->assign("name", $row['name']);
	$s2cms->assign("url", $row['url']);
    $s2cms->assign("text", $row['text']);
    $s2cms->assign("com_on", $row['com_on']);
    $s2cms->assign("title", $row['meta_title']);
    $s2cms->assign("keywords", $row['meta_keywords']);
    $s2cms->assign("description", $row['meta_description']);
	$s2cms->assign("head_title", "Редактируем - ".$row['name']);
    $s2cms->assign("page", $theme."/pages/addedit.tpl");
	$s2cms->display($theme."/main.tpl");
}


 //Удалить страницу
if($act == "delete") {
	$Pages = $Pages->Del();
    $Validate->Locate("/admin/pages/", 0, $done_delete);
}



	
// Удалить сразу несколько записей можно
// при помощи запроса "DELETE FROM table_1 WHERE id IN (1,3,5,7)"
// Получаем список отмеченных checkbox
if($dropdown == "del") {
     //Отправляем в функцию
     $query = delete($_POST['type']);
     // Завершаем формирование SQL-запроса на удаление
     $SQLS = "DELETE FROM ".PREF."pages WHERE id_item IN ".$query;
     $db->sql_query($SQLS);
    
     $Validate->Locate("/admin/pages/", 0, $done_delete);
   
}




/***************************************************ВСЕ ОПЕРАЦИИ С МЕНЮ*******************************************/
 //Обший Просмотр всех меню
if($act == "view_cat") {

	$Pages = $Pages->ViewCat();
	while($row = $db->sql_fetchrow($Pages)) {

 
		$view_cat[] = array(
							"id_menu" => $row["id_menu"],
							"name" => $row["name"]
		);

	}

 
    $s2cms->assign("view_cat", $view_cat);
    $s2cms->assign("page", $theme."/pages/view_cat.tpl");
	$s2cms->display($theme."/main.tpl");

}


//Добавление меню 
if($act == "menu_add") {
      $Pages = $Pages->AddMenu();  
      $Validate->Locate("/admin/structure/", 0, $done_add);

}


//Удалить меню 
if($act == "menu_delete") {
	$Pages = $Pages->DelMenu();
    $Validate->Locate("/admin/structure/", 0, $done_delete);

}

 
 

//Вывод деревовиднго меню нерграниченой вложености в Админке

if($act == "structure") {
 
 /*Определяем как называеться меню*/
$rows = $db->sql_fetchrow($Pages->ViewHead());
 
$cats_tree_data = array();
$Pages = $Pages->ViewAdminStr();
if ( $Pages ) {
  while ($row = $db->sql_fetchrow($Pages) ) {
    $cats_tree_data[$row['id_item']] = $row;
    $cats_tree_data[$row['parent']]['childs'][] = $row['id_item'];
    $parent = $row['parent'];
  }
}

    $s2cms->assign("cats_tree_data", $cats_tree_data );
    $s2cms->assign("cats_tree", $cats_tree_data );
	$s2cms->assign("head", $rows['name'] );
	$s2cms->assign("menu", $menu);/*Передаем в шаблон меню*/
    $s2cms->assign("page", $theme."/pages/structure.tpl");
	$s2cms->display($theme."/main.tpl");

}





 //Включение и выключение пункта меню
	if(isset($_GET['enabled']))
    {  
	  $id = $_GET['enabled'];
	  $SQL = "UPDATE ".PREF."structure SET enabled=1-enabled WHERE id_item = '".$id."'";
      $db->sql_query($SQL);
	   if(isset($_GET['act']))
      $Validate->Locate("/admin/structure/".$menu."/", 0, $done_add);
    }
	//

	
	
//Позиция меню  в админ панели
if($_POST['update'] == "structure_pos"){
	$array = $_POST['pos'];
	$ids = $_POST['id'];
    $parent = (isset($_POST['parent'])) ? $_POST['parent'] == 1 ? '' : $_POST['parent'] : '';
	foreach ($ids as $i=>$idval) {
        $SQL = "UPDATE ".PREF."structure SET pos = ".$parent.$array[$i]." WHERE id_item = ".$idval;
        $db->sql_query($SQL);
	} echo '<a class="close" href="#"><img alt="close" title="Close this notification" src="/admin/templates/'.$theme.'/images/icons/cross_grey_small.png"></a><div >Структура успешно сохранена!</div>';

}



 
//Добавление пункта меню  в админ панели
if($act == "structure_add") {
	$Pages = $Pages->AddStructure();
	
	//Добавляем страницу если в админ панели стоит галочка
	if($pageon){
	$SQL = "INSERT ".PREF."pages SET   name = '".$_POST['name']."', url = '".$_POST['url']."', com_on = 'no', text = 'Страница - ".$_POST['name']."', meta_title = '".$_POST['name']."', meta_keywords = '".$_POST['name']."', meta_description = '".$_POST['name']."'";
    $db->sql_query($SQL);
    
	}
	
    $Validate->Locate("/admin/structure/".$menu."/", 0, $done_add);
}





//Редактирование пункта меню  в админ панели
if($act == "structure_edit") {

//Структура меню при редактировании РЕКУРСИВНОЕ МЕНЮ
$cats_tree_data = array();
$Сats_tree = $Pages->ViewAdminStr();
if ( $Сats_tree  ) {
  while ($rows = $db->sql_fetchrow($Сats_tree) ) {
    $cats_tree_data[$rows['id_item']] = $rows;
    $cats_tree_data[$rows['parent']]['childs'][] = $rows['id_item'];
   }
}
//Отправка и отображение данных

	if(isset($_POST['name']) || isset($_POST['url'])) {
		//Включение и выключение пункта меню
          if(isset($_POST['enabled']))
  		  $Pages->enabled = validate('1', 1);
          else
  		  $Pages->enabled = validate('0', 1);
      //
		
		$Pages = $Pages->EditStructure();
		$Validate->Locate("/admin/structure/".$menu."/", 0, $done_edit);
	} else {
		$Pages = $Pages->ViewEditStructure();
		$row = $db->sql_fetchrow($Pages);
    }

    $parentName = $cats_tree_data[$row['parent']]['name'];

    $s2cms->assign("cats_tree_data", $cats_tree_data );
	$s2cms->assign("id", $id_item);
	$s2cms->assign("parentName", $parentName);
    $s2cms->assign("name", $row['name']);
    $s2cms->assign("url", $row['url']);
    $s2cms->assign("parent", $row['parent']);

	$s2cms->assign("enabled", $row['enabled']);
    $s2cms->assign("page", $theme."/pages/structure_edit.tpl");
	$s2cms->display($theme."/main.tpl");

}

 

 //Удаление пункта меню в админ панели
if($act == "structure_delete") {
	$Pages = $Pages->DelStructure();
    $Validate->Locate("/admin/structure/".$menu."/", 0, $done_delete);

}

?>