<?
 
$Config = new Config();
$Validate = new Validate(); 
 

	$Config->id_item = validate($_GET['id_item'], 60);
    $Config->login = validate($_POST['login'], 60);
    $Config->password = validate($_POST['password'], 60);
    $Config->name = validate($_POST['name'], 120);
    $Config->email = validate($_POST['email'], 120);
    $Config->reg_date = date("Y-m-d H:i:s");
    $Config->chmod = validate($_POST['chmod'], 11);
	$Config->lang = $lg;

if($act == "admin_view") {

	$Config = $Config->ViewAdminAll();
	while($row = $db->sql_fetchrow($Config)) {

		$view_admin[] = array(
							"id_item" => $row["id_item"],
							"login" => $row["login"],
							"name" => $row["name"],
							"email" => $row["email"],
							"chmod" => $row["chmod"],
							"reg_date" => $row["reg_date"],
							"last_enter" => $row["last_enter"],
		);

	}

    $s2cms->assign("view_admin", $view_admin);
    $s2cms->assign("page", $theme."/config/admin_view.tpl");
	$s2cms->display($theme."/main.tpl");

}

if($act == "admin_add") {

	$Config = $Config->AdminAdd();
	$Validate->Locate("/admin/login/view/", 0, $done_add);

}

if($act == "admin_edit") {

	if(isset($_POST['login']) || isset($_POST['password'])) {

		$Config = $Config->AdminEdit();
		$Validate->Locate("/admin/login/view/", 0, $done_edit);

	} else {

		$Config = $Config->AdminViewEdit();
		$row = $db->sql_fetchrow($Config);

	}

    $s2cms->assign("login", $row['login']);
    $s2cms->assign("name", $row['name']);
    $s2cms->assign("email", $row['email']);
    $s2cms->assign("chmod", $row['chmod']);
    $s2cms->assign("page", $theme."/config/admin_edit.tpl");
	$s2cms->display($theme."/main.tpl");

}

if($act == "admin_delete") {

	$Config = $Config->AdminDel();
    $Validate->Locate("/admin/login/view/", 0, $done_delete);

}

if($act == "uploads") {

	$file = $_FILES['file']['tmp_name'];
	$filename = $_FILES['file']['name'];
    $filetype = $_FILES['file']['type'];
    $type = strtolower(substr($filename, 1 + strrpos($filename, ".")));
    $new_name = time().'.'.$type;
    $name = copy($file, "../uploads/".$new_name);

	$s2cms->assign("path", getenv("SERVER_NAME"));
	$s2cms->assign("new_name", $new_name);
    $s2cms->assign("page", "config/uploads.tpl");
	$s2cms->display("main.tpl");

}

if($act == "config") {

	if(isset($_POST['page']) || isset($_POST['keywords']) || isset($_POST['description'])) {

    	$Config->email = validate($_POST['email'], 120);
		$Config->page = validate($_POST['page'], 4);
		$Config->lang = validate($_POST['lang'], 2);
    	$Config->work_site = validate($_POST['work_site'], 1);
        $Config->close_text = validate($_POST['close_text'], 1000);
        $Config->title = validate($_POST['title'], 3000);
        $Config->keywords = validate($_POST['keywords'], 3000);
        $Config->description = validate($_POST['description'], 3000);
		$Config->code_counter = validate($_POST['code_counter'], 1000);
		$Config = $Config->ConfigEdit();
		$Validate->Locate("/admin/config/", 0, $done_edit);

	} else {

		$Config = $Config->ConfigView();
		$row = $db->sql_fetchrow($Config);

	}

	$s2cms->assign("email", $row['email']);
	$s2cms->assign("lang", $row['lang']);
	$s2cms->assign("page_num", $row['page']);
	$s2cms->assign("work_site", $row['work_site']);
	$s2cms->assign("close_text", $row['close_text']);
	$s2cms->assign("title", $row['title']);
	$s2cms->assign("keywords", $row['keywords']);
	$s2cms->assign("description", $row['description']);
	$s2cms->assign("code_counter", $row['code_counter']);
	$s2cms->assign("page", $theme."/config/config.tpl");
	$s2cms->display($theme."/main.tpl");

}


# Файловый менеджер
if($act == "mypicture"){ 
	$s2cms->assign("page", $theme."/config/images.tpl");
	$s2cms->display($theme."/main.tpl");
}









# Файловый менеджер
if($act == "styles"){ 
  
    $styles_dir = "../templates/".$theme_frontend."/css";

    # Сохранить стиль
   if(isset($_POST['content']) && isset($_POST['filename']))
    { 
      $style_file = $_POST['filename']; 
	  
      if(preg_match("/^[\_\w]*\.css$/i", $style_file))
      {
        if($handle = @fopen($styles_dir.'/'.$style_file, 'w'))
        {
          fputs($handle, stripslashes($_POST['content']));
          fclose($handle);
        }
     }
	 else
        {
          $error = "Нет прав для записи файла ".$styles_dir.'/'.$style_file;
        }
   }
 
 
 
# Обработка файлаов в каталоге
$files = array();
 
  # Собираем список стилей
    if ($handle = @opendir($styles_dir))
    {
      while (false !== ($file = readdir($handle)))
      { 
        if (is_file($styles_dir.'/'.$file) && preg_match("/^[\_\w]*\.css$/i", $file))
        {
          unset($style); 
		  $style[filename] = $file;
          $style[name] = $file;
          $style[modified_date] = filemtime($styles_dir.'/'.$file);
          $style[edit_url] = '/admin/styles/'.$file;
          
		  if($cont = @file_get_contents($styles_dir.'/'.$file))
          {        
            preg_match('/style name:(.+)\n/i', $cont, $matches);
            if(isset($matches[1]))   
            $contents= trim($matches[1]);
            if(!empty($contents))
             $style[name] = $contents;
         }  
		 
         $files[$style[modified_date].$style[filename]]=$style;              
        } 
      } 
	    

       ksort($files);
       $size = count($files);

      for($i=0; $i<$size; $i++)
      { 
	  $styles[$i] = array_pop($files);
	   if(validate($_GET['edit'], 60) == $styles[$i][filename])
          $current_style = $styles[$i];
		  }
   if(empty($current_style))
	  {
		  $current_style = $styles[0]; 
	  }
       if(!$content = @file_get_contents($styles_dir.'/'.$current_style[filename]))
      {
        $error = "Не могу прочитать файл ".$styles_dir.'/'.$current_style[filename];
      }
	  }else {
  	 $error = "Не могу открыть папку $styles_dir";
    } 
 
 
$s2cms->assign('Styles', $styles);
$s2cms->assign('Content', $content);
$s2cms->assign('CurrentStyle', $current_style);
$s2cms->assign('Error', $error);
$s2cms->assign("page", $theme."/config/styles.tpl");
$s2cms->display($theme."/main.tpl");
  
}
 


?>