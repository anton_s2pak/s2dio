<?
/**
* @package S2dio CMS http://s2dio.com.ua
* @copyright Авторские права (C) 2011 Stupak Oleg. Все права защищены.
* @license Лицензия http://www.gnu.org/copyleft/gpl.html GNU/GPL, смотрите LICENSE.php
* S2dio CMS! - свободное программное обеспечение. Эта версия может быть изменена
* в соответствии с Генеральной Общественной Лицензией GNU, поэтому возможно
* её дальнейшее распространение в составе результата работы, лицензированного
* согласно Генеральной Общественной Лицензией GNU или других лицензий свободных
* программ или программ с открытым исходным кодом.
* Для просмотра подробностей и замечаний об авторском праве, смотрите файл COPYRIGHT.php.
*/

define("SECURITY", TRUE);
// Определение шаблона
define('ADM', 0);

session_start();
include '../config.php';


// Кеширование в админке нам не нужно
Header("Cache-Control: no-cache, must-revalidate");
Header("Pragma: no-cache");

 
include 'login/login.php';

	$s2cms->assign("admin_ses", $_SESSION['login']);
	$s2cms->assign("admin_ses", $_SESSION['login']);
 	$s2cms->assign("id_ses", $_SESSION['id_item']);
	$s2cms->assign("chmod_ses", $_SESSION['chmod']);

if(isset($_GET['mod'])) {

  	$s2cms->assign("mod", $_GET['mod']);
  	$mod = validate($_GET['mod'], 120);
	switch($mod) {

        case $mod: include 'modules/'.$mod.'/view.php'; break;

	}

} else {



 
	 
	$s2cms->assign("server_name", $_SERVER['SERVER_NAME']);
    $s2cms->assign("OS", PHP_OS);
    $s2cms->assign("server_software", $_SERVER["SERVER_SOFTWARE"]);
    $s2cms->assign("php_version", phpversion());
    $s2cms->assign("mysql_version", mysql_get_server_info());
	$s2cms->assign("server_ip", $_SERVER['SERVER_ADDR']);
    $s2cms->assign("upload_max_filesize", ini_get('upload_max_filesize'));
    $post_max_size = (ini_get('file_uploads')==0) ? $lang['sys_offswitched'] : @ini_get('post_max_size');
    $s2cms->assign("post_max_size", $post_max_size);
    $free_space = disk_free_space('/');
	if( $free_space < 1024 ) $free_space = $free_space.' bytes'; elseif ( $free_space < 1048576 ) $free_space = round( ( $free_space / 1024 ), 1 ).' KB'; elseif( $free_space < 1073741824 ) $free_space = round(($free_space / 1048576),1).' MB'; else $free_space = round(($free_space / 1073741824),1).' GB';
 	$s2cms->assign("free_space", $free_space);
    $s2cms->assign("max_execution_time", ini_get('max_execution_time'));
    $s2cms->assign("remote_ip", $_SERVER['REMOTE_ADDR']);
	$s2cms->assign("page", "{$theme}/index.tpl");
	$s2cms->display("{$theme}/main.tpl");

}

?>