<?php /* Smarty version Smarty-3.0.8, created on 2012-11-04 14:43:44
         compiled from ".\templates\s2tem/pages/structure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:234565096630014abe7-40118737%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '065521b1920f037172ecd5925bad14e7c2522f93' => 
    array (
      0 => '.\\templates\\s2tem/pages/structure.tpl',
      1 => 1352033016,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '234565096630014abe7-40118737',
  'function' => 
  array (
    'cats_tree' => 
    array (
      'parameter' => 
      array (
        'parent_id' => 0,
      ),
      'compiled' => '',
    ),
    'tree' => 
    array (
      'parameter' => 
      array (
        'parent_id' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'has_nocache_code' => 0,
)); /*/%%SmartyHeaderCode%%*/?>

 <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/sitemap/sitemapstyler.js" type="text/javascript"></script>
 <link rel="stylesheet" href="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/sitemap/sitemapstyler.css" type="text/css" media="screen" />
 

 
 

<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3><?php echo $_smarty_tpl->getVariable('head')->value;?>
</h3>
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <div class="content-box-content">
    <div class="tab-content default-tab" id="tab1">
      <!-- This is the target div. id must match the href of this div's tab -->
	   <div id="list">
 <div class="notification success png_bg" id="response"> </div>
           
 <?php if (!function_exists('smarty_template_function_cats_tree')) {
    function smarty_template_function_cats_tree($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->template_functions['cats_tree']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
  <?php if (sizeof($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs'])>0){?>
    <ul id="sitemap">
    <?php  $_smarty_tpl->tpl_vars['child_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['child_id']->key => $_smarty_tpl->tpl_vars['child_id']->value){
?>
      <li  id="pos_<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['pos'];?>
" class="<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
">
	  
	 
		<?php if ($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['enabled']==0){?> 
		<a href = "/admin/structure/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['menu'];?>
/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
/" class="lamp"><img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/lamp_off.gif" alt="Не активно" title="Не активно"></a>
		<?php }else{ ?>
		<a href = "/admin/structure/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['menu'];?>
/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
/" class="lamp"><img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/lamp_on.gif" alt="Активно" title="Активно"></a>
		<?php }?>
	    <a href="/admin/structure/edit/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['menu'];?>
/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
/"><?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['name'];?>
</a>
		<div class="right">
		<img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/1312200629_move.png" alt="Переместить" border="0"> 
		<a href="/admin/structure/edit/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['menu'];?>
/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
/">&nbsp;&nbsp;<img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/pencil.png" alt="Изменить" border="0"></a>
		<a onClick="conf('/admin/structure/del/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['menu'];?>
/<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
/')" href="#">&nbsp;&nbsp;<img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/cross.png" alt="Удалить" border="0"></a>
        </div>
		<?php smarty_template_function_cats_tree($_smarty_tpl,array('parent_id'=>$_smarty_tpl->tpl_vars['child_id']->value));?>

		
		
		</li>
    <?php }} ?>
    </ul>
<?php }?><?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;}}?>

<?php smarty_template_function_cats_tree($_smarty_tpl,array('data'=>$_smarty_tpl->getVariable('cats_tree_data')->value));?>
  		  


           
      </div>
    </div>
    <!-- End #tab1 -->
  </div>
 
  <!-- End .content-box-content -->
</div>
<!-- End .content-box -->  
 
 <ul class="shortcut-buttons-set">
  <li><a href="#url" class="shortcut-button new-punkt-menu modal-window"><span class="png_bg">Создать новый пункт меню</span></a></li>
</ul>




<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->
 
<div id="boxes" class="url">
<div id="dialog" class="window">
  <h3>Добавить пункт меню</h3>
  <form name="form" method="post" action="/admin/structure/add/<?php echo $_smarty_tpl->getVariable('menu')->value;?>
/">
    <fieldset>
      <p>
        <label>Заголовок</label>
        <input name="name" class="text-input large" type="text" id="url">
      </p>
      <p>
        <label>URL</label>
        <input name="url" class="text-input large" type="text" id="trans"> 
      </p> 
       
	  

   <p>
<label>Категория</label>
 <div id="myMenu"> <label>Выбрать вариант</label>
<div class="myMenu">
	
  <?php if (!function_exists('smarty_template_function_tree')) {
    function smarty_template_function_tree($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->template_functions['tree']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
										
   <?php if (sizeof($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs'])>0){?>
    <ul id="sitemapDropBox">
    <?php  $_smarty_tpl->tpl_vars['child_id'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['child_id']->key => $_smarty_tpl->tpl_vars['child_id']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['child_id']->key;
?>			
      <li  id="<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
">
	  
	 
	 
	    <a href="javascript: void(0);"><?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['name'];?>
</a>
		
	    <?php smarty_template_function_tree($_smarty_tpl,array('parent_id'=>$_smarty_tpl->tpl_vars['child_id']->value));?>

		
		
		</li>
    <?php }} ?>
    </ul>
<?php }?><?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;}}?>

<?php smarty_template_function_tree($_smarty_tpl,array('data'=>$_smarty_tpl->getVariable('cats_tree')->value));?>
  
<input type="hidden" id="id_str" name="id_str" />
</div>
</div>
</p> 


 
	 <p>
        <label>Создать страницу</label>
        <input name="pageon" class="checkbox"  value="1" type="checkbox">
        &nbspСоздать страницу с таким названием </p>
      
        <label>Активность</label>
        <input name="enabled" class="checkbox" checked="checked" value="1" type="checkbox">
        &nbspВкл/Выкл </p>
      <p>
	     
	  

        <input type="submit" class="button" value="Добавить пункт меню">
      </p>
    </fieldset>
  </form>
  <div class="close"></div>
</div>
</div>
