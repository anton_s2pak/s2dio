<?php /* Smarty version Smarty-3.0.8, created on 2012-10-28 19:08:59
         compiled from ".\templates\s2tem/photos/addedit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9790508d66ab10ccf6-84938303%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '13b9e1b326559df5a3ba0526d37a801efd809ae7' => 
    array (
      0 => '.\\templates\\s2tem/photos/addedit.tpl',
      1 => 1351444129,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9790508d66ab10ccf6-84938303',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include 'Z:\home\s2dio.local\www\includes\lib\smarty\plugins\modifier.truncate.php';
?><form name="form" method="post" action="" id="form" enctype="multipart/form-data">
    <div class="content-box">
        <div class="content-box-header">
            <h3><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('head_title')->value,40);?>
</h3>
            <ul class="content-box-tabs">
                <li><a href="#tab1" class="default-tab">Добавить картинку</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="content-box-content">
            <div class="tab-content default-tab" id="tab1">
                <fieldset>
                    <p>
                        <label>Категория:</label>
                        <select size="8" name="id_category" class="select" multiple>
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('view_cat')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
"
                                    <?php if ($_smarty_tpl->tpl_vars['item']->value['id_item']==$_smarty_tpl->getVariable('cat')->value||$_smarty_tpl->tpl_vars['item']->value['id_item']==$_smarty_tpl->getVariable('id_category_get')->value){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</option>
                        <?php }} ?>
                        </select>
                    <p>
                        <label>Название</label>
                        <input class="text-input medium-input" type="text" value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
" id="url" name="name" autofocus />
                        <span class="info"></span></p>
                    <p>
                    <label>Картинка</label>
                <?php if ($_smarty_tpl->getVariable('image')->value){?>   <p><img src="/uploads/photos/small/<?php echo $_smarty_tpl->getVariable('image')->value;?>
" class="border_img_edit"></p> <?php }?>
                    <input name="image" type="file" class="" size="50">
                    </p>
                </fieldset>
            </div>
        </div>
        <!-- End .content-box-content -->
    </div>
    <!-- End .content-box -->
    <input type="button" class="button" onClick="Valid()" value="Сохранить">
    <input type="button" class="button" value="Отменить">
</form>