<?php /* Smarty version Smarty-3.0.8, created on 2012-10-14 16:47:27
         compiled from ".\templates\s2tem/config/config.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15563507ac26fd75a35-89345637%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea0c5df586f9d5a4b1c01bd8c5f735e864b339bf' => 
    array (
      0 => '.\\templates\\s2tem/config/config.tpl',
      1 => 1323676870,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15563507ac26fd75a35-89345637',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form name="form" method="post" action="">
  <div class="content-box">
    <div class="content-box-header">
      <h3>Настройка системы</h3>
      <ul class="content-box-tabs">
        <li><a href="#tab1" class="default-tab">Основные настройки</a></li>
        
        <li><a href="#tab2">SEO сайта</a></li>
        <li><a href="#tab3">Дополнительно</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <div class="content-box-content">
      <div class="tab-content default-tab" id="tab1">
        <fieldset>
          <p>
            <label>E-mail администратора</label>
            <input class="text-input medium-input" name="email" value="<?php echo $_smarty_tpl->getVariable('email')->value;?>
" type="text" />
            <span class="info"></span> </p>
          <p>
            <label>Количество вывода на страницу</label>
            <input class="text-input small" name="page" value="<?php echo $_smarty_tpl->getVariable('page_num')->value;?>
" type="text"  />
            <span class="info"></span> </p>
          <p>
            <label>Работа сайта</label>
            <input name="work_site" type="radio" value="1" <?php if ($_smarty_tpl->getVariable('work_site')->value==1){?>checked<?php }?>>
            Включить
            <input name="work_site" type="radio" value="0" <?php if ($_smarty_tpl->getVariable('work_site')->value==0){?>checked<?php }?>>
            Выключить</p>
          <p>
            <label>Текст при отключении сайта</label>
            <textarea   name="close_text" class="textarea" cols="70" rows="5"><?php echo $_smarty_tpl->getVariable('close_text')->value;?>
</textarea>
          </p>
        </fieldset>
      </div>
      <!-- End #tab1 -->
      <div class="tab-content" id="tab2">
        <fieldset>
          <p>
            <label>Титлы(title)</label>
            <input name="title" value="<?php echo $_smarty_tpl->getVariable('title')->value;?>
" type="text" class="text-input large-input">
          </p>
          <p>
            <label>Ключевые слова(keywords)</label>
            <textarea  name="keywords" class="textarea" cols="70" rows="5"><?php echo $_smarty_tpl->getVariable('keywords')->value;?>
</textarea>
          </p>
          <p>
            <label>Описание(description)</label>
            <textarea   name="description" class="textarea" cols="70" rows="5"><?php echo $_smarty_tpl->getVariable('description')->value;?>
</textarea>
          </p>
        </fieldset>
      </div>
      <div class="tab-content" id="tab3">
        <fieldset>
          <p>
            <label>Коды счетчиков</label>
            <textarea   name="code_counter" class="textarea" cols="70" rows="5"><?php echo $_smarty_tpl->getVariable('code_counter')->value;?>
</textarea>
          </p>
        </fieldset>
      </div>
      <div class="clear"></div>
    </div>
    <!-- End .content-box-content -->
  </div>
  <!-- End .content-box -->
  <input class="button" value="Сохранить" onclick="Config(this.form);" type="button">
  <input type="button"  class="button"   value="Отменить">
</form>
