<?php /* Smarty version Smarty-3.0.8, created on 2012-10-14 16:46:41
         compiled from ".\templates\s2tem/main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:23408507ac2413b97c4-54415925%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b2e43675f5634a0a6047d54de5586322c61aeb9' => 
    array (
      0 => '.\\templates\\s2tem/main.tpl',
      1 => 1350138286,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23408507ac2413b97c4-54415925',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--
    /*
     * @copyright	Copyright (C) 2009 - 2011. All rights reserved.
     * @
     * @CMS S2dio
     * @copyright Copyright (C) 2010 - 2011. All rights reserved.
     * @author Original by Stupak Oleg  <http://www.s2dio.com.ua/>
     * @
    */
    -->

    <title>S2 CMS - Панель управления</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/css/reset.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/css/invalid.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/jquery.js"></script>
    <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/jquery.synctranslit.js"
            type="text/javascript"></script> 
    <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript"
            src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/tiny_mce/plugins/smimage/smplugins.js"></script>
    <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/editor.js" type="text/javascript"></script>
    <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/form.js" type="text/javascript"></script>
    <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/script.js" type="text/javascript"></script>
    <script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/simpla.jquery.configuration.js"></script>

    <!-- Internet Explorer -->
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="/admin/templates/s2tem/css/ie.css" type="text/css" media="screen"/>
    <![endif]-->
    <!--[if IE 6]>
    <script type="text/javascript" src="/admin/templates/s2tem/js/DD_belatedPNG_0.0.7a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix('.png_bg, img, li');
    </script>
    <![endif]-->

</head>
<?php echo $_smarty_tpl->getVariable('themewwww')->value;?>

<body>

<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

<div id="sidebar">
    <div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

        <h1 id="sidebar-title"><a href="/">S2dio CMS - Панель управления</a></h1>

        <!-- Logo (221px wide) -->
        <a href="/admin"><img id="logo" src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/logocms.png" alt="S2dio.com.ua"/></a>

        <!-- Sidebar Profile links -->
        <div id="profile-links">



            Привет <a href="/admin/login/edit/<?php echo $_smarty_tpl->getVariable('id_ses')->value;?>
/"><?php echo $_smarty_tpl->getVariable('admin_ses')->value;?>
</a>,<?php if ($_smarty_tpl->getVariable('chmod_ses')->value==1){?> <br> вы являетесь
            администратором<br/><?php }?>
            <div class="jclock"></div>
            <br/>
            <a href="/admin/">Админ-панель</a> | <a href="/" target="_blank" title="View the Site">Перейти на сайт</a> | <a href="/admin/logout/"
                                                                                       title="Sign Out">Выход</a>
        </div>

        <ul id="main-nav">  <!-- Accordion Menu -->
            <li>

                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="pages"){?>current<?php }?>">
                    <!-- Add the class "current" to current menu item -->
                    Страницы и меню
                </a>
                <ul>
                    <li><a href="/admin/structure/1/">Меню</a></li>
                    <li><a href="/admin/pages/">Страницы</a></li>
                    <!-- Add class "current" to sub menu items also -->


                </ul>

            </li>
<?php if ($_smarty_tpl->getVariable('chmod_ses')->value==1){?>
            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="news"){?>current<?php }?>">
                    Новости
                </a>
                <ul>
                    <li><a href="/admin/news/">Редактировать новости</a></li>
                    <li><a href="/admin/news/add/">Добавить новость</a></li>
                </ul>
            </li>

            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="articles"){?>current<?php }?>">
                    Статьи
                </a>
                <ul>
                    <li><a href="/admin/articles/">Редактировать статьи</a></li>
                    <li><a href="/admin/articles/add/">Добавить статьи</a></li>

                </ul>
            </li>

            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="catalog"){?>current<?php }?>">
                    Каталог
                </a>
                <ul>
                    <li><a href="/admin/catalog/">Редактировать каталог</a></li>
                    <li><a href="/admin/catalog/add/">Добавить в каталог</a></li>
                </ul>
            </li>
<?php }?>
            <li>
                <a href="#" class="nav-top-item  <?php if ($_smarty_tpl->getVariable('mod')->value=="photos"){?>current<?php }?>">
                    Фотогалерея
                </a>
                <ul>
                    <li><a href="/admin/photos/">Редактировать галерею</a></li>
                    <li><a href="/admin/photos/add/">Добавить в галерею</a></li>
                </ul>
            </li>
<?php if ($_smarty_tpl->getVariable('chmod_ses')->value==1){?>
            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="faq"){?>current<?php }?>">
                    Вопрос-Ответ
                </a>
                <ul>
                    <li><a href="/admin/faq/" class="current">Просмотр</a></li>

                </ul>
            </li>


            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="comments"){?>current<?php }?>">
                    Комментарии
                </a>
                <ul>
                    <li><a href="/admin/comments/" class="current">Просмотр</a></li>

                </ul>
            </li>


            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="banners"){?>current<?php }?>">
                    Баннера
                </a>
                <ul>
                    <li><a href="/admin/banners/">Редактировать баннер</a></li>
                    <li><a href="/admin/banners/add/">Добавить баннер</a></li>
                </ul>
            </li>
<?php }?>
            <li>
                <a href="#" class="nav-top-item <?php if ($_smarty_tpl->getVariable('mod')->value=="config"){?>current<?php }?>">
                    Управление системой
                </a>
                <ul>
                    <?php if ($_smarty_tpl->getVariable('chmod_ses')->value==1){?><li><a href="/admin/login/view/">Пользователи</a></li><?php }?>
                    <li><a href="/admin/config/"> Настройка системы</a></li>
                    <?php if ($_smarty_tpl->getVariable('chmod_ses')->value==1){?><li><a href="/admin/mypicture/">Файловый менеджер</a></li><?php }?>
                    <li><a href="/admin/styles/">Редактировать стили</a></li>
                </ul>
            </li>

            <!--li>
                       <a href="#upload" class="nav-top-item no-submenu modal-window">
                           Загрузить фотографию
                       </a>
                   </li-->

        </ul>
        <!-- End #main-nav -->


        <!-- Модальное окно фото -->
        <div id="boxes" class="upload">
            <div id="dialog" class="window">
                <h3>Загрузить фотографию</h3>

                <form action="/admin/uploads/" method="post" enctype="multipart/form-data" target="_blank"
                      style="padding-top: 5px">
                    <center>
                        <input name="file" type="file"><br><br>
                        <input name="Submit" type="button" class="button" onclick="UploadsFiles(this.form);"
                               value="Загрузить">
                    </center>
                </form>
                <div class="close"></div>
            </div>
        </div>
        <div id="maska"></div>


    </div>
</div>
<!-- End #sidebar -->

<div id="main-content"> <!-- Main Content Section with everything -->

<noscript> <!-- Show a notification if the user has disabled javascript -->
    <div class="notification error png_bg">
        <div>
            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/"
                                                                                  title="Upgrade to a better browser">upgrade</a>
            your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852"
                               title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface
            properly.
        </div>
    </div>
</noscript>
<div id="allinfo"></div>
<?php $_template = new Smarty_Internal_Template($_smarty_tpl->getVariable('page')->value, $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>








<div id="footer">
    <small>
        &#169; Copyright 2011 S2 CMS | Powered by <a href="http://www.s2dio.com.ua">www.s2dio.com.ua</a> | <a href="#">Top</a>
    </small>
</div>
<!-- End #footer -->

</div>
<!-- End #main-content -->

</div>
</body>

</html>
