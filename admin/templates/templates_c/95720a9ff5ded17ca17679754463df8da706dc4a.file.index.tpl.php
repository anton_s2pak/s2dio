<?php /* Smarty version Smarty-3.0.8, created on 2012-10-14 16:46:41
         compiled from ".\templates\s2tem/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16105507ac2417a42b4-24966897%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95720a9ff5ded17ca17679754463df8da706dc4a' => 
    array (
      0 => '.\\templates\\s2tem/index.tpl',
      1 => 1350138527,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16105507ac2417a42b4-24966897',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<!-- Page Head -->
			<h2>Добро пожаловать <?php echo $_smarty_tpl->getVariable('admin_ses')->value;?>
</h2>
			<p id="page-intro">С чего вы хотите начать?</p>


    <div class="content-box column-right"><!-- Start Content Box -->

        <div class="content-box-header">

            <h3>Информация</h3>

        </div> <!-- End .content-box-header -->

        <div class="content-box-content">

            <div class="tab-content default-tab">


                <table width="100%" bgcolor="#e3e6e8" border="0" cellspacing="1" cellpadding="1" id="table">
                    <tr class="table_rows">

                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Имя хоста:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('server_name')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">IP-адрес сервера:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('server_ip')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Операционная система:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('OS')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Установленое ПО:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('server_software')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Версия PHP:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('php_version')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Версия MySQL:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('mysql_version')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Максимальный размер загрузки:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('upload_max_filesize')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Максимальный размер загрузки через POST:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('post_max_size')->value;?>
</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Размер свободного места на диске:</td>
                        <td width="50%">
                        <?php echo $_smarty_tpl->getVariable('free_space')->value;?>

                        </td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Максимальное время исполнения:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('max_execution_time')->value;?>
 сек</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Ваш IP-адрес:</td>
                        <td width="50%"><?php echo $_smarty_tpl->getVariable('remote_ip')->value;?>
</td>
                    </tr>
                </table>

            </div> <!-- End #tab3 -->

        </div> <!-- End .content-box-content -->

    </div> <!-- End .content-box -->



			<ul class="shortcut-buttons-set">

				<li><a class="shortcut-button new-page" href="/admin/pages/add/"><span class="png_bg">
					Создать страницу
				</span></a></li>
				
				<li><a class="shortcut-button upload-image" href="/admin/photos/add/"><span class="png_bg">
					Загрузить рисунок
				</span></a></li>
				
				<li><a class="shortcut-button add-event" href="/admin/structure/1/"><span class="png_bg">
					Подключить страницу
				</span></a></li>

			</ul><!-- End .shortcut-buttons-set -->
			
			<div class="clear"></div> <!-- End .clear -->


