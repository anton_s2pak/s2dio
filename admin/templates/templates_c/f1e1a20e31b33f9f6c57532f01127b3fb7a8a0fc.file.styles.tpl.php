<?php /* Smarty version Smarty-3.0.8, created on 2012-11-07 21:08:30
         compiled from ".\templates\s2tem/config/styles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9621509ab1ae848619-96563126%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f1e1a20e31b33f9f6c57532f01127b3fb7a8a0fc' => 
    array (
      0 => '.\\templates\\s2tem/config/styles.tpl',
      1 => 1323676870,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9621509ab1ae848619-96563126',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include 'Z:\home\s2dio.local\www\includes\lib\smarty\plugins\modifier.escape.php';
?><script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/codemirror/js/codemirror.js" type="text/javascript" charset="utf-8"></script>


 <div class="content-box">
    <div class="content-box-header">
      <h3>Редактируем стиль "<?php echo $_smarty_tpl->getVariable('CurrentStyle')->value['filename'];?>
"</h3>
       
      <div class="clear"></div>
    </div> </div>
   
  
      <div id="cont_center">
<div id="cont_right">
<?php if ($_smarty_tpl->getVariable('Error')->value){?>
<div id="error_minh">
  <div id="error"> <img src="./images/error.jpg" alt=""/>
    <p><?php echo $_smarty_tpl->getVariable('Error')->value;?>
</p>
  </div>
</div>
<?php }?>
 
<form name='template' method="post">
<input type=hidden name=filename value='<?php echo $_smarty_tpl->getVariable('CurrentStyle')->value['filename'];?>
'>
<div style="background-color:#f0f0f0; border: 1px solid #e0e0e0; padding: 3px; text-align:right;"> <span class=tovar_on style='float:left'><?php echo $_smarty_tpl->getVariable('themes')->value;?>
</span>
  <input type=submit value='Сохранить' class="button">
  <div style='background-color:#ffffff; border: 1px solid #e0e0e0; padding: 0px;width:700px; height:1000px;'>
    <textarea id="code" name=content  style='word-wrap:normal;width:700px; height:1000px;'><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('Content')->value);?>
</textarea>
  </div>
</div>
</div>
<div id="cont_left">
<h3>Стили</h3>
  <ul>
    <?php  $_smarty_tpl->tpl_vars['style'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('Styles')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['style']->key => $_smarty_tpl->tpl_vars['style']->value){
?>
    <li class="filename"> <a href="<?php echo $_smarty_tpl->tpl_vars['style']->value['edit_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['style']->value['name'];?>
</a> <br>
      <span class=filename_small><?php echo $_smarty_tpl->tpl_vars['style']->value['name'];?>
</span> </li>
    <?php }} ?>
  </ul>
</div>
</div>
    
      
 







<script type="text/javascript">
 
  var editor = CodeMirror.fromTextArea('code', {
    height: "350px",
    parserfile: ["parsecss.js"],
    stylesheet: ["/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/codemirror/css/csscolors.css"],
    path: "/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/codemirror/js/",
    dumbTabs: true,
    saveFunction: function() { 
      window.document.template.submit();
    },
    textWrapping: true
  });
</script>
