<?php /* Smarty version Smarty-3.0.8, created on 2012-11-10 09:01:18
         compiled from ".\templates\s2tem/photos/edit_cat.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26743509dfbbec379b9-26752505%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5e6b6f2cd011aa2e790b7356043f1a6e43afde2d' => 
    array (
      0 => '.\\templates\\s2tem/photos/edit_cat.tpl',
      1 => 1352053822,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26743509dfbbec379b9-26752505',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form id="form" method="post" action="" enctype="multipart/form-data">
  <div class="content-box">
    <div class="content-box-header">
      <h3>Изменить название раздела</h3>
      <div class="clear"></div>
    </div>
    <div class="content-box-content">
      <div class="tab-content default-tab" id="tab1">
        <fieldset>
          <p>
            <label>Заголовок</label>
            <input class="text-input medium-input" type="text"  value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
" id="url" name="name" />
            <span class="info"></span> </p>
          <p>
            <label>Url</label>
             <input name="url" class="text-input large" type="text" id="trans" value="<?php echo $_smarty_tpl->getVariable('url')->value;?>
"> 
          </p>
          <p>
            <label>Порядок</label>
            <?php if ($_smarty_tpl->getVariable('id_item')->value!=1&$_smarty_tpl->getVariable('id_item')->value!=2&$_smarty_tpl->getVariable('id_item')->value!=3){?><input type="text" class="text-input order" style="width: 50px; text-align: center;" value="<?php echo $_smarty_tpl->getVariable('order')->value;?>
" id="order" name="order" maxlength="3" /><?php }?>
          </p>
          <p>
            <label>Изображение раздела</label>
            <?php if ($_smarty_tpl->getVariable('image')->value){?><img src="/uploads/photos/small/<?php echo $_smarty_tpl->getVariable('image')->value;?>
" class="border_img_edit"><?php }?>
            <input name="image" type="file" class="" size="50">
          </p>
          <p>
            <label>Описание раздела</label>
            <textarea id="edit_two"  name="text" cols="70" rows="25"><?php echo $_smarty_tpl->getVariable('text')->value;?>
</textarea>
          </p>
        </fieldset>
      </div>
      <!-- End #tab1 -->
    </div>
    <!-- End .content-box-content -->
  </div>
  <!-- End .content-box -->
  <input type="button" onclick="Valid(true);" class="button" value="Сохранить">
  <input type="button"  class="button" onclick="window.location.href='/admin/photos/';return false;"  value="Отменить">
</form>
