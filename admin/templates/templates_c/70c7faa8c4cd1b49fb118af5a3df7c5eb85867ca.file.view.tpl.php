<?php /* Smarty version Smarty-3.0.8, created on 2012-10-20 19:34:41
         compiled from ".\templates\s2tem/photos/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:203825082d2a130aee1-47508072%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70c7faa8c4cd1b49fb118af5a3df7c5eb85867ca' => 
    array (
      0 => '.\\templates\\s2tem/photos/view.tpl',
      1 => 1350750880,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '203825082d2a130aee1-47508072',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="content-box">
    <!-- Start Content Box -->
    <div class="content-box-header"><h3>Фотогалерея - Просмотр</h3>
        <div class="clear"></div>
    </div>
<?php if ($_smarty_tpl->getVariable('num')->value<=0){?>
</div>
<div align="center" style="color: red; font-size: 16px">На даный момент этот раздел пуст</div>
<ul class="shortcut-buttons-set">
    <li><a href="/admin/photos/add/<?php echo $_smarty_tpl->getVariable('cat')->value;?>
/" class="shortcut-button new-article modal-window"><span class="png_bg">Добавить фото в галерею</span></a></li>
</ul>
<?php }else{ ?>
<div class="content-box-content">
    <div id="table">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <colgroup>
                <col>
                <col width="45%">
                <col width="1">
                <col width="1">
            </colgroup>
        <thead>
            <tr>
                <th>Название</th>
                <th style="text-align: center">Картинка</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('view')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <tr align="center" bgcolor="#efefef">
                <td style="text-align: left;"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</td>
                <td style="padding: 5px"><img
                        border="0" src="/uploads/photos/small/<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
" width="54"></td>
                <td><a href="/admin/photos/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_category'];?>
/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/"><img
                        src="/admin/templates/s2tem/images/icons/pencil.png" alt="Изменить" border="0"></a></td>
                <td><a onClick="conf('/admin/photos/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/')" href="#"><img
                        src="/admin/templates/s2tem/images/icons/cross.png" alt="Удалить" border="0"></a></td>
            </tr>
        <?php }} ?>
        </tbody>
    </table>
</div></div></div>
<ul class="shortcut-buttons-set">
    <li><a href="/admin/photos/add/<?php echo $_smarty_tpl->getVariable('item')->value['id_category'];?>
/" class="shortcut-button new-article modal-window"> <span class="png_bg"> Добавить фото в галерею</span></a></li>
</ul>
<?php }?>
<div class="clear"></div>
