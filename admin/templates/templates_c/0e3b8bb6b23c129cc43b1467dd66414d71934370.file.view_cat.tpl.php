<?php /* Smarty version Smarty-3.0.8, created on 2012-11-04 20:30:49
         compiled from ".\templates\s2tem/photos/view_cat.tpl" */ ?>
<?php /*%%SmartyHeaderCode:273705096b45986a770-23794422%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e3b8bb6b23c129cc43b1467dd66414d71934370' => 
    array (
      0 => '.\\templates\\s2tem/photos/view_cat.tpl',
      1 => 1352053831,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '273705096b45986a770-23794422',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="content-box">
    <!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Фотогалерея</h3>

        <div class="clear"></div>
    </div>
    <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <!-- This is the target div. id must match the href of this div's tab -->
            <div id="table">
                <table>
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th style="text-align: center">Кол-во</th>
                            <th style="text-align: center">Картинка</th>
                            <th style="text-align: center">Порядок</th>
                            <th style="text-align: center">Дата</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('view_cat')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                        <tr>
                            <td><a href="/admin/photos/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></td>
                            <td style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['item']->value['num'];?>
</td>
                            <td style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['item']->value['image']){?><a href="/admin/photos/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/"><img
                                    src="/uploads/photos/small/<?php echo $_smarty_tpl->tpl_vars['item']->value['image'];?>
" width="54" height="54"></a><?php }?></td>
                            <td style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['item']->value['id_item']!=1&$_smarty_tpl->tpl_vars['item']->value['id_item']!=2&$_smarty_tpl->tpl_vars['item']->value['id_item']!=3){?><?php echo $_smarty_tpl->tpl_vars['item']->value['order'];?>
<?php }?></td>
                            <td style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['item']->value['date'];?>
</td>
                            <td style="text-align: center"><!-- Icons -->
                                <a href="/admin/photos/cat/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/" title="Редактировать"><img
                                        src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/pencil.png"
                                        alt="Редактировать"/></a> <?php if ($_smarty_tpl->tpl_vars['item']->value['id_item']!=1&$_smarty_tpl->tpl_vars['item']->value['id_item']!=2&$_smarty_tpl->tpl_vars['item']->value['id_item']!=3){?>
                                    <a onClick="conf('/admin/photos/cat/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/')" href="#"
                                       title="Удалить"><img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/cross.png"
                                                            alt="Удалить"/></a><?php }?></td>
                        </tr>
                        <?php }} ?>
                        </tbody>
                </table>
            </div>
        </div>
        <!-- End #tab1 -->
    </div>
    <!-- End .content-box-content -->
</div>
<ul class="shortcut-buttons-set">
    <li><a href="#menuadd" class="shortcut-button news-menu modal-window"> <span
            class="png_bg"> Создать новую галерею </span></a></li>
    <li><a href="/admin/photos/add/" class="shortcut-button new-article modal-window"> <span class="png_bg"> Добавить фото в галерею</span></a>
    </li>
</ul>
<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->
<!-- Модальное окно-->
<div id="boxes" class="menuadd">
    <div id="dialog" class="window">
        <h3>Добавить раздел</h3>
        <form name="form" id="form" method="post" action="/admin/photos/cat/add/" enctype="multipart/form-data">
            <fieldset>
                <p>
                    <label>Название</label>
                    <input name="name" type="text" class="text-input large" id="url">
                    <span class="info"></span>
                </p>
                <p>
                    <label>Url</label>
                    <input name="url" class="text-input large" type="text" id="trans">
                </p><?php if ($_smarty_tpl->getVariable('item')->value['id_item']!=1&$_smarty_tpl->getVariable('item')->value['id_item']!=2&$_smarty_tpl->getVariable('item')->value['id_item']!=3){?>
                <p>
                    <label>Порядок</label>
                    <input type="text" class="text-input order" style="width: 50px; text-align: center;" value="" name="order" maxlength="3" />
                </p>
                <?php }else{ ?>
                    <input type="hidden" value="999" />
                <?php }?>
                <p>
                    <label>Изображение</label>
                    <input type="file" size="50" name="image"></p>
                <p>
                    <label>Описание</label>
                    <textarea id="edit_two" name="text" cols="70" rows="25"></textarea>
                </p>

                <input type="submit" onclick="Valid(true); return false;" class="button" value="Добавить галерею">
                </p>
            </fieldset>
        </form>
        <div class="close"></div>
    </div>
</div>