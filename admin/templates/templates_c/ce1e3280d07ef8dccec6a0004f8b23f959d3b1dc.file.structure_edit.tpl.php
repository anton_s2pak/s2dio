<?php /* Smarty version Smarty-3.0.8, created on 2012-10-20 19:15:19
         compiled from ".\templates\s2tem/pages/structure_edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:225115082ce170c0af5-44942602%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce1e3280d07ef8dccec6a0004f8b23f959d3b1dc' => 
    array (
      0 => '.\\templates\\s2tem/pages/structure_edit.tpl',
      1 => 1349632156,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '225115082ce170c0af5-44942602',
  'function' => 
  array (
    'cats_tree' => 
    array (
      'parameter' => 
      array (
        'parent_id' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'has_nocache_code' => 0,
)); /*/%%SmartyHeaderCode%%*/?>

<script src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/sitemap/sitemapstyler.js" type="text/javascript"></script>
<link rel="stylesheet" href="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/js/sitemap/sitemapstyler.css" type="text/css" media="screen"/>


<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Изменить меню</h3>
        <div class="clear"></div>
    </div>
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <form name="form" method="post" action="">
                <fieldset>
                    <p>
                        <label>Заголовок</label>
                        <input class="text-input medium-input" type="text" id="url" name="name" value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
"/>
                    </p>
                    <p>
                        <label>Url</label>
                        <input class="text-input medium-input" type="text" id="trans" name="url" value="<?php echo $_smarty_tpl->getVariable('url')->value;?>
"/>
                    </p>


                    <p>
                        <label>Категория</label>


                    <div id="myMenu"><label><?php if ($_smarty_tpl->getVariable('parent')->value){?><?php echo $_smarty_tpl->getVariable('parentName')->value;?>
<?php }else{ ?> Выбрать вариант <?php }?></label>

                        <div class="myMenu">
                        <?php if (!function_exists('smarty_template_function_cats_tree')) {
    function smarty_template_function_cats_tree($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->template_functions['cats_tree']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
                            <?php if (sizeof($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs'])>0){?>
                                <ul id="sitemapDropBox">
                                    <?php  $_smarty_tpl->tpl_vars['child_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['child_id']->key => $_smarty_tpl->tpl_vars['child_id']->value){
?>

                                        <li id="<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
">
                                            <a href="javascript: void(0);" <?php if ($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item']==$_smarty_tpl->getVariable('id')->value){?>
                                               class="disabled" <?php }?>> <?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['name'];?>
</a>
                                            <?php smarty_template_function_cats_tree($_smarty_tpl,array('parent_id'=>$_smarty_tpl->tpl_vars['child_id']->value));?>

                                        </li>
                                    <?php }} ?>
                                </ul>
                            <?php }?><?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;}}?>

                        <?php smarty_template_function_cats_tree($_smarty_tpl,array('data'=>$_smarty_tpl->getVariable('cats_tree_data')->value));?>

                            <input type="hidden" id="id_str" name="id_str" value="<?php echo $_smarty_tpl->getVariable('parent')->value;?>
" />

                        </div>

                    </div>
                    </p>

                    <p>
                        <label>Активность</label>
                        <input type="checkbox" name="enabled" <?php if ($_smarty_tpl->getVariable('enabled')->value==1){?>checked<?php }?> value="1"/> Вкл/Выкл
                    </p>

                    <p>
                        <input class="button" type="submit" value="Сохранить изменения"/>
                    </p>

                </fieldset>

                <div class="clear"></div>

            </form>

        </div>


    </div>

</div>
			
			
		
			
 