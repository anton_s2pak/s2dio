<?php /* Smarty version Smarty-3.0.8, created on 2012-10-28 20:30:36
         compiled from ".\templates\s2tem/pages/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21500508d79ccdc3408-46353602%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '148038897d086aa6e04138bf35212840508f1aa2' => 
    array (
      0 => '.\\templates\\s2tem/pages/view.tpl',
      1 => 1350137583,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21500508d79ccdc3408-46353602',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
 
  
<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3>Редактировать страницы</h3>
    <!--<<ul class="content-box-tabs">
						li><a href="#tab1" class="default-tab">Table</a></li>   
						<li><a href="#tab2">Forms</a></li>
					</ul>-->
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <div class="content-box-content">
    <div class="tab-content default-tab" id="tab1">
	  <form name="form" method="post" action="">
      
      <div  id="table">
        <table>
          <thead>
            <tr>
              <th><input class="check-all" type="checkbox" /></th>
              <th>ID</th>
              <th>Заголовок</th>
              <th>Url</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          
          <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('view')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
          <tr>
            <td><input type="checkbox" name="type[]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
"/></td>
            <td><?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
</td>
            <td><a href="/admin/pages/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/" title="title"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></td>
            <td><?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
</td>
            <td><!-- Icons -->
              <a href="/admin/pages/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/" title="Редактироание"><img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/pencil.png" alt="Редактироание" /></a>
			  <a onClick="conf('/admin/pages/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_item'];?>
/')" href="#" title="Удалить"><img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/cross.png" alt="Удалить" /></a>
			  <a href="#" title="Edit Meta"><img src="/admin/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a></td>
          </tr>
          <?php }} ?>
        </table>
      </div>
    </div>
 </form>
  </div>
  <!-- End .content-box-content -->
</div>
<!-- End .content-box -->


<ul class="shortcut-buttons-set">
  <li><a class="shortcut-button new-page"href="/admin/pages/add/"><span class="png_bg"> Создать страницу </span></a></li>
 
    
</ul>
<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->
