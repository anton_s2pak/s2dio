<form name="form" method="post" action=""  id="form">
  <div class="content-box">
    <div class="content-box-header">
      <h3>{$head_title|truncate:40}{if $indexPage eq 1} - Назначена главной страницей{/if}</h3>
      <ul class="content-box-tabs">
        <li><a href="#tab1" class="default-tab">Добавить страницу</a></li>
        <!-- href must be unique and match the id of target div -->
        <li><a href="#tab2">SEO страницы</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <div class="content-box-content">
      <div class="tab-content default-tab" id="tab1">
        <fieldset>
          <p>
            <label>Заголовок</label>
            <input class="text-input medium-input" type="text"  value="{$name}" id="url" name="name" />
            <span class="info"></span> </p>
          <p>
            <label>Текст</label>
            <textarea id="edit"  name="text" cols="70" rows="25">{$text}</textarea>
 
          </p>
          
          <p>
				<label>Будет главной страницей</label>
			<input name="page" type="radio" value="1" {if $indexPage eq 1}checked{/if} />
            Да &nbsp;&nbsp;
            <input name="page" type="radio" value="0" {if $indexPage eq 0}checked{/if} />
            Нет </p>
        </fieldset>
      </div>
      <!-- End #tab1 -->
      
      <div class="tab-content" id="tab2">
        <fieldset>
		<p>
            <label>URL&nbsp; <a href="#urls" class="modal-window"> ? </a></label>
            <input name="url" type="text" class="text-input medium-input" type="text" value="{$url}" id="trans">.html
          </p>
          <p>
            <label>Титлы&nbsp;(title) <a href="#title" class="modal-window"> ? </a></label>
            <input name="title" type="text" class="text-input large-input" type="text" value="{$title}" id="large-input">
          </p>
          <p>
            <label>Ключевые слова&nbsp;(keywords) <a href="#keywords" class="modal-window"> ? </a></label>
            <textarea name="keywords" class="textarea">{$keywords}</textarea>
          </p>
          <p>
            <label>Описание&nbsp;(description) <a href="#description" class="modal-window"> ? </a></label>
            <textarea name="description" class="textarea">{$description}</textarea>
          </p>
        </fieldset>
        
        
        <div class="clear"></div>
        <!-- End .clear --> 
        
      </div>
      <!-- End #tab2 --> 
      
    </div>
    <!-- End .content-box-content --> 
    
  </div>
  <!-- End .content-box -->
  <input type="button"  class="button" onClick="Valid()" value="Сохранить">   <input type="button"  class="button"   value="Отменить">
</form>
<div id="boxes" class="urls">
<div id="dialog" class="window"  style="width:450px">
  <h3>Uniform Resource Locator</h3>
 <p>УРЛ (URL, Uniform Resource Locator) - уникальный адрес страницы, может состоять только из букв латинского алфавита, чисел и некоторых символов. </p>
 <p>1. <strong>Длина url не более 3-5 слов</strong>.</p>
<p>В интервью Matt Catts (Специализируется на проблемах поисковой оптимизации Google Inc) заявил, что при использовании в урле более 5 слов, «как правило <strong>вес этих слов будет занижен</strong>»<br>
  Еще один довод в пользу коротких урлов: <a rel="nofollow" title="http://searchengineland.com/080515-084124.php" target="_blank" href="http://3pu.info/go/http://searchengineland.com/080515-084124.php">недавнее исследование</a> показало, что в результатах поисковой выдачи <strong>кликабельность</strong> коротких урлов была в 2 раза выше длинных.<br>
  Короткий урл также удобен при прямом наборе с клавиатуры и легче запоминается.</p>
<p>2. <strong>Дефис</strong> лучше, чем <strong>нижнее подчеркивание</strong>.</p>
<p>Google не имеет предпочтений к одному из типов разделения слов в урле. Но дефис лучше в том плане, что Google <a rel="nofollow" title="http://www.mattcutts.com/blog/dashes-vs-underscores/" target="_blank" href="http://3pu.info/go/http://www.mattcutts.com/blog/dashes-vs-underscores/">видит</a> <strong>каждое слово в url как индивидуальное</strong>.</p>
<p>«Т.е если урл будет вида слово1_слово2, то Google будет выдавать   страницу по запросу «слово1_слово2». А если урл вида слово1-слово2, то   страница будет выдаваться как по запросу «слово1», так и по запросу   «слово2» »</p>

 <div class="close"></div>
  </div>
</div>
<div id="boxes" class="title">
<div id="dialog" class="window"  style="width:450px">
  <h3>Title</h3>
  <p>Title – 50-80 знаков (обычно – 75) </p>
  <p> Трудно переоценить значимость тега title, по сути это самый важный тег на странице. От правильного составленного тега title будет зависеть не только успех продвижения но и количество переходов с поисковых систем. Ведь именно содержимое этого тега показывается в результате выдачи поисковых систем. И от его привлекательности будет зависеть обратят на него внимание пользователи и перейдут на сайт или нет. </p>
  <p>Стремитесь к тому что бы сделать тег title уникальным во всей выдаче поисковых систем. Что это значит? Это значит чтоб больше ни у кого не было точно такого же описания в title. Смотрите пример такого неуникального описания. </p>
  <p>Старайтесь не повторять в тайтле одно ключевое слово больше двух раз. Кроме того на каждой странице вашего сайта должно быть разное содержимое тега title. Это тоже часто встречаемая ошибка когда часть страниц (иногда и все страницы) сайта имеют одинаковое содержание этого тега. Частенько поисковые системы принимают сайты с одинаковыми title и description за автоматическими сгенерированные скриптами говносайты. И выкидывают их из своего индекса, либо изначально не индексируют такие страницы или сайты. </p>
 <div class="close"></div>
  </div>
</div>
<div id="boxes" class="keywords">
<div id="dialog" class="window"  style="width:450px">
  <h3>Keywords</h3>
  <p>Keywords - до 250 (250 – максимум, ориентируйтесь на ударные первые 150 знаков).</p>
  <p><b>Keywords</b> – ключевые слова. О длине было сказано выше. Должен состоять из ключевых слов и фраз, желательно без повторений (не более 3 раз). Например, писать такой ряд ключевых слов: свадебные платья, свадебные букеты, свадебные костюмы, свадебные аксессуары и т.д. – просто не имеет особого смысла. Вы повторили много раз прилагательное «свадебный» – потеряли кучу места, которое могли бы использовать для других слов, например, названий других предлагаемых товаров, уникальных предложений.</p>
  <p>Да, кстати об уникальности. Опять таки, как и с тегом Title – желательно проследить за тем, чтоб ключевые слова отличались для каждой конкретной страницы.</p>
  <p>Не брезгуйте упомянуть в ключевых словах какое-нибудь редкое слово, название редкого товара, который есть только у вас. Другими словами. не пренебрегайте низкочастотными запросами. Возможно не все ищут «бабочек капустниц» для украшения свадебного лимузина, но те, кто ищут – уж точно попадут к вам, особенно если вы не забудете указать эти слова в Title и метатегах.</p>
 <div class="close"></div>
  </div>
</div>
<div id="boxes" class="description">
<div id="dialog" class="window" style="width:450px">
  <h3>Description</h3>
  <p>Description – описание страницы. Лично я всегда воспринимала Description, как немного более развернутый Title. То есть, опять таки, минимум открытой рекламы, максимум информативности, соответствующая плотность ключевых слов, уникальность для каждой страницы.
  <p>Старайтесь не повторять в тайтле одно ключевое слово больше двух раз. Кроме того на каждой странице вашего сайта должно быть разное содержимое тега title. Это тоже часто встречаемая ошибка когда часть страниц (иногда и все страницы) сайта имеют одинаковое содержание этого тега. Частенько поисковые системы принимают сайты с одинаковыми title и description за автоматическими сгенерированные скриптами говносайты. И выкидывают их из своего индекса, либо изначально не индексируют такие страницы или сайты. </p>
 <div class="close"></div>
  </div>
</div>
 