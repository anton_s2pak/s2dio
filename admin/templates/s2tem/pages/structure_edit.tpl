{*-----------------------------------------------Скрипт и стиль деревовидного меню------------------------------------------*}
<script src="/admin/templates/{$theme}/js/sitemap/sitemapstyler.js" type="text/javascript"></script>
<link rel="stylesheet" href="/admin/templates/{$theme}/js/sitemap/sitemapstyler.css" type="text/css" media="screen"/>


<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Изменить меню</h3>
        <div class="clear"></div>
    </div>
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <form name="form" method="post" action="">
                <fieldset>
                    <p>
                        <label>Заголовок</label>
                        <input class="text-input medium-input" type="text" id="url" name="name" value="{$name}"/>
                    </p>
                    <p>
                        <label>Url</label>
                        <input class="text-input medium-input" type="text" id="trans" name="url" value="{$url}"/>
                    </p>


                    <p>
                        <label>Категория</label>


                    <div id="myMenu"><label>{if $parent}{$parentName}{else} Выбрать вариант {/if}</label>

                        <div class="myMenu">


                        {*-----------------------------------------------Вывод рекурсивного меню----------------------------------------------------*}
                        {function name=cats_tree parent_id=0}
                            {if $data[$parent_id].childs|@sizeof > 0}
                                <ul id="sitemapDropBox">
                                    {foreach $data[$parent_id].childs as $child_id}

                                        <li id="{$data[$child_id].id_item}">
                                            <a href="javascript: void(0);" {if $data[$child_id].id_item == $id}
                                               class="disabled" {/if}> {$data[$child_id].name}</a>
                                            {call name=cats_tree parent_id=$child_id}
                                        </li>
                                    {/foreach}
                                </ul>
                            {/if}
                        {/function}
                        {call name=cats_tree data=$cats_tree_data}
                        {*-----------------------------------------------Вывод рекурсивного меню----------------------------------------------------*}
                            <input type="hidden" id="id_str" name="id_str" value="{$parent}" />

                        </div>

                    </div>
                    </p>

                    <p>
                        <label>Активность</label>
                        <input type="checkbox" name="enabled" {if $enabled == 1}checked{/if} value="1"/> Вкл/Выкл
                    </p>

                    <p>
                        <input class="button" type="submit" value="Сохранить изменения"/>
                    </p>

                </fieldset>

                <div class="clear"></div>

            </form>

        </div>


    </div>

</div>
			
			
		
			
 