
<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3>Все меню</h3>
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <div class="content-box-content">
    <div class="tab-content default-tab" id="tab1">
      <!-- This is the target div. id must match the href of this div's tab -->
      <div  id="table">
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Название меню</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
           
          <tbody>
 {*--------------------------------------------------------Вывод всех меню------------------------------------------------------*}           
          {foreach from=$view_cat key=key item=item}
          <tr>
            <td>{$item.id_menu}</td>
            <td><a href="/admin/structure/{$item.id_menu}/" title="title">{$item.name}</a></td>
            <td><!-- Icons -->
              <a href="/admin/structure/{$item.id_menu}/" title="Редактировать"><img src="/admin/templates/{$theme}/images/icons/pencil.png" alt="Редактировать" /></a> 
              <a onClick="conf('/admin/structure/menu_del/{$item.id_menu}/')" href="#" title="Удалить"><img src="/admin/templates/{$theme}/images/icons/cross.png" alt="Удалить" /></a></td>
          </tr>
          {/foreach}
          </tbody>
          
        </table>
      </div>
    </div>
    <!-- End #tab1 -->
  </div>
  <!-- End .content-box-content -->
</div>
<!-- End .content-box -->
{*-----------------------------------------------Модальное окно Добавить меню----------------------------------------------------*}  
<ul class="shortcut-buttons-set">
  <li><a href="#menuadd" class="shortcut-button new-menu modal-window"> <span class="png_bg"> Создать новое меню </span></a></li>
 
</ul>
<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->


<!-- Модальное окно-->
<div id="boxes" class="menuadd">
<div id="dialog" class="window">
  <h3>Добавить меню</h3>
  
  <form name="form" method="post" action="/admin/structure/menu/">
    <fieldset>
<p>
        <label>Введите название меню</label>
    <input name="menu" type="text" class="text-input large">
	 </p>
	  <p>
    <input type="submit" class="button" value="Добавить меню">
	
	</p>
	  </fieldset>
  </form>
    <div class="close"></div> 
</div>
</div>