 
  
<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3>Редактировать страницы</h3>
    <!--<<ul class="content-box-tabs">
						li><a href="#tab1" class="default-tab">Table</a></li>   
						<li><a href="#tab2">Forms</a></li>
					</ul>-->
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <div class="content-box-content">
    <div class="tab-content default-tab" id="tab1">
	  <form name="form" method="post" action="">
      
      <div  id="table">
        <table>
          <thead>
            <tr>
              <th><input class="check-all" type="checkbox" /></th>
              <th>ID</th>
              <th>Заголовок</th>
              <th>Url</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          
          {foreach from=$view key=key item=item}
          <tr>
            <td><input type="checkbox" name="type[]" value="{$item.id_item}"/></td>
            <td>{$item.id_item}</td>
            <td><a href="/admin/pages/edit/{$item.id_item}/" title="title">{$item.name}</a></td>
            <td>{$item.url}</td>
            <td><!-- Icons -->
              <a href="/admin/pages/edit/{$item.id_item}/" title="Редактироание"><img src="/admin/templates/{$theme}/images/icons/pencil.png" alt="Редактироание" /></a>
			  <a onClick="conf('/admin/pages/delete/{$item.id_item}/')" href="#" title="Удалить"><img src="/admin/templates/{$theme}/images/icons/cross.png" alt="Удалить" /></a>
			  <a href="#" title="Edit Meta"><img src="/admin/templates/{$theme}/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a></td>
          </tr>
          {/foreach}
        </table>
      </div>
    </div>
 </form>
  </div>
  <!-- End .content-box-content -->
</div>
<!-- End .content-box -->


<ul class="shortcut-buttons-set">
  <li><a class="shortcut-button new-page"href="/admin/pages/add/"><span class="png_bg"> Создать страницу </span></a></li>
 
    
</ul>
<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->
