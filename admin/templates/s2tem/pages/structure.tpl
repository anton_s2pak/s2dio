
 {*-----------------------------------------------Скрипт и стиль деревовидного меню------------------------------------------*}
 <script src="/admin/templates/{$theme}/js/sitemap/sitemapstyler.js" type="text/javascript"></script>
 <link rel="stylesheet" href="/admin/templates/{$theme}/js/sitemap/sitemapstyler.css" type="text/css" media="screen" />
 

 
 

<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3>{$head}</h3>
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <div class="content-box-content">
    <div class="tab-content default-tab" id="tab1">
      <!-- This is the target div. id must match the href of this div's tab -->
	   <div id="list">
 <div class="notification success png_bg" id="response"> </div>
      


{*-----------------------------------------------Вывод рекурсивного меню----------------------------------------------------*}     
 {function name=cats_tree parent_id=0}
  {if $data[$parent_id].childs|@sizeof > 0}
    <ul id="sitemap">
    {foreach $data[$parent_id].childs as $child_id}
      <li  id="pos_{$data[$child_id].pos}" class="{$data[$child_id].id_item}">
	  
	 
		{*--------------Пункт меню-------------------*}
		{if $data[$child_id].enabled == 0} 
		<a href = "/admin/structure/{$data[$child_id].menu}/{$data[$child_id].id_item}/" class="lamp"><img src="/admin/templates/{$theme}/images/lamp_off.gif" alt="Не активно" title="Не активно"></a>
		{else}
		<a href = "/admin/structure/{$data[$child_id].menu}/{$data[$child_id].id_item}/" class="lamp"><img src="/admin/templates/{$theme}/images/lamp_on.gif" alt="Активно" title="Активно"></a>
		{/if}
	    <a href="/admin/structure/edit/{$data[$child_id].menu}/{$data[$child_id].id_item}/">{$data[$child_id].name}</a>
        {*--------------Редактировать-------------------*}
		<div class="right">
		<img src="/admin/templates/{$theme}/images/icons/1312200629_move.png" alt="Переместить" border="0"> 
		<a href="/admin/structure/edit/{$data[$child_id].menu}/{$data[$child_id].id_item}/">&nbsp;&nbsp;<img src="/admin/templates/{$theme}/images/icons/pencil.png" alt="Изменить" border="0"></a>
		<a onClick="conf('/admin/structure/del/{$data[$child_id].menu}/{$data[$child_id].id_item}/')" href="#">&nbsp;&nbsp;<img src="/admin/templates/{$theme}/images/icons/cross.png" alt="Удалить" border="0"></a>
        </div>
		{call name=cats_tree parent_id=$child_id}
		
		
		</li>
    {/foreach}
    </ul>
{/if}	
{/function}
{call name=cats_tree data=$cats_tree_data}
{*-----------------------------------------------Вывод рекурсивного меню----------------------------------------------------*}  		  


           
      </div>
    </div>
    <!-- End #tab1 -->
  </div>
 
  <!-- End .content-box-content -->
</div>
<!-- End .content-box -->
{*-----------------------------------------------Модальное окно Добавить пункт меню----------------------------------------------------*}  
 
 <ul class="shortcut-buttons-set">
  <li><a href="#url" class="shortcut-button new-punkt-menu modal-window"><span class="png_bg">Создать новый пункт меню</span></a></li>
</ul>




<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->
 
<div id="boxes" class="url">
<div id="dialog" class="window">
  <h3>Добавить пункт меню</h3>
  <form name="form" method="post" action="/admin/structure/add/{$menu}/">
    <fieldset>
      <p>
        <label>Заголовок</label>
        <input name="name" class="text-input large" type="text" id="url">
      </p>
      <p>
        <label>URL</label>
        <input name="url" class="text-input large" type="text" id="trans"> 
      </p> 
       
	  

   <p>
<label>Категория</label>
 <div id="myMenu"> <label>Выбрать вариант</label>
<div class="myMenu">
	
  {function name=tree parent_id=0}
										
   {if $data[$parent_id].childs|@sizeof > 0}
    <ul id="sitemapDropBox">
    {foreach from=$data[$parent_id].childs key=k item=child_id  }			
      <li  id="{$data[$child_id].id_item}">
	  
	 
		{*--------------Пункт меню-------------------*}
	 
	    <a href="javascript: void(0);">{$data[$child_id].name}</a>
        {*--------------Редактировать-------------------*}
		
	    {call name=tree parent_id=$child_id}
		
		
		</li>
    {/foreach}
    </ul>
{/if}	
{/function}
{call name=tree data=$cats_tree} 
{*-----------------------------------------------Вывод рекурсивного меню----------------------------------------------------*}  
<input type="hidden" id="id_str" name="id_str" />
</div>
</div>
</p> 


 
	 <p>
        <label>Создать страницу</label>
        <input name="pageon" class="checkbox"  value="1" type="checkbox">
        &nbspСоздать страницу с таким названием </p>
      
        <label>Активность</label>
        <input name="enabled" class="checkbox" checked="checked" value="1" type="checkbox">
        &nbspВкл/Выкл </p>
      <p>
	     
	  

        <input type="submit" class="button" value="Добавить пункт меню">
      </p>
    </fieldset>
  </form>
  <div class="close"></div>
</div>
</div>
