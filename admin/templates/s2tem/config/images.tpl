<!--div id="open">open</div><div id="close">close</div><div id="dock">dock</div><div id="undock">undock</div-->

<div class="content-box">
  <div class="content-box-header">
    <h3>Файловый менеджер</h3>
    <div class="clear"></div>
  </div>
</div>
<link rel="stylesheet" href="/admin/templates/{$theme}/js/redactor/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" title="no title" charset="utf-8">
<link rel="stylesheet" href="/admin/templates/{$theme}/css/elfinder.css" type="text/css" media="screen" title="no title" charset="utf-8">
<script src="/admin/templates/{$theme}/js/redactor/js/elfinder.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/admin/templates/{$theme}/js/redactor/js/i18n/elfinder.ru.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
		$().ready(function() {
			
			var f = $('#finder').elfinder({
				url : '/admin/templates/{$theme}/js/redactor/connectors/php/connector.php',
				lang : 'ru',
				docked : true
 
			})
			// window.console.log(f)
			//$('#close,#open,#dock,#undock').click(function() {
				//$('#finder').elfinder($(this).attr('id'));
			//})
		})
	</script>
<div id="finder">finder</div>
