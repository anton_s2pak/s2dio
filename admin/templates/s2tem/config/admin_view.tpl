 
<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3>Пользователи</h3>
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <div class="content-box-content">
    <div class="tab-content default-tab" id="tab1">
      <!-- This is the target div. id must match the href of this div's tab -->
      <div  id="table">
        <table>
          <thead>
            <tr>
           
              <th>ID</th>
              <th>Логин</th>
              <th>ФИО</th>
              <th>Email</th>
              <th>Группа</th>
              <th>Дата регистрации</th>
              <th>Дата посещения</th>
              <th> </th> 
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td colspan="6"> </td>
            </tr>
          </tfoot>
          <tbody>
          
          {foreach from=$view_admin key=key item=item}
          <tr>
            
            <td>{$item.id_item}</td>
            <td><a href="/admin/login/edit/{$item.id_item}/" title="title">{$item.login}</a></td>
            <td>{$item.name}</td>
            <td>{$item.email}</td>
            <td>{if $item.chmod eq 1}Администраторы{else}Редакторы{/if} </td>
            <td>{$item.reg_date}</td>
            <td>{$item.last_enter}</td>
            <td><!-- Icons -->
              <a href="/admin/login/edit/{$item.id_item}/" title="Редактировать"><img src="/admin/templates/{$theme}/images/icons/pencil.png" alt="Редактировать" /></a> 
              <a onClick="conf('/admin/login/delete/{$item.id_item}/')"  href="#" title="Удалить"><img src="/admin/templates/{$theme}/images/icons/cross.png" alt="Удалить" /></a></td>
          </tr>
          {/foreach}
          </tbody>
          
        </table>
      </div>
    </div>
    <!-- End #tab1 -->
  </div>
  <!-- End .content-box-content -->
</div>
<!-- End .content-box -->
 
<ul class="shortcut-buttons-set">
  <li><a href="#menuadd" class="shortcut-button user-m  modal-window"> <span class="png_bg"> Добавить пользователя</span></a></li>
</ul>
<!-- End .shortcut-buttons-set -->
<div class="clear"></div> 

<!-- Модальное окно-->
<div id="boxes" class="menuadd">
  <div id="dialog" class="window">
    <h3>Добавить пользователя</h3>
   
	<form name="form" method="post" action="/admin/login/add/">
      <fieldset>
      
      <p>
        <label>Логин</label>
        <input name="login" type="text"   class="text-input large">
      </p>
      <p>
        <label>Пароль</label>
        <input name="password"  type="text"   class="text-input large">
      </p>
      <p>
       <label>Email</label>
        <input name="email" type="text" class="text-input large">
      </p>
      
        <p>
       <label>ФИО</label>
        <input name="name" type="text"  class="text-input large">
      </p>
        <p>
         <label>Группа</label>
      <select name="chmod" class="select">
           		<option value="1">Администраторы</option>
				<option value="2" selected="selected">Редакторы</option>
   			</select>
             </p> 
            
      <p>
        <input type="button" class="button"  onclick="AdminAdd(this.form);"  value="Добавить">
      </p>
    </form>
    <div class="close"></div>
  </div>
</div>


 