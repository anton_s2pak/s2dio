<form name="form" method="post" action="">
  <div class="content-box">
    <div class="content-box-header">
      <h3>Настройка системы</h3>
      <ul class="content-box-tabs">
        <li><a href="#tab1" class="default-tab">Основные настройки</a></li>
        
        <li><a href="#tab2">SEO сайта</a></li>
        <li><a href="#tab3">Дополнительно</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <div class="content-box-content">
      <div class="tab-content default-tab" id="tab1">
        <fieldset>
          <p>
            <label>E-mail администратора</label>
            <input class="text-input medium-input" name="email" value="{$email}" type="text" />
            <span class="info"></span> </p>
          <p>
            <label>Количество вывода на страницу</label>
            <input class="text-input small" name="page" value="{$page_num}" type="text"  />
            <span class="info"></span> </p>
          <p>
            <label>Работа сайта</label>
            <input name="work_site" type="radio" value="1" {if $work_site eq 1}checked{/if}>
            Включить
            <input name="work_site" type="radio" value="0" {if $work_site eq 0}checked{/if}>
            Выключить</p>
          <p>
            <label>Текст при отключении сайта</label>
            <textarea   name="close_text" class="textarea" cols="70" rows="5">{$close_text}</textarea>
          </p>
        </fieldset>
      </div>
      <!-- End #tab1 -->
      <div class="tab-content" id="tab2">
        <fieldset>
          <p>
            <label>Титлы(title)</label>
            <input name="title" value="{$title}" type="text" class="text-input large-input">
          </p>
          <p>
            <label>Ключевые слова(keywords)</label>
            <textarea  name="keywords" class="textarea" cols="70" rows="5">{$keywords}</textarea>
          </p>
          <p>
            <label>Описание(description)</label>
            <textarea   name="description" class="textarea" cols="70" rows="5">{$description}</textarea>
          </p>
        </fieldset>
      </div>
      <div class="tab-content" id="tab3">
        <fieldset>
          <p>
            <label>Коды счетчиков</label>
            <textarea   name="code_counter" class="textarea" cols="70" rows="5">{$code_counter}</textarea>
          </p>
        </fieldset>
      </div>
      <div class="clear"></div>
    </div>
    <!-- End .content-box-content -->
  </div>
  <!-- End .content-box -->
  <input class="button" value="Сохранить" onclick="Config(this.form);" type="button">
  <input type="button"  class="button"   value="Отменить">
</form>
