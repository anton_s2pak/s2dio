
<form name="form" method="post" action="">
  <div class="content-box">
    <div class="content-box-header">
      <h3>Редактируем пользователя - {$login}</h3>
      <div class="clear"></div>
    </div>
    <div class="content-box-content">
      <p>
        <label>Логин</label>
        <input class="text-input small-input" type="text"  name="login" value="{$login}"/>
        <span class="info"></span> </p>
      <p>
        <label>Пароль</label>
        <input class="text-input small-input" type="text"  name="password"  id="url" name="name" />
        <span class="info"></span> </p>
      <p>
        <label>Email</label>
        <input class="text-input small-input" type="text"  name="email" value="{$email}"/>
        <span class="info"></span> </p>
      <p>
        <label>ФИО</label>
        <input class="text-input small-input" type="text" name="name" value="{$name}" id="url"  />
        <span class="info"></span> </p>
      {if $chmod_ses eq 1}
      <p>
        <label>Группа</label>
        <select name="chmod" class="select">
          <option value="1" {if $chmod eq 1}selected="selected"{/if}>Администраторы</option>
          <option value="2" {if $chmod eq 2}selected="selected"{/if}>Редакторы</option>
        </select>
      </p>
      {/if} </div>
    <!-- End .content-box-content -->
  </div>
  <!-- End .content-box -->
  <input type="button"  class="button" onClick="AdminAdd(this.form);" value="Сохранить">
  <input type="button"  class="button"   value="Отменить">
</form> 