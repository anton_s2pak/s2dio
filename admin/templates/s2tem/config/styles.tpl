<script src="/admin/templates/{$theme}/js/codemirror/js/codemirror.js" type="text/javascript" charset="utf-8"></script>


 <div class="content-box">
    <div class="content-box-header">
      <h3>Редактируем стиль "{$CurrentStyle.filename}"</h3>
       
      <div class="clear"></div>
    </div> </div>
   
  
      <div id="cont_center">
<div id="cont_right">
{if $Error}
<div id="error_minh">
  <div id="error"> <img src="./images/error.jpg" alt=""/>
    <p>{$Error}</p>
  </div>
</div>
{/if}
 
<form name='template' method="post">
<input type=hidden name=filename value='{$CurrentStyle.filename}'>
<div style="background-color:#f0f0f0; border: 1px solid #e0e0e0; padding: 3px; text-align:right;"> <span class=tovar_on style='float:left'>{$themes}</span>
  <input type=submit value='Сохранить' class="button">
  <div style='background-color:#ffffff; border: 1px solid #e0e0e0; padding: 0px;width:700px; height:1000px;'>
    <textarea id="code" name=content  style='word-wrap:normal;width:700px; height:1000px;'>{$Content|escape}</textarea>
  </div>
</div>
</div>
<div id="cont_left">
<h3>Стили</h3>
  <ul>
    {foreach from=$Styles item=style}
    <li class="filename"> <a href="{$style.edit_url}">{$style.name}</a> <br>
      <span class=filename_small>{$style.name}</span> </li>
    {/foreach}
  </ul>
</div>
</div>
    
      
 







<script type="text/javascript">
 
  var editor = CodeMirror.fromTextArea('code', {
    height: "350px",
    parserfile: ["parsecss.js"],
    stylesheet: ["/admin/templates/{$theme}/js/codemirror/css/csscolors.css"],
    path: "/admin/templates/{$theme}/js/codemirror/js/",
    dumbTabs: true,
    saveFunction: function() { 
      window.document.template.submit();
    },
    textWrapping: true
  });
</script>
