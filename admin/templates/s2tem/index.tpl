	<!-- Page Head -->
			<h2>Добро пожаловать {$admin_ses}</h2>
			<p id="page-intro">С чего вы хотите начать?</p>


    <div class="content-box column-right"><!-- Start Content Box -->

        <div class="content-box-header">

            <h3>Информация</h3>

        </div> <!-- End .content-box-header -->

        <div class="content-box-content">

            <div class="tab-content default-tab">


                <table width="100%" bgcolor="#e3e6e8" border="0" cellspacing="1" cellpadding="1" id="table">
                    <tr class="table_rows">

                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Имя хоста:</td>
                        <td width="50%">{$server_name}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">IP-адрес сервера:</td>
                        <td width="50%">{$server_ip}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Операционная система:</td>
                        <td width="50%">{$OS}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Установленое ПО:</td>
                        <td width="50%">{$server_software}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Версия PHP:</td>
                        <td width="50%">{$php_version}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Версия MySQL:</td>
                        <td width="50%">{$mysql_version}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Максимальный размер загрузки:</td>
                        <td width="50%">{$upload_max_filesize}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Максимальный размер загрузки через POST:</td>
                        <td width="50%">{$post_max_size}</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Размер свободного места на диске:</td>
                        <td width="50%">
                        {$free_space}
                        </td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Максимальное время исполнения:</td>
                        <td width="50%">{$max_execution_time} сек</td>
                    </tr>
                    <tr align="center" bgcolor="#efefef">
                        <td width="50%" style="padding: 5px;">Ваш IP-адрес:</td>
                        <td width="50%">{$remote_ip}</td>
                    </tr>
                </table>

            </div> <!-- End #tab3 -->

        </div> <!-- End .content-box-content -->

    </div> <!-- End .content-box -->



			<ul class="shortcut-buttons-set">

				<li><a class="shortcut-button new-page" href="/admin/pages/add/"><span class="png_bg">
					Создать страницу
				</span></a></li>
				
				<li><a class="shortcut-button upload-image" href="/admin/photos/add/"><span class="png_bg">
					Загрузить рисунок
				</span></a></li>
				
				<li><a class="shortcut-button add-event" href="/admin/structure/1/"><span class="png_bg">
					Подключить страницу
				</span></a></li>

			</ul><!-- End .shortcut-buttons-set -->
			
			<div class="clear"></div> <!-- End .clear -->


