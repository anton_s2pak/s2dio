<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--
    /*
     * @copyright	Copyright (C) 2009 - 2011. All rights reserved.
     * @
     * @CMS S2dio
     * @copyright Copyright (C) 2010 - 2011. All rights reserved.
     * @author Original by Stupak Oleg  <http://www.s2dio.com.ua/>
     * @
    */
    -->

    <title>S2 CMS - Панель управления</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/admin/templates/{$theme}/css/reset.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/templates/{$theme}/css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/templates/{$theme}/css/invalid.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="/admin/templates/{$theme}/js/jquery.js"></script>
    <script src="/admin/templates/{$theme}/js/jquery.synctranslit.js"
            type="text/javascript"></script> {*Скрипт транслит*}
    <script src="/admin/templates/{$theme}/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript"
            src="/admin/templates/{$theme}/js/tiny_mce/plugins/smimage/smplugins.js"></script>
    <script src="/admin/templates/{$theme}/js/editor.js" type="text/javascript"></script>
    <script src="/admin/templates/{$theme}/js/form.js" type="text/javascript"></script>
    <script src="/admin/templates/{$theme}/js/script.js" type="text/javascript"></script>
    <script src="/admin/templates/{$theme}/js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="/admin/templates/{$theme}/js/simpla.jquery.configuration.js"></script>

    <!-- Internet Explorer -->
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="/admin/templates/s2tem/css/ie.css" type="text/css" media="screen"/>
    <![endif]-->
    <!--[if IE 6]>
    <script type="text/javascript" src="/admin/templates/s2tem/js/DD_belatedPNG_0.0.7a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix('.png_bg, img, li');
    </script>
    <![endif]-->

</head>
{$themewwww}
<body>

<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

<div id="sidebar">
    <div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

        <h1 id="sidebar-title"><a href="/">S2dio CMS - Панель управления</a></h1>

        <!-- Logo (221px wide) -->
        <a href="/admin"><img id="logo" src="/admin/templates/{$theme}/images/logocms.png" alt="S2dio.com.ua"/></a>

        <!-- Sidebar Profile links -->
        <div id="profile-links">



            Привет <a href="/admin/login/edit/{$id_ses}/">{$admin_ses}</a>,{if $chmod_ses eq 1} <br> вы являетесь
            администратором<br/>{/if}
            <div class="jclock"></div>
            <br/>
            <a href="/admin/">Админ-панель</a> | <a href="/" target="_blank" title="View the Site">Перейти на сайт</a> | <a href="/admin/logout/"
                                                                                       title="Sign Out">Выход</a>
        </div>

        <ul id="main-nav">  <!-- Accordion Menu -->
            <li>

                <a href="#" class="nav-top-item {if $mod == "pages"}current{/if}">
                    <!-- Add the class "current" to current menu item -->
                    Страницы и меню
                </a>
                <ul>
                    <li><a href="/admin/structure/1/">Меню</a></li>
                    <li><a href="/admin/pages/">Страницы</a></li>
                    <!-- Add class "current" to sub menu items also -->


                </ul>

            </li>
{if $chmod_ses eq 1}
            <li>
                <a href="#" class="nav-top-item {if $mod == "news"}current{/if}">
                    Новости
                </a>
                <ul>
                    <li><a href="/admin/news/">Редактировать новости</a></li>
                    <li><a href="/admin/news/add/">Добавить новость</a></li>
                </ul>
            </li>

            <li>
                <a href="#" class="nav-top-item {if $mod == "articles"}current{/if}">
                    Статьи
                </a>
                <ul>
                    <li><a href="/admin/articles/">Редактировать статьи</a></li>
                    <li><a href="/admin/articles/add/">Добавить статьи</a></li>

                </ul>
            </li>

            <li>
                <a href="#" class="nav-top-item {if $mod == "catalog"}current{/if}">
                    Каталог
                </a>
                <ul>
                    <li><a href="/admin/catalog/">Редактировать каталог</a></li>
                    <li><a href="/admin/catalog/add/">Добавить в каталог</a></li>
                </ul>
            </li>
{/if}
            <li>
                <a href="#" class="nav-top-item  {if $mod == "photos"}current{/if}">
                    Фотогалерея
                </a>
                <ul>
                    <li><a href="/admin/photos/">Редактировать галерею</a></li>
                    <li><a href="/admin/photos/add/">Добавить в галерею</a></li>
                </ul>
            </li>
{if $chmod_ses eq 1}
            <li>
                <a href="#" class="nav-top-item {if $mod == "faq"}current{/if}">
                    Вопрос-Ответ
                </a>
                <ul>
                    <li><a href="/admin/faq/" class="current">Просмотр</a></li>

                </ul>
            </li>


            <li>
                <a href="#" class="nav-top-item {if $mod == "comments"}current{/if}">
                    Комментарии
                </a>
                <ul>
                    <li><a href="/admin/comments/" class="current">Просмотр</a></li>

                </ul>
            </li>


            <li>
                <a href="#" class="nav-top-item {if $mod == "banners"}current{/if}">
                    Баннера
                </a>
                <ul>
                    <li><a href="/admin/banners/">Редактировать баннер</a></li>
                    <li><a href="/admin/banners/add/">Добавить баннер</a></li>
                </ul>
            </li>
{/if}
            <li>
                <a href="#" class="nav-top-item {if $mod == "config"}current{/if}">
                    Управление системой
                </a>
                <ul>
                    {if $chmod_ses eq 1}<li><a href="/admin/login/view/">Пользователи</a></li>{/if}
                    <li><a href="/admin/config/"> Настройка системы</a></li>
                    {if $chmod_ses eq 1}<li><a href="/admin/mypicture/">Файловый менеджер</a></li>{/if}
                    <li><a href="/admin/styles/">Редактировать стили</a></li>
                </ul>
            </li>

            <!--li>
                       <a href="#upload" class="nav-top-item no-submenu modal-window">
                           Загрузить фотографию
                       </a>
                   </li-->

        </ul>
        <!-- End #main-nav -->


        <!-- Модальное окно фото -->
        <div id="boxes" class="upload">
            <div id="dialog" class="window">
                <h3>Загрузить фотографию</h3>

                <form action="/admin/uploads/" method="post" enctype="multipart/form-data" target="_blank"
                      style="padding-top: 5px">
                    <center>
                        <input name="file" type="file"><br><br>
                        <input name="Submit" type="button" class="button" onclick="UploadsFiles(this.form);"
                               value="Загрузить">
                    </center>
                </form>
                <div class="close"></div>
            </div>
        </div>
        <div id="maska"></div>


    </div>
</div>
<!-- End #sidebar -->

<div id="main-content"> <!-- Main Content Section with everything -->

<noscript> <!-- Show a notification if the user has disabled javascript -->
    <div class="notification error png_bg">
        <div>
            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/"
                                                                                  title="Upgrade to a better browser">upgrade</a>
            your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852"
                               title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface
            properly.
        </div>
    </div>
</noscript>




{*Сообщение об ошибках*}
<div id="allinfo"></div>
{include file=$page}













{*
            <div class="content-box"><!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>Content box</h3>

                    <ul class="content-box-tabs">
                        <li><a href="#tab1" class="default-tab">Table</a></li> <!-- href must be unique and match the id of target div -->
                        <li><a href="#tab2">Forms</a></li>
                    </ul>

                    <div class="clear"></div>

                </div> <!-- End .content-box-header -->

                <div class="content-box-content">

                    <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                        <div class="notification attention png_bg">
                            <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                This is a Content Box. You can put whatever you want in it. By the way, you can close this notification with the top-right cross.
                            </div>
                        </div>
                             <div  id="table">
                        <table>

                            <thead>
                                <tr>
                                   <th><input class="check-all" type="checkbox" /></th>
                                   <th>Column 1</th>
                                   <th>Column 2</th>
                                   <th>Column 3</th>
                                   <th>Column 4</th>
                                   <th>Column 5</th>
                                </tr>

                            </thead>

                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="bulk-actions align-left">
                                            <select name="dropdown">
                                                <option value="option1">Choose an action...</option>
                                                <option value="option2">Edit</option>
                                                <option value="option3">Delete</option>
                                            </select>
                                            <a class="button" href="#">Apply to selected</a>
                                        </div>

                                        <div class="pagination">
                                            <a href="#" title="First Page">&laquo; First</a><a href="#" title="Previous Page">&laquo; Previous</a>
                                            <a href="#" class="number" title="1">1</a>
                                            <a href="#" class="number" title="2">2</a>
                                            <a href="#" class="number current" title="3">3</a>
                                            <a href="#" class="number" title="4">4</a>
                                            <a href="#" title="Next Page">Next &raquo;</a><a href="#" title="Last Page">Last &raquo;</a>
                                        </div> <!-- End .pagination -->
                                        <div class="clear"></div>
                                    </td>
                                </tr>
                            </tfoot>

                            <tbody>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Lorem ipsum dolor</td>
                                    <td><a href="#" title="title">Sit amet</a></td>
                                    <td>Consectetur adipiscing</td>
                                    <td>Donec tortor diam</td>
                                    <td>
                                        <!-- Icons -->
                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a>
                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                        </div>
                    </div> <!-- End #tab1 -->

                    <div class="tab-content" id="tab2">

                        <form action="" method="post">

                            <fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->

                                <p>
                                    <label>Small form input</label>
                                        <input class="text-input small-input" type="text" id="small-input" name="small-input" /> <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                                        <br /><small>A small description of the field</small>
                                </p>

                                <p>
                                    <label>Medium form input</label>
                                    <input class="text-input medium-input" type="text" id="medium-input" name="medium-input" /> <span class="input-notification error png_bg">Error message</span>
                                </p>

                                <p>
                                    <label>Large form input</label>
                                    <input class="text-input large-input" type="text" id="large-input" name="large-input" />
                                </p>

                                <p>
                                    <label>Checkboxes</label>
                                    <input type="checkbox" name="checkbox1" /> This is a checkbox <input type="checkbox" name="checkbox2" /> And this is another checkbox
                                </p>

                                <p>
                                    <label>Radio buttons</label>
                                    <input type="radio" name="radio1" /> This is a radio button<br />
                                    <input type="radio" name="radio2" /> This is another radio button
                                </p>

                                <p>
                                    <label>This is a drop down list</label>
                                    <select name="dropdown" class="small-input">
                                        <option value="option1">Option 1</option>
                                        <option value="option2">Option 2</option>
                                        <option value="option3">Option 3</option>
                                        <option value="option4">Option 4</option>
                                    </select>
                                </p>

                                <p>
                                    <label>Textarea with WYSIWYG</label>
                                    <textarea class="text-input textarea wysiwyg" id="textarea" name="textfield" cols="79" rows="15"></textarea>
                                </p>

                                <p>
                                    <input class="button" type="submit" value="Submit" />
                                </p>

                            </fieldset>

                            <div class="clear"></div><!-- End .clear -->

                        </form>

                    </div> <!-- End #tab2 -->

                </div> <!-- End .content-box-content -->

            </div> <!-- End .content-box --> -->


            *}








<div id="footer">
    <small>
        &#169; Copyright 2011 S2 CMS | Powered by <a href="http://www.s2dio.com.ua">www.s2dio.com.ua</a> | <a href="#">Top</a>
    </small>
</div>
<!-- End #footer -->

</div>
<!-- End #main-content -->

</div>
</body>

</html>
