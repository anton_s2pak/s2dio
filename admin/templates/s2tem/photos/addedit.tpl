<form name="form" method="post" action="" id="form" enctype="multipart/form-data">
    <div class="content-box">
        <div class="content-box-header">
            <h3>{$head_title|truncate:40}</h3>
            <ul class="content-box-tabs">
                <li><a href="#tab1" class="default-tab">Добавить картинку</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="content-box-content">
            <div class="tab-content default-tab" id="tab1">
                <fieldset>
                    <p>
                        <label>Категория:</label>
                        <select size="8" name="id_category" class="select" multiple>
                        {foreach from=$view_cat key=key item=item}
                            <option value="{$item.id_item}"
                                    {if $item.id_item == $cat || $item.id_item == $id_category_get} selected="selected"{/if}>{$item.name}</option>
                        {/foreach}
                        </select>
                    <p>
                        <label>Название</label>
                        <input class="text-input medium-input" type="text" value="{$name}" id="url" name="name" autofocus />
                        <span class="info"></span></p>
                    <p>
                    <label>Картинка</label>
                {if $image}   <p><img src="/uploads/photos/small/{$image}" class="border_img_edit"></p> {/if}
                    <input name="image" type="file" class="" size="50">
                    </p>
                </fieldset>
            </div>
        </div>
        <!-- End .content-box-content -->
    </div>
    <!-- End .content-box -->
    <input type="button" class="button" onClick="Valid()" value="Сохранить">
    <input type="button" class="button" value="Отменить">
</form>