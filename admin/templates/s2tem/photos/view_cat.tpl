<div class="content-box">
    <!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Фотогалерея</h3>

        <div class="clear"></div>
    </div>
    <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <!-- This is the target div. id must match the href of this div's tab -->
            <div id="table">
                <table>
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th style="text-align: center">Кол-во</th>
                            <th style="text-align: center">Картинка</th>
                            <th style="text-align: center">Порядок</th>
                            <th style="text-align: center">Дата</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$view_cat key=key item=item}
                        <tr>
                            <td><a href="/admin/photos/{$item.id_item}/" title="{$item.name}">{$item.name}</a></td>
                            <td style="text-align: center">{$item.num}</td>
                            <td style="text-align: center">{if $item.image}<a href="/admin/photos/{$item.id_item}/"><img
                                    src="/uploads/photos/small/{$item.image}" width="54" height="54"></a>{/if}</td>
                            <td style="text-align: center">{if $item.id_item != 1 & $item.id_item != 2 & $item.id_item != 3}{$item.order}{/if}</td>
                            <td style="text-align: center">{$item.date}</td>
                            <td style="text-align: center"><!-- Icons -->
                                <a href="/admin/photos/cat/edit/{$item.id_item}/" title="Редактировать"><img
                                        src="/admin/templates/{$theme}/images/icons/pencil.png"
                                        alt="Редактировать"/></a> {if $item.id_item != 1 & $item.id_item != 2 & $item.id_item != 3}
                                    <a onClick="conf('/admin/photos/cat/delete/{$item.id_item}/')" href="#"
                                       title="Удалить"><img src="/admin/templates/{$theme}/images/icons/cross.png"
                                                            alt="Удалить"/></a>{/if}</td>
                        </tr>
                        {/foreach}
                        </tbody>
                </table>
            </div>
        </div>
        <!-- End #tab1 -->
    </div>
    <!-- End .content-box-content -->
</div>

{*-----------------------------------------------Модальное окно Добавить меню----------------------------------------------------*}
<ul class="shortcut-buttons-set">
    <li><a href="#menuadd" class="shortcut-button news-menu modal-window"> <span
            class="png_bg"> Создать новую галерею </span></a></li>
    <li><a href="/admin/photos/add/" class="shortcut-button new-article modal-window"> <span class="png_bg"> Добавить фото в галерею</span></a>
    </li>
</ul>
<!-- End .shortcut-buttons-set -->
<div class="clear"></div>
<!-- End .clear -->
<!-- Модальное окно-->
<div id="boxes" class="menuadd">
    <div id="dialog" class="window">
        <h3>Добавить раздел</h3>
        <form name="form" id="form" method="post" action="/admin/photos/cat/add/" enctype="multipart/form-data">
            <fieldset>
                <p>
                    <label>Название</label>
                    <input name="name" type="text" class="text-input large" id="url">
                    <span class="info"></span>
                </p>
                <p>
                    <label>Url</label>
                    <input name="url" class="text-input large" type="text" id="trans">
                </p>{if $item.id_item != 1 & $item.id_item != 2 & $item.id_item != 3}
                <p>
                    <label>Порядок</label>
                    <input type="text" class="text-input order" style="width: 50px; text-align: center;" value="" name="order" maxlength="3" />
                </p>
                {else}
                    <input type="hidden" value="999" />
                {/if}
                <p>
                    <label>Изображение</label>
                    <input type="file" size="50" name="image"></p>
                <p>
                    <label>Описание</label>
                    <textarea id="edit_two" name="text" cols="70" rows="25"></textarea>
                </p>

                <input type="submit" onclick="Valid(true); return false;" class="button" value="Добавить галерею">
                </p>
            </fieldset>
        </form>
        <div class="close"></div>
    </div>
</div>