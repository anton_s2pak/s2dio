<form id="form" method="post" action="" enctype="multipart/form-data">
  <div class="content-box">
    <div class="content-box-header">
      <h3>Изменить название раздела</h3>
      <div class="clear"></div>
    </div>
    <div class="content-box-content">
      <div class="tab-content default-tab" id="tab1">
        <fieldset>
          <p>
            <label>Заголовок</label>
            <input class="text-input medium-input" type="text"  value="{$name}" id="url" name="name" />
            <span class="info"></span> </p>
          <p>
            <label>Url</label>
             <input name="url" class="text-input large" type="text" id="trans" value="{$url}"> 
          </p>
          <p>
            <label>Порядок</label>
            {if $id_item != 1 & $id_item != 2 & $id_item != 3}<input type="text" class="text-input order" style="width: 50px; text-align: center;" value="{$order}" id="order" name="order" maxlength="3" />{/if}
          </p>
          <p>
            <label>Изображение раздела</label>
            {if $image}<img src="/uploads/photos/small/{$image}" class="border_img_edit">{/if}
            <input name="image" type="file" class="" size="50">
          </p>
          <p>
            <label>Описание раздела</label>
            <textarea id="edit_two"  name="text" cols="70" rows="25">{$text}</textarea>
          </p>
        </fieldset>
      </div>
      <!-- End #tab1 -->
    </div>
    <!-- End .content-box-content -->
  </div>
  <!-- End .content-box -->
  <input type="button" onclick="Valid(true);" class="button" value="Сохранить">
  <input type="button"  class="button" onclick="window.location.href='/admin/photos/';return false;"  value="Отменить">
</form>
