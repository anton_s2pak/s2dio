<div class="content-box">
    <!-- Start Content Box -->
    <div class="content-box-header"><h3>Фотогалерея - Просмотр</h3>
        <div class="clear"></div>
    </div>
{if $num <= 0}
</div>
<div align="center" style="color: red; font-size: 16px">На даный момент этот раздел пуст</div>
<ul class="shortcut-buttons-set">
    <li><a href="/admin/photos/add/{$cat}/" class="shortcut-button new-article modal-window"><span class="png_bg">Добавить фото в галерею</span></a></li>
</ul>
{else}
<div class="content-box-content">
    <div id="table">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <colgroup>
                <col>
                <col width="45%">
                <col width="1">
                <col width="1">
            </colgroup>
        <thead>
            <tr>
                <th>Название</th>
                <th style="text-align: center">Картинка</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        {foreach from=$view key=key item=item}
            <tr align="center" bgcolor="#efefef">
                <td style="text-align: left;">{$item.name}</td>
                <td style="padding: 5px"><img
                        border="0" src="/uploads/photos/small/{$item.image}" alt="{$item.name}" width="54"></td>
                <td><a href="/admin/photos/edit/{$item.id_category}/{$item.id_item}/"><img
                        src="/admin/templates/s2tem/images/icons/pencil.png" alt="Изменить" border="0"></a></td>
                <td><a onClick="conf('/admin/photos/delete/{$item.id_item}/')" href="#"><img
                        src="/admin/templates/s2tem/images/icons/cross.png" alt="Удалить" border="0"></a></td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div></div></div>
<ul class="shortcut-buttons-set">
    <li><a href="/admin/photos/add/{$item.id_category}/" class="shortcut-button new-article modal-window"> <span class="png_bg"> Добавить фото в галерею</span></a></li>
</ul>
{/if}
<div class="clear"></div>
