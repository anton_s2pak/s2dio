function conf(location) {
    if (window.confirm("Внимание!\nВы действительно хотите удалить запись?") == false) {
    } else {
        window.location = location;
    }
}
;
function menu(subname) {
    lati = document.getElementById(subname);
    $('.submenu:not(lati)').slideUp('slow'),
        $(lati).animate({height:'toggle', opacity:'toggle'}, 'slow');
}
;
$(document).ready(function () {
    $('.page_add .form_add a').click(function () {
        Tabs(this);
        return false;
    })
    $('.team a').click(function () {
        return false;
    });
});
function Tabs(a) {
    a = $(a);
    if (!a.hasClass('active')) {
        $('.page_add .form_add a.active').removeClass('active');
        a.addClass('active');
        class_ = a.attr('rel');
        s = $('.page_add .form_add span');
        s.filter(':visible').addClass('hidden');
        s.filter('.' + class_).removeClass('hidden');
    }
}
;

/*Сортировка меню*/
$(document).ready(function () {

    $("#table tbody").find("tr").hover(function(){
        $(this).addClass('tr-hovered');
    },function(){
        $(this).removeClass('tr-hovered');
    });

    function slideout() {
        setTimeout(function () {
            $("#response").slideUp("slow", function () {
            });
        }, 5000);
    }

    $("#response").hide();
    $(function () {
        $("#list ul").sortable({ opacity:0.8, cursor:'move', update:function () {
            var parent = $(this).parent().attr('id').replace(/\D+/gi,''),
                parent = parent ? '&parent='+parent : '',
                order = $(this).sortable("serialize",{key:'pos'}) + parent +'&update=structure_pos';

            order = '';
            $.each($('li', this), function(key, value){
                    var key = key + 1;
                    if (!order){
                        order += "pos[]=" + key + "&id[]=" + $(value).attr('class');
                    }else{
                        order += "&pos[]=" + key + "&id[]=" + $(value).attr('class')
                    };
            });
            order +=  parent + '&update=structure_pos';
            console.log(order);
            $.post("/admin/?mod=pages", order, function (theResponse) {
                $("#response").html(theResponse);
                $("#response").slideDown('slow');
                slideout();
            });
        }
        });
    });


    /*Модальное окно*/

    var wclass;
    $('.modal-window').click(function (e) {
        wclass = $(this).attr('href').replace('#', '');
        $("#boxes." + wclass, "." + wclass + " .window").stop(false, true).hide(0).show(0);
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        $('#maska').css({'width':maskWidth, 'height':maskHeight}).stop(false, true).fadeIn(300).fadeTo("slow", 0.8);
        var winH = $(window).height();
        var winW = $(window).width();
        $("." + wclass + " .window").css('top', winH / 2 - $("." + wclass + " .window").height() / 2).css('left', winW / 2 - $("." + wclass + " .window").width() / 2).fadeIn(200);
        e.preventDefault();
        e.stopPropagation();
        return false;
    });
    $("#boxes .close").click(function (e) {
        $("." + wclass + " .window").stop(false, true).hide();
        $("#maska").stop(false, true).hide();
        e.prevenDEfault();
        e.stopPropagation();
        return false;
    });
    $('#maska').click(function () {
        $(this).stop(false, true).hide();
        $("." + wclass + " .window").stop(false, true).hide();
        return false;
    });
    function modalWinReCalCoords() {
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        $('#maska').css({'width':maskWidth, 'height':maskHeight});
        var winH = $(window).height();
        var winW = $(window).width();
        $("." + wclass + " .window").css('top', winH / 2 - $("." + wclass + " .window").height() / 2).css('left', winW / 2 - $("." + wclass + " .window").width() / 2);
    }

    ;
    $(window).resize(function () {
        modalWinReCalCoords();
    });


    /*Подключаем транслитирацию*/
    $(window).load(function () {
        $('#url').syncTranslit({destination:'trans'});


        /*Выпадающий список с категориями*/

        DropDown();
        function DropDown() {
            var mouseButton;
            var inputHidden = $('.myMenu #id_str');
            var conteinerText = $('#myMenu label');


            function hide() {
                $('#myMenu label').removeClass('active');
                $('.myMenu').hide();
            }

            ;
            ;
            $('#myMenu label').click(function (e) {
                $(this).next().fadeToggle(0);
                $(this).toggleClass('active');
                modalWinReCalCoords();
                e.preventDefault();
                e.stopPropagation();
                return false;
            });

            $(document).click(function (e) {
                if (e.which == 1) {
                    $('#myMenu label').removeClass('active');
                    $('.myMenu').hide();
                }
            });

            $('.myMenu').click(function (e) {
                e.stopPropagation();
            });

            $('#sitemapDropBox a').click(function () {
                var id = $(this).parent('li').attr('id');
                var html = $(this).html();
                inputHidden.val(id);
                conteinerText.text(html);
                hide();
            });


        }

        ;


    });


});


/*
 * jQuery jclock - Clock plugin - v 0.2.1
 * http://plugins.jquery.com/project/jclock
 *
 * Copyright (c) 2007-2008 Doug Sparling <http://www.dougsparling.com>
 * Licensed under the MIT License:
 *   http://www.opensource.org/licenses/mit-license.php
 */
(function ($) {

    $.fn.jclock = function (options) {
        var version = '0.2.1';

        // options
        var opts = $.extend({}, $.fn.jclock.defaults, options);

        return this.each(function () {
            $this = $(this);
            $this.timerID = null;
            $this.running = false;

            var o = $.meta ? $.extend({}, opts, $this.data()) : opts;

            $this.timeNotation = o.timeNotation;
            $this.am_pm = o.am_pm;
            $this.utc = o.utc;
            $this.utc_offset = o.utc_offset;

            $this.css({
                fontFamily:o.fontFamily,
                fontSize:o.fontSize,
                backgroundColor:o.background,
                color:o.foreground
            });

            $.fn.jclock.startClock($this);

        });
    };

    $.fn.jclock.startClock = function (el) {
        $.fn.jclock.stopClock(el);
        $.fn.jclock.displayTime(el);
    }
    $.fn.jclock.stopClock = function (el) {
        if (el.running) {
            clearTimeout(el.timerID);
        }
        el.running = false;
    }
    $.fn.jclock.displayTime = function (el) {
        var time = $.fn.jclock.getTime(el);
        el.html(time);
        el.timerID = setTimeout(function () {
            $.fn.jclock.displayTime(el)
        }, 1000);
    }
    $.fn.jclock.getTime = function (el) {
        var now = new Date();
        var hours, minutes, seconds;

        if (el.utc == true) {
            if (el.utc_offset != 0) {
                now.setUTCHours(now.getUTCHours() + el.utc_offset);
            }
            hours = now.getUTCHours();
            minutes = now.getUTCMinutes();
            seconds = now.getUTCSeconds();
        } else {
            hours = now.getHours();
            minutes = now.getMinutes();
            seconds = now.getSeconds();
        }

        var am_pm_text = '';
        (hours >= 12) ? am_pm_text = " P.M." : am_pm_text = " A.M.";

        if (el.timeNotation == '12h') {
            hours = ((hours > 12) ? hours - 12 : hours);
        } else {
            hours = ((hours < 10) ? "0" : "") + hours;
        }

        minutes = ((minutes < 10) ? "0" : "") + minutes;
        seconds = ((seconds < 10) ? "0" : "") + seconds;

        var timeNow = hours + ":" + minutes + ":" + seconds;
        if ((el.timeNotation == '12h') && (el.am_pm == true)) {
            timeNow += am_pm_text;
        }

        return timeNow;
    };

    // plugin defaults
    $.fn.jclock.defaults = {
        timeNotation:'24h',
        am_pm:false,
        utc:false,
        fontFamily:'',
        fontSize:'',
        foreground:'',
        background:'',
        utc_offset:0
    };

})(jQuery);


$(function ($) {
    $('.jclock').jclock();
});

	 
