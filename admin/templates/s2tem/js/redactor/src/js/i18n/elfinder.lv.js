/**
 * Latvian translation
 * @author Uldis PlotiЕ†ЕЎ <uldis.plotins@gmail.com>
 * @version 2010-09-22
 */
(function($) {
if (elFinder && elFinder.prototype.options && elFinder.prototype.options.i18n) 
	elFinder.prototype.options.i18n.lv = {
		/* errors */
		'Root directory does not exists'       : 'Saknes direktorija neeksistД“',
		'Unable to connect to backend'         : 'NeizdevДЃs savienoties ar serveri',
		'Access denied'                        : 'Pieeja liegta',
		'Invalid backend configuration'        : 'Nekorekta servera atbilde',
		'Unknown command'                      : 'NezinДЃma komanda',
		'Command not allowed'                  : 'Komandas izpilde liegta',
		'Invalid parameters'                   : 'Nekorekti parametri',
		'File not found'                       : 'Fails netika atrasts',
		'Invalid name'                         : 'Nekorekts vДЃrds',
		'File or folder with the same name already exists' : 'Fails vai direktorija ar ЕЎДЃdu nosaukumu jau eksistД“',
		'Unable to rename file'                : 'NeizdevДЃs pДЃrsaukt failu',
		'Unable to create folder'              : 'NeizdevДЃs izveidot direktoriju',
		'Unable to create file'                : 'NeizdevДЃs izveidot failu',  
		'No file to upload'                    : 'Nav failu augЕЎupielДЃdД“ЕЎanai',
		'Select at least one file to upload'   : 'IzvД“lieties vismaz vienu failu augЕЎupielДЃdД“ЕЎanai',
		'File exceeds the maximum allowed filesize' : 'Faila izmД“rs ir lielДЃks ar maksimДЃli atДјaujamo',
		'Data exceeds the maximum allowed size' : 'Datu apjoms pДЃrsniedza maksimДЃli atДјaujamo',
		'Not allowed file type'                 : 'NeatДјauts faila tips',
		'Unable to upload file'                 : 'NeizdevДЃs augЕЎupielДЃdД“t failu',
		'Unable to upload files'                : 'NeizdevДЃs augЕЎupielДЃdД“t failus',
		'Unable to remove file'                 : 'NeizdevДЃs dzД“st failu',
		'Unable to save uploaded file'          : 'AugЕЎupielДЃdД“to failu saglabДЃt neizdevДЃs',
		'Some files was not uploaded'           : 'DaЕѕus failus augЕЎupielДЃdД“t neizdevДЃs',
		'Unable to copy into itself'            : 'KopД“t sevД« nav iespД“jams',
		'Unable to move files'                  : 'NeizdevДЃs pДЃrvietot failus',
		'Unable to copy files'                  : 'KopД“t failus neizdevДЃs',
		'Unable to create file copy'            : 'Faila kopiju izveidot neizdevДЃs',
		'File is not an image'                  : 'Dotais fails nav attД“ls',
		'Unable to resize image'                : 'AttД“la izmД“ra maiЕ†a neizevДЃs',
		'Unable to write to file'               : 'IerakstД«t failДЃ neizdevДЃs',
		'Unable to create archive'              : 'Izveidot arhД«vu neizdevДЃs',
		'Unable to extract files from archive'  : 'IzdabЕ«t failus no arhД«va neizdevДЃs',
		'Unable to open broken link'            : 'NeizdevДЃs atvД“rt sasistu saiti',
		'File URL disabled by connector config' : 'AtbilstoЕЎi konektora iestatД«jumiem pieeja failu adresД“m ir liegta',
		/* statusbar */
		'items'          : 'objekti',
		'selected items' : 'izvД“lД“tie objekti',
		/* commands/buttons */
		'Back'                    : 'AtpakaДј',
		'Reload'                  : 'Atjaunot',
		'Open'                    : 'AtvД“rt',
		'Preview with Quick Look' : 'ДЂtrs caurskats',
		'Select file'             : 'IzvД“lД“ties failu',
		'New folder'              : 'Jauna direktorija',
		'New text file'           : 'Jauns fails',
		'Upload files'            : 'AugЕЎupielДЃdД“t failus',
		'Copy'                    : 'KopД“t',
		'Cut'                     : 'Izgriezt',
		'Paste'                   : 'Ievietot',
		'Duplicate'               : 'DublД“t',
		'Remove'                  : 'DzД“st',
		'Rename'                  : 'PДЃrsaukt',
		'Edit text file'          : 'RediДЈД“t teksta failu',
		'View as icons'           : 'Ikonas',
		'View as list'            : 'Saraksts',
		'Resize image'            : 'AttД“lu izmД“rs',
		'Create archive'          : 'Jauns arhД«vs',
		'Uncompress archive'      : 'AtvД“rt arhД«vu',
		'Get info'                : 'SaЕ†emt informДЃciju',
		'Help'                    : 'PalД«dzД«ba',
		'Dock/undock filemanager window' : 'Atvienot/pievienot failu pДЃrvaldnieku lapai',
		/* upload/get info dialogs */
		'Maximum allowed files size' : 'MaksimДЃlais atДјautais failu izmД“rs',
		'Add field'   : 'Pievienot lauku',
		'File info'   : 'Faila Д«paЕЎД«bas',
		'Folder info' : 'Direktorijas Д«paЕЎД«bas',
		'Name'        : 'VДЃrds',
		'Kind'        : 'Veids',
		'Size'        : 'IzmД“rs',
		'Modified'    : 'MainД«ts',
		'Permissions' : 'Pieejas tiesД«bas',
		'Link to'     : 'UzrДЃda uz',
		'Dimensions'  : 'AtДјauja',
		'Confirmation required' : 'NepiecieЕЎas apstiprinДЃjums',
		'Are you sure you want to remove files?<br /> This cannot be undone!' : 'Vai tieЕЎДЃm vД“laties dzД“st failus? <br />DarbД«ba neatgriezinДЃma.',
		/* permissions */
		'read'        : 'rasД«ЕЎana',
		'write'       : 'rakstД«ЕЎana',
		'remove'      : 'dzД“ЕЎana',
		/* dates */
		'Jan'         : 'Jan',
		'Feb'         : 'Feb',
		'Mar'         : 'РњР°r',
		'Apr'         : 'Рђpr',
		'May'         : 'Mai',
		'Jun'         : 'JЕ«n',
		'Jul'         : 'JЕ«l',
		'Aug'         : 'Aug',
		'Sep'         : 'Sep',
		'Oct'         : 'Okt',
		'Nov'         : 'Nov',
		'Dec'         : 'Dec',
		'Today'       : 'Е odien',
		'Yesterday'   : 'Vakar',
		/* mimetypes */
		'Unknown'                           : 'NezinДЃms',
		'Folder'                            : 'Direktorija',
		'Alias'                             : 'Saite',
		'Broken alias'                      : 'Sasista saite',
		'Plain text'                        : 'Parasts teksts',
		'Postscript document'               : 'Postscript dokuments',
		'Application'                       : 'Pielikums',
		'Microsoft Office document'         : 'Microsoft Office dokuments',
		'Microsoft Word document'           : 'Microsoft Word dokuments',  
		'Microsoft Excel document'          : 'Microsoft Excel dokuments',
		'Microsoft Powerpoint presentation' : 'Microsoft Powerpoint prezentДЃcija',
		'Open Office document'              : 'Open Office dokuments',
		'Flash application'                 : 'Flash pielikums',
		'XML document'                      : 'XML dokuments',
		'Bittorrent file'                   : 'Bittorrent fails',
		'7z archive'                        : '7z arhД«vs',
		'TAR archive'                       : 'TAR arhД«vs',
		'GZIP archive'                      : 'GZIP arhД«vs',
		'BZIP archive'                      : 'BZIP arhД«vs',
		'ZIP archive'                       : 'ZIP arhД«vs',
		'RAR archive'                       : 'RAR arhД«vs',
		'Javascript application'            : 'Javascript pielikums',
		'PHP source'                        : 'PHP izejas kods',
		'HTML document'                     : 'HTML dokuments',
		'Javascript source'                 : 'Javascript izejas kods',
		'CSS style sheet'                   : 'CSS stilu tabula',
		'C source'                          : 'C izejas kods',
		'C++ source'                        : 'C++ izejas kods',
		'Unix shell script'                 : 'Unix shell skripts',
		'Python source'                     : 'Python izejas kods',
		'Java source'                       : 'Java izejas kods',
		'Ruby source'                       : 'Ruby izejas kods',
		'Perl script'                       : 'Perl skripts',
		'BMP image'                         : 'BMP attД“ls',
		'JPEG image'                        : 'JPEG attД“ls',
		'GIF Image'                         : 'GIF attД“ls',
		'PNG Image'                         : 'PNG attД“ls',
		'TIFF image'                        : 'TIFF attД“ls',
		'TGA image'                         : 'TGA attД“ls',
		'Adobe Photoshop image'             : 'Adobe Photoshop attД“ls',
		'MPEG audio'                        : 'MPEG audio',
		'MIDI audio'                        : 'MIDI audio',
		'Ogg Vorbis audio'                  : 'Ogg Vorbis audio',
		'MP4 audio'                         : 'MP4 audio',
		'WAV audio'                         : 'WAV audio',
		'DV video'                          : 'DV video',
		'MP4 video'                         : 'MP4 video',
		'MPEG video'                        : 'MPEG video',
		'AVI video'                         : 'AVI video',
		'Quicktime video'                   : 'Quicktime video',
		'WM video'                          : 'WM video',
		'Flash video'                       : 'Flash video',
		'Matroska video'                    : 'Matroska video',
		// 'Shortcuts' : 'TaustiЕ†i',		
		'Select all files' : 'AtzД«mД“t visus failus',
		'Copy/Cut/Paste files' : 'KopД“t/Izgriezt/Ievietot failus',
		'Open selected file/folder' : 'AtvД“rt izvД“lД“to direktoriju/failu',
		'Open/close QuickLook window' : 'AtvД“rt/aizvД“rt ДЃtrДЃs apskates logu',
		'Remove selected files' : 'DzД“st atzД«mД“tos failus',
		'Selected files or current directory info' : 'InformДЃcija par atzД«mД“tajiem failiem vai esoЕЎo direktoriju',
		'Create new directory' : 'Jauna mape',
		'Open upload files form' : 'AtvД“rt failu augЕЎupielДЃdes logu',
		'Select previous file' : 'IzvД“lД“ties iepriekЕЎД“jo failu',
		'Select next file' : 'IzvД“lД“ties nДЃkamo failu',
		'Return into previous folder' : 'Atgriezties iepriekЕЎД“jДЃ direktorijДЃ',
		'Increase/decrease files selection' : 'PalielinДЃt/samazinДЃt iezД«mД“to failu skaitu',
		'Authors'                       : 'Autori',
		'Sponsors'  : 'Sponsori',
		'elFinder: Web file manager'    : 'elFinder: Failu pДЃrvaldnieks priekЕЎ Web',
		'Version'                       : 'Versija',
		'Copyright: Studio 42 LTD'      : 'Copyright: Studija 42',
		'Donate to support project development' : 'Atbalstiet izstrДЃdi',
		'Javascripts/PHP programming: Dmitry (dio) Levashov, dio@std42.ru' : 'Javascripts/php programmД“ЕЎana: Dmitrijs (dio) LevaЕЎovs, dio@std42.ru',
		'Python programming, techsupport: Troex Nevelin, troex@fury.scancode.ru' : 'Python programmД“ЕЎana, tehniskДЃ uzturД“ЕЎana: Troex Nevelin, troex@fury.scancode.ru',
		'Design: Valentin Razumnih'     : 'Dizains: ValentД«ns Razumnihs',
		'Spanish localization'          : 'SpДЃЕ†u lokalizДЃcija',
		'Icons' : 'РРєРѕРЅРєРё',
		'License: BSD License'          : 'Licence: BSD License',
		'elFinder documentation'        : 'elFinder dokumentДЃcija',
		'Simple and usefull Content Management System' : 'Д’rta un vienkДЃrЕЎa Satura PДЃrvaldes sistД“ma',
		'Support project development and we will place here info about you' : 'Atbalstiet produkta izstrДЃdi un mД“s ЕЎeit ievietosim informДЃciju par jums.',
		'Contacts us if you need help integrating elFinder in you products' : 'Ja ir nepiecieЕЎama palД«dzД«ba jЕ«su produktu integrДЃcijДЃ ar elFinder, tad sazinieties ar mums.',
		'elFinder support following shortcuts' : 'elFinder atbalsta sekojoЕЎas taustiЕ†u kombinДЃcijas',
		'helpText' : 'elFinder darbojas lД«dzД«gi jЕ«su datora failu pДЃrvaldniekam.<br />Veikt manipulДЃcijas ar failiem iespД“jams ar augЕЎД“jДЃ paneДјa pogДЃm, konteksta izvД“lnes palД«в€‚zД«bu vai taustiЕ†u kombinДЃcijДЃm. Lai pДЃrvietot failus/direktorijas, vienkДЃrЕЎi pДЃrnesiet tos/tДЃs uz nepiecieЕЎamДЃs direktorijas ikonas. Ja tajДЃ brД«dД« bЕ«s nospiests Shift taustiЕ†ЕЎ, tad faili tiks kopД“ti.'
		
	};
	
})(jQuery);
