/*
 * Ukranian translation
 * @author Artem Vasilyev
 * @version 2010-09-19
 */
(function($) {
if (elFinder && elFinder.prototype.options && elFinder.prototype.options.i18n) 
	elFinder.prototype.options.i18n.uk = {
		/* errors */
		'Root directory does not exists'       : 'РљРѕСЂРµРЅРµРІР° РґРёСЂРµРєС‚РѕСЂС–СЏ РЅРµ С–СЃРЅСѓС”',
		'Unable to connect to backend'         : 'РќРµ РІРґР°Р»РѕСЃСЏ Р·\'С”РґРЅР°С‚РёСЃСЏ Р· СЃРµСЂРІРµСЂРѕРј',
		'Access denied'                        : 'Р”РѕСЃС‚СѓРї Р·Р°Р±РѕСЂРѕРЅРµРЅРѕ',
		'Invalid backend configuration'        : 'РџРѕРјРёР»РєРё Сѓ РІС–РґРїРѕРІС–РґСЊ СЃРµСЂРІРµСЂР°',
		'Unknown command'                      : 'РќРµРІС–РґРѕРјР° РєРѕРјР°РЅРґР°',
		'Command not allowed'                  : 'Р’РёРєРѕРЅР°РЅРЅСЏ РєРѕРјР°РЅРґРё Р·Р°Р±РѕСЂРѕРЅРµРЅРѕ',
		'Invalid parameters'                   : 'РҐРёР±РЅРёР№ РїР°СЂР°РјРµС‚СЂРё',
		'File not found'                       : 'Р¤Р°Р№Р» РЅРµ Р·РЅР°Р№РґРµРЅРѕ',
		'Invalid name'                         : 'РќРµРєРѕСЂРµРєС‚РЅРёР№ С–Рј\'СЏ',
		'File or folder with the same name already exists' : 'Р¤Р°Р№Р» Р°Р±Рѕ РїР°РїРєР° Р· С‚Р°РєРѕСЋ РЅР°Р·РІРѕСЋ РІР¶Рµ С–СЃРЅСѓС”',
		'Unable to rename file'                : 'РќРµ РІРґР°Р»РѕСЃСЏ РїРµСЂРµР№РјРµРЅСѓРІР°С‚Рё С„Р°Р№Р»',
		'Unable to create folder'              : 'РќРµ РІРґР°Р»РѕСЃСЏ СЃС‚РІРѕСЂРёС‚Рё РїР°РїРєСѓ',
		'Unable to create file'                : 'РќРµ РІРґР°Р»РѕСЃСЏ СЃС‚РІРѕСЂРёС‚Рё С„Р°Р№Р»',  
		'No file to upload'                    : 'РќРµРјР°С” С„Р°Р№Р»С–РІ РґР»СЏ Р·Р°РІР°РЅС‚Р°Р¶РµРЅРЅСЏ',
		'Select at least one file to upload'   : 'Р’РёР±РµСЂС–С‚СЊ, СЏРє РјС–РЅС–РјСѓРј, РѕРґРёРЅ С„Р°Р№Р» РґР»СЏ Р·Р°РІР°РЅС‚Р°Р¶РµРЅРЅСЏ',
		'File exceeds the maximum allowed filesize' : 'Р Р°Р·РјРµСЂ С„Р°Р№Р»Р° РїСЂРµРІС‹С€Р°РµС‚ РјР°РєСЃРёРјР°Р»СЊРЅРѕ СЂР°Р·СЂРµС€РµРЅРЅС‹Р№ СЂР°Р·РјРµСЂ',
		'Not allowed file type'                 : 'РќРµСЂР°Р·СЂРµС€РµРЅРЅС‹Р№ С‚РёРї С„Р°Р№Р»Р°',
		'Unable to upload file'                 : 'РќРµ РІРґР°Р»РѕСЃСЏ Р·Р°РІР°РЅС‚Р°Р¶РёС‚Рё С„Р°Р№Р»',
		'Unable to upload files'                : 'РќРµ РІРґР°Р»РѕСЃСЏ РѕС‚СЂРёРјР°С‚Рё С„Р°Р№Р»Рё',
		'Unable to remove file'                 : 'РќРµ РІРґР°Р»РѕСЃСЏ РІРёРґР°Р»РёС‚Рё С„Р°Р№Р»',
		'Unable to save uploaded file'          : 'РќРµ РІРґР°Р»РѕСЃСЏ Р·Р±РµСЂРµРіС‚Рё Р·Р°РІР°РЅС‚Р°Р¶РµРЅРёР№ С„Р°Р№Р»',
		'Some files was not uploaded'           : 'Р”РµСЏРєС– С„Р°Р№Р»Рё РЅРµ РІРґР°Р»РѕСЃСЏ Р·Р°РІР°РЅС‚Р°Р¶РёС‚Рё',
		'Unable to copy into itself'            : 'РќРµРјРѕР¶Р»РёРІРѕ СЃРєРѕРїС–СЋРІР°С‚Рё РІ СЃРµР±Рµ',
		'Unable to move files'                  : 'РќРµ РІРґР°Р»РѕСЃСЏ РїРµСЂРµРјС–СЃС‚РёС‚Рё С„Р°Р№Р»Рё',
		'Unable to copy files'                  : 'РќРµ РІРґР°Р»РѕСЃСЏ СЃРєРѕРїС–СЋРІР°С‚Рё С„Р°Р№Р»Рё',
		'Unable to create file copy'            : 'РќРµ РІРґР°Р»РѕСЃСЏ СЃС‚РІРѕСЂРёС‚Рё РєРѕРїС–СЋ С„Р°Р№Р»Сѓ',
		'File is not an image'                  : 'Р¤Р°Р№Р» РЅРµ С” Р·РѕР±СЂР°Р¶РµРЅРЅСЏРј',
		'Unable to resize image'                : 'РќРµ РІРґР°Р»РѕСЃСЏ Р·РјС–РЅРёС‚Рё СЂРѕР·РјС–СЂРё Р·РѕР±СЂР°Р¶РµРЅРЅСЏ',
		'Unable to write to file'               : 'РќРµ РІРґР°Р»РѕСЃСЏ Р·Р°РїРёСЃР°С‚Рё С„Р°Р№Р»',
		'Unable to create archive'              : 'РќРµ РІРґР°Р»РѕСЃСЏ СЃС‚РІРѕСЂРёС‚Рё Р°СЂС…С–РІ',
		'Unable to extract files from archive'  : 'РќРµ РІРґР°Р»РѕСЃСЏ РІРёС‚СЏРіС‚Рё С„Р°Р№Р»Рё Р· Р°СЂС…С–РІСѓ',
		'Unable to open broken link'            : 'РќРµРјРѕР¶Р»РёРІРѕ РІС–РґРєСЂРёС‚Рё Р±РёС‚Сѓ РїРѕСЃРёР»Р°РЅРЅСЏ',
		'File URL disabled by connector config' : 'Р”РѕСЃС‚СѓРї РґРѕ Р°РґСЂРµСЃ С„Р°Р№Р»С–РІ Р·Р°Р±РѕСЂРѕРЅРµРЅРёР№ РЅР°Р»Р°С€С‚СѓРІР°РЅРЅСЏРјРё РєРѕРЅРЅРµРєС‚РѕСЂР°',
		/* statusbar */
		'items'          : 'РѕР±\'С”РєС‚С–РІ',
		'selected items' : 'РІРёР±СЂР°РЅРѕ РѕР±\'С”РєС‚С–РІ',
		/* commands/buttons */
		'Back'                    : 'РќР°Р·Р°Рґ',
		'Reload'                  : 'РћРЅРѕРІРёС‚Рё',
		'Open'                    : 'Р’С–РґРєСЂРёС‚Рё',
		'Preview with Quick Look' : 'РЁРІРёРґРєРёР№ РїРµСЂРµРіР»СЏРґ',
		'Select file'             : 'Р’РёР±СЂР°С‚Рё С„Р°Р№Р»',
		'New folder'              : 'РќРѕРІР° РїР°РїРєР°',
		'New text file'           : 'РќРѕРІРёР№ С„Р°Р№Р»',
		'Upload files'            : 'Р—Р°РІР°РЅС‚Р°Р¶РёС‚Рё С„Р°Р№Р»Рё',
		'Copy'                    : 'РљРѕРїС–СЋРІР°С‚Рё',
		'Cut'                     : 'Р’РёСЂС–Р·Р°С‚Рё',
		'Paste'                   : 'Р’СЃС‚Р°РІРёС‚Рё',
		'Duplicate'               : 'Р”СѓР±Р»СЋРІР°С‚Рё',
		'Remove'                  : 'Р’РёРґР°Р»РёС‚Рё',
		'Rename'                  : 'РџРµСЂРµР№РјРµРЅСѓРІР°С‚Рё',
		'Edit text file'          : 'Р РµРґР°РіСѓРІР°С‚Рё С„Р°Р№Р»',
		'View as icons'           : 'Р†РєРѕРЅРєРё',
		'View as list'            : 'РЎРїРёСЃРѕРє',
		'Resize image'            : 'Р РѕР·РјС–СЂ Р·РѕР±СЂР°Р¶РµРЅРЅСЏ',
		'Create archive'          : 'РќРѕРІРёР№ Р°СЂС…С–РІ',
		'Uncompress archive'      : 'Р РѕР·РїР°РєСѓРІР°С‚Рё Р°СЂС…С–РІ',
		'Get info'                : 'Р’Р»Р°СЃС‚РёРІРѕСЃС‚С–',
		'Help'                    : 'Р”РѕРїРѕРјРѕРіР°',
		'Dock/undock filemanager window' : 'Р’С–Рґ\'С”РґРЅР°С‚Рё/РїСЂРёС”РґРЅР°С‚Рё РњРµРЅРµРґР¶РµСЂ С„Р°Р№Р»С–РІ РґРѕ СЃС‚РѕСЂС–РЅРєРё',
		/* upload/get info dialogs */
		'Maximum allowed files size' : 'РњР°РєСЃРёРјР°Р»СЊРЅРёР№ СЂРѕР·РјС–СЂ С„Р°Р№Р»С–РІ',
		'Add field'   : 'Р”РѕРґР°С‚Рё РїРѕР»Рµ',
		'File info'   : 'Р’Р»Р°СЃС‚РёРІРѕСЃС‚С– С„Р°Р№Р»Сѓ',
		'Folder info' : 'РџР°СЂР°РјРµС‚СЂРё РїР°РїРєРё',
		'Name'        : 'РќР°Р·РІР°',
		'Kind'        : 'РўРёРї',
		'Size'        : 'Р РѕР·РјС–СЂ',
		'Modified'    : 'Р—РјС–РЅРµРЅРѕ',
		'Permissions' : 'Р”РѕСЃС‚СѓРї',
		'Link to'     : 'Р’РєР°Р·СѓС”',
		'Dimensions'  : 'Р”РѕР·РІС–Р»',
		'Confirmation required' : 'РќРµРѕР±С…С–РґРЅРѕ РїС–РґС‚РІРµСЂРґРёС‚Рё',
		'Are you sure you want to remove files?<br /> This cannot be undone!' : 'Р’Рё РІРїРµРІРЅРµРЅС–, С‰Рѕ С…РѕС‡РµС‚Рµ РІРёРґР°Р»РёС‚Рё С„Р°Р№Р»? <br /> Р”С–СЏ С” РЅРµР·РІРѕСЂРѕС‚РЅС–Рј.',
		/* permissions */
		'read'        : 'С‡РёС‚Р°РЅРЅСЏ',
		'write'       : 'Р·Р°РїРёСЃ',
		'remove'      : 'РІРёРґР°Р»РµРЅРЅСЏ',
		/* dates */
		'Jan'         : 'РЎС–С‡РµРЅСЊ',
		'Feb'         : 'Р›СЋС‚РёР№',
		'Mar'         : 'Р‘РµСЂРµР·РЅСЏ',
		'Apr'         : 'РљРІС–С‚РЅСЏ',
		'May'         : 'РўСЂР°РІРµРЅСЊ',
		'Jun'         : 'Р§РµСЂРІРЅСЏ',
		'Jul'         : 'Р›РёРїРµРЅСЊ',
		'Aug'         : 'РЎРµСЂРїРЅСЏ',
		'Sep'         : 'Р’РµСЂРµСЃРµРЅСЊ',
		'Oct'         : 'Р–РѕРІС‚РЅСЏ',
		'Nov'         : 'Р›РёСЃС‚РѕРїР°Рґ',
		'Dec'         : 'Р“СЂСѓРґРµРЅСЊ',
		'Today'       : 'РЎСЊРѕРіРѕРґРЅС–',
		'Yesterday'   : 'Р’С‡РѕСЂР°',
		/* mimetypes */
		'Unknown'                           : 'РќРµРІС–РґРѕРјРёР№',
		'Folder'                            : 'РџР°РїРєР°',
		'Alias'                             : 'РџРѕСЃРёР»Р°РЅРЅСЏ',
		'Broken alias'                      : 'Р‘РёС‚Р° РїРѕСЃРёР»Р°РЅРЅСЏ',
		'Plain text'                        : 'Р—РІРёС‡Р°Р№РЅРёР№ С‚РµРєСЃС‚',
		'Postscript document'               : 'Р”РѕРєСѓРјРµРЅС‚ postscript',
		'Application'                       : 'Р”РѕРґР°С‚РѕРє',
		'Microsoft Office document'         : 'Р”РѕРєСѓРјРµРЅС‚ Microsoft Office',
		'Microsoft Word document'           : 'Р”РѕРєСѓРјРµРЅС‚ Microsoft Word',  
		'Microsoft Excel document'          : 'Р”РѕРєСѓРјРµРЅС‚ Microsoft Excel',
		'Microsoft Powerpoint presentation' : 'РџСЂРµР·РµРЅС‚Р°С†С–СЏ Microsoft Powerpoint',
		'Open Office document'              : 'Р”РѕРєСѓРјРµРЅС‚ Open Office',
		'Flash application'                 : 'Р”РѕРґР°С‚РѕРє Flash',
		'XML document'                      : 'Р”РѕРєСѓРјРµРЅС‚ XML',
		'Bittorrent file'                   : 'Bittorrent С„Р°Р№Р»',
		'7z archive'                        : 'РђСЂС…С–РІ 7z',
		'TAR archive'                       : 'РђСЂС…С–РІ TAR',
		'GZIP archive'                      : 'РђСЂС…С–РІ GZIP',
		'BZIP archive'                      : 'РђСЂС…С–РІ BZIP',
		'ZIP archive'                       : 'РђСЂС…С–РІ ZIP',
		'RAR archive'                       : 'РђСЂС…С–РІ RAR',
		'Javascript application'            : 'Р”РѕРґР°С‚РѕРє Javascript',
		'PHP source'                        : 'РСЃС…РѕРґРЅРёРє PHP',
		'HTML document'                     : 'Р”РѕРєСѓРјРµРЅС‚ HTML',
		'Javascript source'                 : 'РСЃС…РѕРґРЅРёРє Javascript',
		'CSS style sheet'                   : 'РўР°Р±Р»РёС†СЏ СЃС‚РёР»С–РІ CSS',
		'C source'                          : 'РСЃС…РѕРґРЅРёРє C',
		'C++ source'                        : 'РСЃС…РѕРґРЅРёРє C++',
		'Unix shell script'                 : 'РЎРєСЂРёРїС‚ Unix shell',
		'Python source'                     : 'РСЃС…РѕРґРЅРёРє Python',
		'Java source'                       : 'РСЃС…РѕРґРЅРёРє Java',
		'Ruby source'                       : 'РСЃС…РѕРґРЅРёРє Ruby',
		'Perl script'                       : 'РЎРєСЂРёРїС‚ Perl',
		'BMP image'                         : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ BMP',
		'JPEG image'                        : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ JPEG',
		'GIF Image'                         : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ GIF',
		'PNG Image'                         : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ PNG',
		'TIFF image'                        : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ TIFF',
		'TGA image'                         : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ TGA',
		'Adobe Photoshop image'             : 'Р—РѕР±СЂР°Р¶РµРЅРЅСЏ Adobe Photoshop',
		'MPEG audio'                        : 'РђСѓРґС–Рѕ MPEG',
		'MIDI audio'                        : 'РђСѓРґС–Рѕ MIDI',
		'Ogg Vorbis audio'                  : 'РђСѓРґС–Рѕ Ogg Vorbis',
		'MP4 audio'                         : 'РђСѓРґС–Рѕ MP4',
		'WAV audio'                         : 'РђСѓРґС–Рѕ WAV',
		'DV video'                          : 'Р’С–РґРµРѕ DV',
		'MP4 video'                         : 'Р’С–РґРµРѕ MP4',
		'MPEG video'                        : 'Р’С–РґРµРѕ MPEG',
		'AVI video'                         : 'Р’С–РґРµРѕ AVI',
		'Quicktime video'                   : 'Р’С–РґРµРѕ Quicktime',
		'WM video'                          : 'Р’С–РґРµРѕ WM',
		'Flash video'                       : 'Р’С–РґРµРѕ Flash',
		'Matroska video'                    : 'Р’С–РґРµРѕ Matroska',
		// 'Shortcuts' : 'РљР»Р°РІРёС€Рё',		
		'Select all files' : 'Р’РёРґС–Р»РёС‚Рё РІСЃС– С„Р°Р№Р»Рё',
		'Copy/Cut/Paste files' : 'РљРѕРїС–СЋРІР°С‚Рё/Р’РёСЂС–Р·Р°С‚Рё/Р’СЃС‚Р°РІРёС‚Рё С„Р°Р№Р»Рё',
		'Open selected file/folder' : 'Р’С–РґРєСЂРёС‚Рё РїР°РїРєСѓ/С„Р°Р№Р»',
		'Open/close QuickLook window' : 'Р’С–РґРєСЂРёС‚Рё/Р·Р°РєСЂРёС‚Рё РІС–РєРЅРѕ С€РІРёРґРєРѕРіРѕ РїРµСЂРµРіР»СЏРґСѓ',
		'Remove selected files' : 'Р’РёРґР°Р»РёС‚Рё РІРёРґС–Р»РµРЅС– С„Р°Р№Р»Рё',
		'Selected files or current directory info' : 'Р†РЅС„РѕСЂРјР°С†С–СЏ РїСЂРѕ РІРёРґС–Р»РµРЅРёС… С„Р°Р№Р»С–РІ Р°Р±Рѕ РїРѕС‚РѕС‡РЅС–Р№ РїР°РїС†С–',
		'Create new directory' : 'РќРѕРІР° РїР°РїРєР°',
		'Open upload files form' : 'Р’С–РґРєСЂРёС‚Рё РІС–РєРЅРѕ Р·Р°РІР°РЅС‚Р°Р¶РµРЅРЅСЏ С„Р°Р№Р»С–РІ',
		'Select previous file' : 'Р’РёР±СЂР°С‚Рё РїРѕРїРµСЂРµРґРЅС–Р№ С„Р°Р№Р»',
		'Select next file' : 'Р’РёР±СЂР°С‚Рё РЅР°СЃС‚СѓРїРЅРёР№ С„Р°Р№Р»',
		'Return into previous folder' : 'РџРѕРІРµСЂРЅСѓС‚РёСЃСЏ РІ РїРѕРїРµСЂРµРґРЅСЋ РїР°РїРєСѓ',
		'Increase/decrease files selection' : 'Р—Р±С–Р»СЊС€РёС‚Рё/Р·РјРµРЅС€РёС‚Рё РІРёРґС–Р»РµРЅРЅСЏ С„Р°Р№Р»С–РІ',
		'Authors'                       : 'РђРІС‚РѕСЂРё',
		'Sponsors'  : 'РЎРїРѕРЅСЃРѕСЂРё',
		'elFinder: Web file manager'    : 'elFinder: Р¤Р°Р№Р»РѕРІРёР№ РјРµРЅРµРґР¶РµСЂ РґР»СЏ Web',
		'Version'                       : 'Р’РµСЂСЃС–СЏ',
		'Copyright: Studio 42 LTD'      : 'Copyright: РЎС‚СѓРґРёСЏ 42',
		'Donate to support project development' : 'РџС–РґС‚СЂРёРјР°Р№С‚Рµ СЂРѕР·СЂРѕР±РєСѓ',
		'Javascripts/PHP programming: Dmitry (dio) Levashov, dio@std42.ru' : 'РџСЂРѕРіСЂР°РјСѓРІР°РЅРЅСЏ Javascripts/php: Р”РјРёС‚СЂРёР№ (dio) Р›РµРІР°С€РѕРІ, dio@std42.ru',
		'Python programming, techsupport: Troex Nevelin, troex@fury.scancode.ru' : 'РџСЂРѕРіСЂР°РјСѓРІР°РЅРЅСЏ Python, С‚РµС…РїРѕРґРґРµСЂР¶РєР°: Troex Nevelin, troex@fury.scancode.ru',
		'Design: Valentin Razumnih'     : 'Р”РёР·Р°Р№РЅ: Р’Р°Р»РµРЅС‚РёРЅ Р Р°Р·СѓРјРЅС‹С…',
		'Spanish localization'          : 'РСЃРїР°РЅСЃРєР°СЏ Р»РѕРєР°Р»РёР·Р°С†РёСЏ',
		'Icons' : 'РРєРѕРЅРєРё',
		'License: BSD License'          : 'Р›С–С†РµРЅР·С–СЏ: BSD License',
		'elFinder documentation'        : 'Р”РѕРєСѓРјРµРЅС‚Р°С†С–СЏ elFinder',
		'Simple and usefull Content Management System' : 'РџСЂРѕСЃС‚Р° С– Р·СЂСѓС‡РЅР° РЎРёСЃС‚РµРјР° РЈРїСЂР°РІР»С–РЅРЅСЏ РЎР°Р№С‚Р°РјРё',
		'Support project development and we will place here info about you' : 'РџС–РґС‚СЂРёРјР°Р№С‚Рµ СЂРѕР·СЂРѕР±РєСѓ РїСЂРѕРґСѓРєС‚Сѓ С– РјРё СЂРѕР·РјС–СЃС‚РёРјРѕ С‚СѓС‚ С–РЅС„РѕСЂРјР°С†С–СЋ РїСЂРѕ РІР°СЃ.',
		'Contacts us if you need help integrating elFinder in you products' : 'РЇРєС‰Рѕ РІРё С…РѕС‡РµС‚Рµ С–РЅС‚РµРіСЂСѓРІР°С‚Рё elFinder РІ СЃРІС–Р№ РїСЂРѕРґСѓРєС‚, Р·РІРµСЂС‚Р°Р№С‚РµСЃСЏ РґРѕ РЅР°СЃ',
		'helpText' : 'elFinder РїСЂР°С†СЋС” Р°РЅР°Р»РѕРіС–С‡РЅРѕ РґРѕ С„Р°Р№Р»РѕРІРѕРіРѕ РјРµРЅРµРґР¶РµСЂР° Сѓ РІР°С€РѕРјСѓ РєРѕРјРї\'СЋС‚РµСЂС–. <br /> РњР°РЅС–РїСѓР»СЋРІР°С‚Рё С„Р°Р№Р»Р°РјРё РјРѕР¶РЅР° Р·Р° РґРѕРїРѕРјРѕРіРѕСЋ РєРЅРѕРїРѕРє РЅР° РІРµСЂС…РЅС–Р№ РїР°РЅРµР»С–, РєРѕРЅС‚РµРєСЃС‚РЅРѕРіРѕ РјРµРЅСЋ Р°Р±Рѕ СЃРїРѕР»СѓС‡РµРЅРЅСЏ РєР»Р°РІС–С€. Р©РѕР± РїРµСЂРµРјС–СЃС‚РёС‚Рё С„Р°Р№Р»Рё/РїР°РїРєРё, РїСЂРѕСЃС‚Рѕ РїРµСЂРµРЅРµСЃС–С‚СЊ С—С… РЅР° С–РєРѕРЅРєСѓ РїРѕС‚СЂС–Р±РЅРѕС— РїР°РїРєРё. РЇРєС‰Рѕ Р±СѓРґРµ Р·Р°С‚РёСЃРЅСѓС‚Р° РєР»Р°РІС–С€Р° Shift С„Р°Р№Р»Рё Р±СѓРґСѓС‚СЊ СЃРєРѕРїС–Р№РѕРІР°РЅС–. <br/> <br/> ElFinder РїС–РґС‚СЂРёРјСѓС” РЅР°СЃС‚СѓРїРЅС– СЃРїРѕР»СѓС‡РµРЅРЅСЏ РєР»Р°РІС–С€:'
		
	};
	
})(jQuery);
