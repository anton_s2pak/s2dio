{strip}<!doctype html>
<html xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>{if isset($title)}{$title}{else}{$title_no}{/if}</title>
<meta property="og:url" content="http://www.polesye.16mb.com"/> <!-- http://www.lysogor.com -->
<meta property="og:title" content="Свадебный фотограф в Житомире Киеве Лысогор."/>
<meta property="og:type" content="website"/>
<meta property="og:image" content="http://www.polesye.16mb.com/templates/lysogor/images/social/logo.jpg"/>
<meta property="og:image:type" content="image/jpg"/>
<meta property="og:image:width" content="200"/>
<meta property="og:image:height" content="200"/>
<meta property="og:site_name" content="lysogor.com"/>
<meta property="fb:admins" content="100001849799049"/>
<meta property="og:description" content="Подробности на http://lysogor.com"/>
<meta name="Document-state" content="Dynamic">
<meta name="revisit-after" content="3 days">
<meta name="robots" content="index, follow"/>
<meta name="keywords" content="{if isset($keywords)}{$keywords}{else}{$keywords_no}{/if}">
<meta name="description" content="{if isset($description)}{$description}{else}{$description_no}{/if}">
<meta name="google-site-verification" content="TCygDWDljBdqtBq4V5AA0QKTorwBgIqpPgILke9pxEM" />
<link rel="shortcut icon" href="/templates/{$theme}/favicon.ico">
<link rel="stylesheet" type="text/css" media="all" href="/templates/{$theme}/css/styles.css"/>
<!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="/templates/{$theme}/css/ie8lte.css" media="screen"/>
<![endif]-->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-24462904-3']);
    _gaq.push(['_setDomainName', '16mb.com']);
    _gaq.push(['_trackPageview']);
    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>
</head>
<body>{/strip}
