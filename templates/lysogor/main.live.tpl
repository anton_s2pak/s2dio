{strip}
{include file="header.tpl"}{if $pageIndex eq 1}{include_php file="modules/photos/last.php"}{/if}
<div class="slider-wrapper content-view">
    {literal}<script type="text/javascript">
        (function(d){
            var h =  (d.compatMode=='CSS1Compat' && !window.opera?d.documentElement.clientHeight:d.body.clientHeight) - 30,
                    defProp = 1920/1200 || 1.6;
            d.querySelector('.slider-wrapper').style.cssText = "width:" + h*defProp + "px; height:" + h + "px;";
        })(document);</script>{/literal}
    <div id="slider" class="{if url == portfolio}portfolio-bg-slider{else}home-bg-slider{/if}"></div>
    <div id="preloader"><!-- --></div>
    <div class="overlay"><!-- --></div>
    <div class="nav-wrapper clearfix">{include_php file="modules/pages/structure.php"}</div>
    {include file=$page}
    <span class="copyright">&copy; <a href="http://www.s2dio.com.ua/">s2dio.com.ua</a></span>
</div>
<div class="social-bar">
    <footer>
        <a href="http://www.facebook.com/LysogorPhoto" target="_blank" style="margin-right: 0px;" class="fb_link" title="Мы на Facebook"></a>
        <a href="http://vk.com/club_lysogor" target="_blank" class="vk_link"  title="Мы Вконтакте"></a>
        <a href="https://twitter.com/share" class="twitter-share-button" data-text="Свадебный фотограф в Житомире Киеве Лысогор. Подробности на " data-lang="ru" data-dnt="true">Твитнуть</a>
        <div class="g-plusone" data-size="medium"></div>
        <div id="vk_recomned">{literal}<script type="text/javascript">document.write(VK.Share.button({url: "http://lysogor.com"},{type: "button", text: "Мне нравится"}));</script>{/literal}</div>
        <div class="fb-like" data-send="false" data-layout="button_count" data-width="250" data-show-faces="true"></div>
    </footer>
</div>
<div id="fb-root"></div>
{literal}
<script src="/js/jquery.js" type="text/javascript"></script>
<script src="/js/slider.js" type="text/javascript"></script>
<script>
function clickIE(){if (document.all){return false}};function clickNS(e){if (document.layers||(document.getElementById&&!document.all)){if (e.which==2||e.which==3){return false}}};if (document.layers){document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS}else{document.onmouseup=clickNS;document.oncontextmenu=clickIE};document.oncontextmenu=new Function("return false");
(function($){
    $(window).load(function(){
        if (jQuery('.contact-form').length){
            window.tab = new lysogor.tabs('.b-tabs');
            lysogor.scroll();
        }
    });
})(jQuery);
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
(function(d) {
    var po = d.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})(document);
</script>
{/literal}
{include file="footer.tpl"}
{/strip}






 