{strip}
    {literal}<script type="text/javascript">
        window.portfolio = true;
        window.slidesJson = {/literal}{$view_cat_one}{literal}</script>
    {/literal}
{/strip}
{if !empty($text)}
<a href="javascript: void(0)" class="lysogor-popup-link"><img src="/templates/{$theme}/images/i.png" /></a>
<div class="lysogor-popup">
    <a href="javascript: void(0);" class="close">x</a>
    <div class="lysogor-popup-content">
        {$text}
    </div>
</div>
{/if}