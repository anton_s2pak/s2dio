{strip}
<div class="nav">
{function name=cats_tree parent_id=0 level=0}
    {if $data[$parent_id].childs|@sizeof > 0}
    <nav>
	<ul{if $level==0} class="menu"{/if}>
		{foreach $data[$parent_id].childs as $child_id key=key} 
		<li id="{$data[$child_id].id_item}"{if $level==0} class="nav-color-{$key}"{/if}>
		<a href="{if $data[$child_id].url == '/portfolio.html'}javascript: void(0);{else}{$data[$child_id].url}{/if}"   title="" {if $data[$child_id].id_item == $id} class="disabled" {/if} {if $data[$child_id].blank} {$data[$child_id].blank}{/if}><span>{$data[$child_id].name}</span>{if $level==0}<span>{$data[$child_id].name}</span>{/if}</a>
		{call name=cats_tree parent_id=$child_id level=level+1}
			{if $data[$child_id].url == '/portfolio.html'}
			<ul>
				{foreach from=$view_cat key=key item=item name=view_cat}
				<li><a href="/portfolio/{$item.url}" title="">{$item.name}</a></li>
				{/foreach}
			</ul>
			{/if}
		</li> 
		{/foreach}
	</ul></nav>
    {if $level==0}<a href="/" id="logo" title="lysogor.com"><img src="/templates/{$theme}/images/logo.png" alt="lysogor.com" /></a>{/if}
     {/if}
{/function}
{call name=cats_tree data=$cats_tree_data}
</div>
{/strip}
 
