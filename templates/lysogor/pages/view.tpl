{strip}{if $pageIndex ne 1}{literal}<script type="text/javascript">window.slidesJson = {/literal}{$viewLast}{literal}</script>{/literal}{/if}
{if !empty($text)}
    {if $pageIndex eq 1}
    <a href="javascript: void(0)" class="lysogor-popup-link"><img src="/templates/{$theme}/images/i.png" /></a>
    <div class="lysogor-popup">
        <a href="javascript: void(0);" class="close">x</a>
        <div class="lysogor-popup-content">
            {$text}
        </div>
    </div>
    {else}
    <div class="ls-content-wrapper">
        <div class="ls-content">
            {$text}
        </div>
    </div>
    {literal}<script type="text/javascript">window.info = true;</script>{/literal}
    {/if}
{/if}
{/strip}