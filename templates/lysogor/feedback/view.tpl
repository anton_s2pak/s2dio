{strip}{literal}<script type="text/javascript">window.slidesJson = {/literal}{$view_feedback}{literal}</script>{/literal}
<div class="contact-form main">
    <div class="b-center" style="width: 330px; background: rgba(0, 0, 0, .8); padding: 10px 20px;">
        <div class="b-tabs">
            <div id="yura" class="b-tab-item contact-person style-v3"  itemscope itemtype="http://schema.org/Person">
                <h2 class="person-name" style="float: none; width: auto; font-size: 17px;" itemprop="name">Юрий Лысогор<small>(<span itemprop="jobTitle">фотограф</span>, <span itemprop="jobTitle">оператор</span>)</small></h2>
                <div class="person-description" style="width: auto; font-size: 13px; float: none;">
                    <div class="highlighter" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="addressLocality">Житомир</span>, <span itemprop="addressLocality"> Киев</span>
                    </div>
                    <div class="highlighter"><a href="mailto:yura@lysogor.com" itemprop="email">yura@lysogor.com</a></div>
                    <div class="highlighter"><a href="http://www.lysogor.com" rel="me nofollow" itemprop="url">http://www.lysogor.com</a><br /><a href="http://lysogor.blogspot.com/" rel="me nofollow" itemprop="url">http://lysogor.blogspot.com/</a></div>
                    <div class="highlighter"><span itemprop="telephone">+38(063)603-42-42</span></div>

                </div>
            </div>
            <div id="tanya" class="b-tab-item contact-person style-v3"  itemscope itemtype="http://schema.org/Person">
                <h2 class="person-name" style="float: none; width: auto; font-size: 17px;" itemprop="name">Татьяна Лысогор<small>(<span itemprop="jobTitle">фотограф</span>)</small></h2>
                <div class="person-description" style="width: auto; font-size: 13px; float: none;">
                    <div class="highlighter" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="addressLocality">Житомир</span>, <span itemprop="addressLocality"> Киев</span>
                    </div>
                    <div class="highlighter"><a href="mailto:tanya@lysogor.com" itemprop="email">tanya@lysogor.com</a></div>
                    <div class="highlighter"><a href="http://www.lysogor.com" rel="me nofollow" itemprop="url">http://www.lysogor.com</a><br /><a href="http://vkontakte.ru/lysogor_tanya" rel="me nofollow" itemprop="url">http://vkontakte.ru/lysogor_tanya</a></div>
                    <div class="highlighter"><span itemprop="telephone">+38(050)463-24-24</span></div>

                </div>
            </div>
            <div class="c-form">
                {$form}
            </div>
        </div>
    </div>
</div>
{if !empty($text)}
<a href="javascript: void(0)" class="lysogor-popup-link"><img src="/templates/{$theme}/images/i.png" /></a>
<div class="lysogor-popup">
    <a href="javascript: void(0);" class="close">x</a>
    <div class="lysogor-popup-content">
        {$text}
    </div>
</div>
{/if}

{literal}<script type="text/javascript">window.info = true;</script>{/literal}{/strip}