<div class="m_right">
  <div class="plus_head">
    <h1>Останні новини</h1>
  </div>
  {foreach from=$view key=key item=item}
  <div class="news"><em>{$item.date}</em>
    <div class="news_short"><a href="/news/view/{$item.id_item}-{$item.url}/" title="{$item.name}" class="dashed">{$item.name|truncate:70}</a>{$item.short_text|truncate:70}</div>
  </div>
  {/foreach}
  <div style="float: right; clear:both;"> <a href="/rss/rss.xml"><img src="/templates/omkdc/images/rss.png"></a></div>
  <div style="float: left; "><a href="/news/" title="Всі новини" class="current">Всі новини</a></div>
  <br>
</div>
  
 
 
  