 <div class="plus_head">
  <h1>Пошук</h1>
  </div>
	<i>По Вашему запиту <b>«{$search}»</b> знайдено {$num}</i><br><br>
{if $num <= 0} <span style="color: red;">На жаль нічого не знайдено, спробуйте по іншому сформулювати запит. </span>
{else}
	<ul>
    {foreach from=$view key=key item=item}
	 
	<a href="/
	{if $cat eq pages}{$item.id_item}-{$item.url}
	{elseif $cat eq news}news/{$item.id_item}-{$item.url}
	{elseif $cat eq articles}articles/{$item.id_item}-{$item.url}
 
	{/if}
	/" title="{$item.name}">{$item.name}</a> <br/>
	{$item.text}
	<br/><br/> 
	{/foreach}
	</ul>
{/if}
 
 