/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Andrew Stromnov (stromnov@gmail.com). */
  $(document).ready(function() {
    $("#datapr").datepicker();
  });
  
  
   $(document).ready(function() {
       $( "#datanar" ).datepicker({ dateFormat: 'dd MM yy р.' });
  });
  

  jQuery(function($){
	$.datepicker.regional['ua'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['січня', 'лютого', 'березеня', 'квітеня ', 'травня', 'червня', 'липня', 'серпня', 'вересня', 'жовтеня', 'листопада', 'грудня'],
		monthNamesShort: ['Січ','Лют','Бер','Квіт','Трав','Чер','Лип','Сер','Вер','Жовт','Лис','Груд'],
		dayNames: ['Неділя', 'Понеділок', 'Вівторок', 'Середу', 'Четвер', 'П ятниця', 'Субота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Нд','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Не',
		dateFormat: 'DD dd MM yy р.',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ua']);
});