<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--
 * @copyright	Copyright (C) 2010 - 2011. All rights reserved.
 * @author	Original by Stypak Oleg
 * @ S2DIO.COM.UA
-->
<head>
<title>{if isset($title)}{$title}{else}{$title_no}{/if}</title>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta name="Document-state" content="Dynamic">
<meta name="revisit-after" content="3 days">
<meta name="copyright" content="Regional Medical Consultative and Diagnostic Center">
<meta name="robots" content="index, follow"/>
<meta name="keywords" content="{if isset($keywords)}{$keywords}{else}{$keywords_no}{/if}">
<meta name="description" content="{if isset($description)}{$description}{else}{$description_no}{/if}">
<link rel="stylesheet" type="text/css" media="all" href="/templates/{$theme}/css/main.css"/>
 
<script type="text/javascript" src="/js/jquery-1.5.js"></script>
<script type="text/javascript" src="/js/enlargeit/enlargeit.js"></script>
<script type="text/javascript" language="javascript" src="/js/accessible.js"></script> 
<link rel="alternate" type="application/rss+xml" title="s2dio - {$news}" href="http://{$smarty.server.SERVER_NAME}/rss/rss.xml">
<link rel="shortcut icon" href="/templates/{$theme}/favicon.gif">
</head>
<body>