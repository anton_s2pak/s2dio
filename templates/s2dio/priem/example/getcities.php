<?php
if(isset($_GET['countryCode'])){
  
  switch($_GET['countryCode']){
    
  
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
     case "Відділення променевої діагностики та компьютерної томографії":
	  echo "obj.options[obj.options.length] = new Option('R-графія органів грудної клітини','R-графія органів грудної клітини');\n";
      echo "obj.options[obj.options.length] = new Option('R-графія органів черевної порожнини (огл.)','R-графія органів черевної порожнини (огл.)');\n";
	  echo "obj.options[obj.options.length] = new Option('R-графія (скопія) стравоходу, шлунку та дванадцятипалої кишки','R-графія (скопія) стравоходу, шлунку та дванадцятипалої кишки');\n";
	  echo "obj.options[obj.options.length] = new Option('Первинне подвійне контрастування шлунку','Первинне подвійне контрастування шлунку');\n";
	  echo "obj.options[obj.options.length] = new Option('Дуоденографія','Дуоденографія');\n";
	  echo "obj.options[obj.options.length] = new Option('Ірігографія','Ірігографія');\n";
	  echo "obj.options[obj.options.length] = new Option('R-графія периферійних відділів кістяка','R-графія периферійних відділів кістяка');\n";
	  echo "obj.options[obj.options.length] = new Option('R-графія  шийного відділу хребта','R-графія  шийного відділу хребта');\n";
	  echo "obj.options[obj.options.length] = new Option('R-графія грудного відділу хребта','R-графія грудного відділу хребта');\n";
	  echo "obj.options[obj.options.length] = new Option('R-графія поперекового відділу хребта','R-графія поперекового відділу хребта');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія ребер','R –графія ребер');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія плечового суглоба','R –графія плечового суглоба');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія плечової кістки','R –графія плечової кістки');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія ліктьового суглоба ','R –графія ліктьового суглоба');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія передпліччя','R –графія передпліччя');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія кистей рук ','R –графія кистей рук');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія кісток тазу','R –графія кісток тазу');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія кульшового суглобу','R –графія кульшового суглобу');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія стегнової кістки ','R –графія стегнової кістки ');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія колінного суглобу ','R –графія колінного суглобу');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія гомілки ','R –графія гомілки');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія стопи ','R –графія стопи');\n";
	  echo "obj.options[obj.options.length] = new Option('R-графія черепа ','R-графія черепа');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія турецького сідла ','R –графія турецького сідла');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія додаткових пазух носа','R –графія додаткових пазух носа');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія скроневої кістки','R –графія скроневої кістки');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія скронево-щелепного суглоба','R –графія скронево-щелепного суглоба');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія кісток носу','R –графія кісток носу');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія зубів','R –графія зубів');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія ключиці','R –графія ключиці');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія основи черепа, соскоподібних відростків','R –графія основи черепа, соскоподібних відростків');\n";
	  echo "obj.options[obj.options.length] = new Option('R –графія стернової кістки','R –графія стернової кістки');\n";
	  echo "obj.options[obj.options.length] = new Option('Мамографія','Мамографія');\n";
	  echo "obj.options[obj.options.length] = new Option('Урографія внутрішньовенна','Урографія внутрішньовенна');\n";
	  echo "obj.options[obj.options.length] = new Option('Урографія ретроградна ','Урографія ретроградна');\n";
	  echo "obj.options[obj.options.length] = new Option('Ретроградна пієлографія та цистографія','Ретроградна пієлографія та цистографія');\n";
	  echo "obj.options[obj.options.length] = new Option('Гістерометросальпінгографія','Гістерометросальпінгографія');\n";
	  echo "obj.options[obj.options.length] = new Option('Пневмокістографія молочних залоз','Пневмокістографія молочних залоз');\n";
	  echo "obj.options[obj.options.length] = new Option('Уретрографія','Уретрографія');\n";
	  echo "obj.options[obj.options.length] = new Option('Подвійне контрастування протоків молочних залоз ','Подвійне контрастування протоків молочних залоз');\n";
	  echo "obj.options[obj.options.length] = new Option('Консультація за поданням рентгенограм з оформленням протоколу','Консультація за поданням рентгенограм з оформленням протоколу');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ  головного мозку','КТ  головного мозку');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ додаткових пазух носа ','КТ додаткових пазух носа ');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ орбіт','КТ орбіт');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ кісток  основи черепа','КТ кісток  основи черепа');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ- соскоподібних відростків (пірамід скроневих кісток, вуха)','КТ- соскоподібних відростків (пірамід скроневих кісток, вуха)');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ шийного відділу хребта','КТ шийного відділу хребта');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ грудного відділу хребта','КТ грудного відділу хребта');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ попереково-крижового відділу хребта','КТ попереково-крижового відділу хребта');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ кісток тазу','КТ кісток тазу');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ гортані, гортаноглотки','КТ гортані, гортаноглотки');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ- м’яких тканин підщелепної ділянки та м’яких тканин шиї','КТ- м’яких тканин підщелепної ділянки та м’яких тканин шиї');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ-щитоподібної залози','КТ-щитоподібної залози');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ органів грудної клітини','КТ органів грудної клітини');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ органів черевної порожнини','КТ органів черевної порожнини');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ органів черевної порожнини (контраст per os)','КТ органів черевної порожнини (контраст per os)');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ органів заочеревного простору','КТ органів заочеревного простору');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ органів малого тазу','КТ органів малого тазу');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ органів малого тазу (контраст per os, per rectum)','КТ органів малого тазу (контраст per os, per rectum)');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ-кінцівок (кісткова структура, суглоби)','КТ-кінцівок (кісткова структура, суглоби)');\n";
	  echo "obj.options[obj.options.length] = new Option('КТ-кінцівок (м’яких тканин)','КТ-кінцівок (м’яких тканин)');\n";
	  echo "obj.options[obj.options.length] = new Option('Внутрівенне підсилення при КТ-дослідженні','Внутрівенне підсилення при КТ-дослідженні');\n";
	  echo "obj.options[obj.options.length] = new Option('Внутрівенне підсилення при R –дослідженні','Внутрівенне підсилення при R –дослідженні');\n";
	  echo "obj.options[obj.options.length] = new Option('Пункційна біопсія (під контролем КТ)ОГК, печінки,  підшлункової з-зи','Пункційна біопсія (під контролем КТ)ОГК, печінки,  підшлункової з-зи');\n";
	  echo "obj.options[obj.options.length] = new Option('Пункційна біопсія (під контролем КТ) м’яких тканин, молочних залоз','Пункційна біопсія (під контролем КТ) м’яких тканин, молочних залоз');\n";
     break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
     case "Ендоскопічне відділення":
	  echo "obj.options[obj.options.length] = new Option('Езофагогастродуоденоскопія','Езофагогастродуоденоскопія');\n";
      echo "obj.options[obj.options.length] = new Option('Бронхоскопія','Бронхоскопія');\n";
	  echo "obj.options[obj.options.length] = new Option('Колоноскопія','Колоноскопія');\n";
	  echo "obj.options[obj.options.length] = new Option('Дуодентальне зондування','Дуодентальне зондування');\n";
	  echo "obj.options[obj.options.length] = new Option('Гідроколонотерапія','Гідроколонотерапія');\n";
	  echo "obj.options[obj.options.length] = new Option('Підготовка до колоноскопії','Підготовка до колоноскопії');\n";
     break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	case "Клініко-консультативне відділення":
     echo "obj.options[obj.options.length] = new Option('Консультація гастроентеролога','Консультація гастроентеролога');\n";
     echo "obj.options[obj.options.length] = new Option('Консультація невропатолога','Консультація невропатолога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація нефролога','Консультація нефролога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація отоларинголога','Консультація отоларинголога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація офтальмолога','Консультація офтальмолога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація пульмонолога','Консультація пульмонолога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація стоматолога','Консультація стоматолога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація терапевта','Консультація терапевта');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація уролога','Консультація уролога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація ендокринолога','Консультація ендокринолога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація акушер-гінеколога','Консультація акушер-гінеколога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація гінеколога-ендокринолога','Консультація гінеколога-ендокринолога');\n";
	 echo "obj.options[obj.options.length] = new Option('Консультація дерматовенеролога','Консультація дерматовенеролога');\n";
	 echo "obj.options[obj.options.length] = new Option('Забір крові з вени у маніпуляційному кабінеті','Забір крові з вени у маніпуляційному кабінеті');\n";
    break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	  
	case "Відділення УЗД":
	  echo "obj.options[obj.options.length] = new Option('Cонографія щитовидної залози ','Cонографія щитовидної залози');\n";
      echo "obj.options[obj.options.length] = new Option('Cонографія молочної залози ','Cонографія молочної залози');\n";
	  echo "obj.options[obj.options.length] = new Option('УЗД регіональних лімфовузлів','УЗД регіональних лімфовузлів');\n";
	  echo "obj.options[obj.options.length] = new Option('УЗД м’яких тканин','УЗД м’яких тканин');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія печінки ','Cонографія печінки');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія жовчного міхура і жовчних протоків ','Cонографія жовчного міхура і жовчних протоків');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія підшлункової залози ','Cонографія підшлункової залози ');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія селезінки','Cонографія селезінки');\n";
	  echo "obj.options[obj.options.length] = new Option('Функціональна проба жовчного міхура','Функціональна проба жовчного міхура');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія шлунку','Сонографія шлунку');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія 12-п кишки','Cонографія 12-п кишки');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія кишківника','Cонографія кишківника');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія нирок ','Cонографія нирок');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія нирок з водяним навантаженням','Cонографія нирок з водяним навантаженням');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія сечоводів','Сонографія сечоводів');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія наднирників','Cонографія наднирників');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія сечового міхура ','Cонографія сечового міхура');\n";
	  echo "obj.options[obj.options.length] = new Option('Визначення залишкової сечі','Визначення залишкової сечі');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія передміхурової залози','Сонографія передміхурової залози');\n";
	  echo "obj.options[obj.options.length] = new Option('Трансректальне УЗД передміхурової залози','Трансректальне УЗД передміхурової залози');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія органів калитки','Cонографія органів калитки');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія статевого члену','Сонографія статевого члену');\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія грудних залоз чоловіків','Cонографія грудних залоз чоловіків');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія слинних залоз','Сонографія слинних залоз');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія жіночих внутрішніх статевих органів','Сонографія жіночих внутрішніх статевих органів');\n";
	  echo "obj.options[obj.options.length] = new Option('Інтравагінальна сонографія жіночих внутрішніх статевих органів ','Інтравагінальна сонографія жіночих внутрішніх статевих органів ');\n";
	  echo "obj.options[obj.options.length] = new Option('УЗ-контроль визрівання фолікула','УЗ-контроль визрівання фолікула);\n";
	  echo "obj.options[obj.options.length] = new Option('Cонографія вагітності та плоду','Cонографія вагітності та плоду');\n";
	  echo "obj.options[obj.options.length] = new Option('Інтравагінальна сонографія вагітності та плоду','Інтравагінальна сонографія вагітності та плоду');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія плевральних синусів для визначення вільної рідини','Сонографія плевральних синусів для визначення вільної рідини');\n";
	  echo "obj.options[obj.options.length] = new Option('Сонографія черевного відділу аорти ','Сонографія черевного відділу аорти ');\n";
	  echo "obj.options[obj.options.length] = new Option('Доплерографія ниркових артерій','Доплерографія ниркових артерій');\n";
	  echo "obj.options[obj.options.length] = new Option('Визначення об’єму нирок ','Визначення об’єму нирок');\n";
	  echo "obj.options[obj.options.length] = new Option('УЗД  колінних суглобів ','УЗД  колінних суглобів');\n";
	  echo "obj.options[obj.options.length] = new Option('УЗД ліктьових суглобів','УЗД ліктьових суглобів');\n";
	  echo "obj.options[obj.options.length] = new Option('УЗД плечових суглобів','УЗД плечових суглобів');\n";
	  echo "obj.options[obj.options.length] = new Option('Лікувально-діагностична пункція під контролем ультразвуку ','Лікувально-діагностична пункція під контролем ультразвуку);\n";
	  echo "obj.options[obj.options.length] = new Option('ТАПБ щитовидної залози з експрес - цитологічним дослідженням під контролем УЗ','ТАПБ щитовидної залози з експрес - цитологічним дослідженням під контролем УЗ');\n";
	  echo "obj.options[obj.options.length] = new Option('ТАПБ  слинних  залоз під контролем  УЗ','ТАПБ  слинних  залоз під контролем  УЗ');\n";
	  echo "obj.options[obj.options.length] = new Option('ТАПБ  інших поверхнево розміщених утворень ','ТАПБ  інших поверхнево розміщених утворень');\n";
	  echo "obj.options[obj.options.length] = new Option('Діагностична пункція лімфовузлів з експрес-цитологічним досліджен','Діагностична пункція лімфовузлів з експрес-цитологічним досліджен');\n";
    break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	case "Клініко-діагностична лабораторія":
	 echo "obj.options[obj.options.length] = new Option('Гематологічні дослідження','Гематологічні дослідження');\n";
	 echo "obj.options[obj.options.length] = new Option('Дослідження на біохімічних аналізаторах ','Дослідження на біохімічних аналізаторах ');\n";
	 echo "obj.options[obj.options.length] = new Option('Імуноферментний аналіз (моніторинг)','Імуноферментний аналіз (моніторинг)');\n";
	 echo "obj.options[obj.options.length] = new Option('Імунологічні дослідження ','Імунологічні дослідження ');\n";
	 echo "obj.options[obj.options.length] = new Option('Загальноклінічні дослідження (моніторинг)','Загальноклінічні дослідження (моніторинг)');\n";
	 echo "obj.options[obj.options.length] = new Option('Цитологічні дослідження ','Цитологічні дослідження ');\n";
	 echo "obj.options[obj.options.length] = new Option('Молекулярно-генетичні дослідження -методом полімеразної ланцюгової реакції (ПЛР)','Молекулярно-генетичні дослідження -методом полімеразної ланцюгової реакції (ПЛР)');\n";
    break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	  
	case "Відділення функціональної діагностики":
	 echo "obj.options[obj.options.length] = new Option('Холтерівське моні торування ЕКГ','Холтерівське моні торування ЕКГ');\n";
     echo "obj.options[obj.options.length] = new Option('ЕКГ','ЕКГ');\n";
	 echo "obj.options[obj.options.length] = new Option('ЕКГ з функціональними пробами','ЕКГ з функціональними пробами');\n";
	 echo "obj.options[obj.options.length] = new Option('ЕКГ з дозованим навантаженням (велоергометрія)','ЕКГ з дозованим навантаженням (велоергометрія)');\n";
	 echo "obj.options[obj.options.length] = new Option('УЗД серця з доплерографією','УЗД серця з доплерографією');\n";
	 echo "obj.options[obj.options.length] = new Option('Спіровелоергометрія','Спіровелоергометрія');\n";
	 echo "obj.options[obj.options.length] = new Option('Спірографія','Спірографія');\n";
	 echo "obj.options[obj.options.length] = new Option('Спірографія з функціональними пробами','Спірографія з функціональними пробами');\n";
	 echo "obj.options[obj.options.length] = new Option('Електроенцефалографія ','Електроенцефалографія ');\n";
	 echo "obj.options[obj.options.length] = new Option('Електроенцефалографія з функціональними пробами','Електроенцефалографія з функціональними пробами');\n";
	 echo "obj.options[obj.options.length] = new Option('Реоенцефалографія','Реоенцефалографія');\n";
	 echo "obj.options[obj.options.length] = new Option('Реоенцефалографія з функціональними пробами','Реоенцефалографія з функціональними пробами');\n";
	 echo "obj.options[obj.options.length] = new Option('Реовазографія  верхніх кінцівок','Реовазографія  верхніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Реовазографія  нижніх кінцівок','Реовазографія  нижніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Реовазографія  верхніх кінцівок з функціональними пробами','Реовазографія  верхніх кінцівок з функціональними пробами');\n";
	 echo "obj.options[obj.options.length] = new Option('Реовазографія  нижніх кінцівок з функціональними пробами','Реовазографія  нижніх кінцівок з функціональними пробами');\n";
	 echo "obj.options[obj.options.length] = new Option('УЗД венозної системи','УЗД венозної системи');\n";
	 echo "obj.options[obj.options.length] = new Option('УЗД судин брахіоцефальної області','УЗД судин брахіоцефальної області');\n";
	 echo "obj.options[obj.options.length] = new Option('УЗД судин верхніх кінцівок','УЗД судин верхніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('УЗД судин нижніх кінцівок','УЗД судин нижніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування брахіоцефальної області','Ультразвукове ангіосканування брахіоцефальної області');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування артерій верхніх кінцівок','Ультразвукове ангіосканування артерій верхніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування вен верхніх кінцівок','Ультразвукове ангіосканування вен верхніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування артерій нижніх кінцівок','Ультразвукове ангіосканування артерій нижніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування артерій нижніх кінцівок','Ультразвукове ангіосканування артерій нижніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування вен нижніх кінцівок','Ультразвукове ангіосканування вен нижніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Транскраніальне ангіосканування','Транскраніальне ангіосканування');\n";
	 echo "obj.options[obj.options.length] = new Option('Діастолічна функція лівого шлуночка ','Діастолічна функція лівого шлуночка');\n";
	 echo "obj.options[obj.options.length] = new Option('Ультразвукове ангіосканування артерій стопи ','Ультразвукове ангіосканування артерій стопи ');\n";
	 echo "obj.options[obj.options.length] = new Option('Доплерографія брахіоцефальної області ','Доплерографія брахіоцефальної області');\n";
	 echo "obj.options[obj.options.length] = new Option('Доплерографія судин верхніх кінцівок','Доплерографія судин верхніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Доплерографія судин нижніх кінцівок','Доплерографія судин нижніх кінцівок');\n";
	 echo "obj.options[obj.options.length] = new Option('Визначення об’єму кровотоку в дистальному сегменті','Визначення об’єму кровотоку в дистальному сегменті');\n";
	 echo "obj.options[obj.options.length] = new Option('Визначення індексу Ліндегарда','Визначення індексу Ліндегарда');\n";
	 echo "obj.options[obj.options.length] = new Option('Метод викликаних потенціалів','Метод викликаних потенціалів');\n";
	 echo "obj.options[obj.options.length] = new Option('Прийом та розшифровка ЕКГ по системі “Телекард” і „Тредекс”','Прийом та розшифровка ЕКГ по системі “Телекард” і „Тредекс”');\n";
    break;
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	  
	case "Кардіодиспансерне відділення":
	echo "obj.options[obj.options.length] = new Option('Консультація кардіолога','Консультація кардіолога');\n";
    echo "obj.options[obj.options.length] = new Option('Консультація ревматолога','Консультація ревматолога');\n";
	echo "obj.options[obj.options.length] = new Option('Консультація невропатолога КДВ','Консультація невропатолога КДВ');\n";
    break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	  
	case "Відділ по ремонту медичної техніки":
    break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	  
	case "Госпрозрахункове відділення":
    break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	  
  }  
}
?> 