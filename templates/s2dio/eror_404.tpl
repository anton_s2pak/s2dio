<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
	<head>
	{literal}
		<style type="text/css">
			body {color:#222; font-size:13px;font-family: sans-serif; background:#fff url(/templates/s2tem/images/404-page.jpg) right top no-repeat;}
			h1 {font-size:600%;font-family:'Trebuchet MS', Verdana, sans-serif; color:#000}
			#page {font-size:122%;width:720px; margin:144px auto 0 auto;text-align:left;line-height:1.2;}
			#message {padding-right:400px;min-height:360px;background:transparent url(/templates/s2tem/images/eror_404.png) 360px 20px no-repeat;}
		</style>
	{/literal}
		<meta http-equiv="Content-Type" content="application/xhtml; charset=utf-8" />
		<meta name="description" content="Произошла ошибка №404 на сайте.
			Вообще-то здесь заказывают создание и продвижение сайтов, разработку логотипов, дизайн для печати и прочее" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<title>Error 404 – Page not found @ s2dio.com.ua</title>
	</head>

	<body>
		<div id="page">
		<div id="message">
			<h1>404</h1>
	<p>Неправильно набран адрес, или такой страницы на&nbsp;сайте больше не&nbsp;существует.</p>
				<p><a href="/">Вернуться на&nbsp;главную</a></p>

	 
		</div>

		</div>
	</body>
</html>
