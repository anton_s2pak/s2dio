{strip}
 
{function name=cats_tree parent_id=0}
   {if $data[$parent_id].childs|@sizeof > 0}
 <ul  id="menu">
     {foreach $data[$parent_id].childs as $child_id} 
      <li id="mainmenu_{$data[$child_id].id_item}">
       <a href="/{$data[$child_id].url}.html"  title="{$data[$child_id].name}" {if $data[$child_id].id_item == $id} class="disabled" {/if}> {$data[$child_id].name}</a>
        {call name=cats_tree parent_id=$child_id}
			{if $data[$child_id].url == portfolio}
			<ul>
				{foreach from=$view_cat key=key item=item name=view_cat}
				<li><a href="/portfolio/{$item.url}" title="{$item.name}">{$item.name}</a>  </li>
				{/foreach}
			</ul>
			{/if}
       </li> 

 
 
      {/foreach}
  </ul>
     {/if}	
{/function}
{call name=cats_tree data=$cats_tree_data}            
                                    
{/strip}



 
