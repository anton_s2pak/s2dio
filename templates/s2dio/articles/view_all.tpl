{literal}

<style>
/*карта*/

    .YMaps-btn{
        top:-35px;
        width: 175px!important;
        right: 15px!important;
    }
    .YMaps-btn-i{
        color:red!important;
        width: 156px!important;
    }
    .YMaps-search-control-submit{
        background:url("/templates/omkdc/images/search_button.png") no-repeat scroll 0 0 transparent!important;
        border: medium none;
        width: 26px!important;
    }
    .YMaps-search-control-submit i{
        display:none!important;
    }
    .YMaps-search-control-text{
    border: medium none!important;
    color: #999999!important;
    float: left!important;
    font-style: italic!important;
    height: 20px!important;
    /*margin: 7px 0 0 7px!important;*/
     width: 136px!important;
    }
    .YMaps-btn-list-item{
        width:158px;
    }
    .YMaps-next{
        margin-right:10px;
    }

	.info_conteiner p {
		
		line-height: 18px;
	}
	.info_conteiner p img{
	/*	float:left;*/
	}
	.YMaps-map-type-layer-container{
		opacity:0.7;
	}






/*Контейнер голоосования*/	
.vote-container{
	position:relative; 
	overflow:hidden;
	}
	
	
	
/*Кнопки голосования*/	
.blue {
    background: url("/templates/omkdc/images/button_vote/button_blue.png") repeat scroll 0 0 transparent;
}

.red {
    background: url("/templates/omkdc/images/button_vote/button_red.png") repeat scroll 0 0 transparent;
}

.yellow {
    background: url("/templates/omkdc/images/button_vote/button_yellow.png") repeat scroll 0 0 transparent;
}

.green {
    background: url("/templates/omkdc/images/button_vote/button_green.png") repeat scroll 0 0 transparent;
}



/*Кнопки при наведении*/	
.blue:hover{
     background:url("/templates/omkdc/images/button_vote/button_blue_h.png");
	 color:#FFF;
}
 
.red:hover{
     background:url("/templates/omkdc/images/button_vote/button_red_h.png");
	 color:#FFF;
}

.yellow:hover{
     background:url("/templates/omkdc/images/button_vote/button_yellow_h.png");
	 color:#FFF;
 }
 
.green:hover{
     background:url("/templates/omkdc/images/button_vote/button_green_h.png");
	 color:#FFF;
 }





/*Не активные кнопки*/ 
.already_voted .green {
    background: url("/templates/omkdc/images/button_vote/green.png") no-repeat scroll 0 0 transparent;
}


.already_voted .red {
    background: url("/templates/omkdc/images/button_vote/red.png") no-repeat scroll 0 0 transparent;
}

.already_voted .yellow {
    background: url("/templates/omkdc/images/button_vote/yelow.png") no-repeat scroll 0 0 transparent;
}

.already_voted .blue {
    background: url("/templates/omkdc/images/button_vote/blue.png") no-repeat scroll 0 0 transparent;
}




a.down_v, a.up_v {
    color: #FFFFFF;
    display: block;
    float: left;
    font-size: 24px;
    font-weight: bold;
    height: 70px;
    line-height: 44px;
    margin: 0 5px;
    text-align: center;
    text-decoration: none;
    text-shadow: 0 1px 0 white;
    width: 104px;
}
	

a.down_v span, a.up_v span {
    float: left;
    font-size: 12px;
    line-height: 22px;
	
    text-shadow: 0 -1px 0 #000000;
    width: 104px;
}
	

 
/*График голосования*/
.graf-container{
	width:300px;
	float:left;

}


#greebar
{
float:left;
background-color:#aada37;
border:solid 1px #698a14;
width:0px;
margin:10px 0 3px 0;
height:17px;
font-size:10px;
color:#cf362f;
padding-left:10px;
}
#redbar
{
float:left;
background-color:#cf362f;
border:solid 1px #881811;
width:0px;
margin:3px 0 10px 0;
height:17px;
font-size:10px;
color:#FFF; 
padding-left:10px;
}


</style>
 
<script type="text/javascript" src="http://api-maps.yandex.ru/1.1/index.xml?key=ADjB2E4BAAAA-F2iTAIAHVGk2WI9iYBZOnG9KQtddZGQsZ4AAAAAAAAAAACybrZJFXQRnonQq4TllCgK7WCTzw==&modules=regions"></script>
<script type="text/javascript" src="/js/map7.js"></script>
<script type="text/javascript">
 
		
		
/*---------------------------------------------------------------------------------------------------------*/
$(function() {
$(".down_v, .up_v").click(function()
  {
	  
 
     var id = $(this).attr("id");
     var name = $(this).attr("name"); 
     var region = $('.loc_val').val();
     var dataString = 'id='+ id + '&name='+ name +'&location='+ region;
     var $this = $(this);
	 var $parent = $this.parents('.vote-container');
	 

	
    $.ajax({
     type: "POST", 
	 dataType : "json",
     url: "/ajax/votes/",
     data: dataString,
     cache: false,
     success: function(data)
     {

     // Получаем результат с сервера и вставляем в нужное место
 	$parent.find('.up_voting a span').text(data.vote_up);
	$parent.find('.down_voting a span').text(data.vote_down);
	$parent.find('.sum_voting').text(data.sum_voting);

	//Граффик
	$parent.find('#greebar').width(data.vote_up);
	$parent.find('#redbar').width(data.vote_down);
	$parent.find('#greebar').text(data.vote_up);
	$parent.find('#redbar').text(data.vote_down);
	
	//Сообщение об ошибке
    if(data.massge == false){
	  $parent.find('#info_vote').text("Вы уже оставили свой голосс!");
	  $parent.addClass("already_voted");
	}else{
	  $parent.find('#info_vote').slideDown(10).delay(1800).fadeOut(1400).text("Ваш голос принят!");
	}
	
	
     } 
   });
 
 return false;
 });
});
</script>
{/literal}



 
  <div class="plus_head">
  <h1>{$nameserch}{if $id_item}{foreach from=$view_cat key=k item=v}{if $id_item == $v.id_item} {$v.name}{/if}{/foreach}{elseif !$id_item}Всі записи розділу{/if}  </h1>
  </div>
  
  <input value="" type="text" class="loc_val">  

  
           {foreach from=$view_all key=key item=item}
<div class="block_q" id="{$item.id_item}" >
         
           
		    <h2><a href="/blog/view/{$item.url}" class="dashed">{$item.name}</a></h2>
			<div class="autor">{if $item.autor}<a href ="/blog/view/search/{$item.autor}/"> {$item.autor}</a>, {/if}{$item.date}, переглядів: {$item.view} </div>
   		    
           
{******************************Карта**************************************}   
 


 

 <div id="YMapsID" style="width:700px; height:300px;"></div>
 

<div id="load_id" class="load_map" >
	<div>
		<span>
		<img src="/images/ajax-loader.gif" />&nbsp;&nbsp;Подключается сервис Яндекс.Карты
		</span>
	</div>
</div>
{************************************************************************} 
        
        	
            
            <span class="news_text">{$item.short_text}</span>
            
 	
  
  
  
{**************************Контейнер кнопок************************************************}  
<div class="vote-container">
   

<div class="up_voting">
<a href="" class="up_v green" id="{$item.id_item}" name="up_voting">Да<span>{$item.up_voting}%</span></a>
</div>

<div class="down_voting">
<a href="" class="down_v red" id="{$item.id_item}" name="down_voting">Нет<span>{$item.down_voting}%</span></a>
</div>


{******************************Графики**************************************************}               
 
<div class="graf-container">
<div id="greebar" style="width:{$item.up_voting}%">{$item.up_voting}%</div> 
<div id="redbar" style="width:{$item.down_voting}%">{$item.down_voting}%</div>
</div>


{******************************Общее колличество голосов********************************}  
<div class="sum_voting">
 {$item.sum_voting}
</div>


{******************************Информация**********************************************}  
<div id="info_vote"></div>
 
{******************************Место расположения**************************************}        
<div id="location"></div> 


</div>



 
 
 
 
 
 
 
 

 
 
 
            
            <hr width="100%">
	</div>
		{/foreach}
		
        
		
{foreach from=$view_cat key=key item=item}
     <a href="/blog/{$item.id_item}/" class="dashed" 
	{foreach from=$view_all key=key item=items}{if  $items.id_category == $item.id_item}style="text-decoration:none;"{/if}{/foreach}>{$item.name}</a>  &nbsp;  
 
 {/foreach}
		
 
		 
		
		<div style="float:right;">
 	 	
		 {foreach from=$nav_page key=key item=item}
  			{if $item.i == $item.page}<a href="/blog/{$id_item}/page/{$item.i}/"  class="s2button-selected">{$item.i}</a>
  			{else}<a href="/blog/{$id_item}/page/{$item.i}/"  class="s2button">{$item.i}</a>{/if}
  		{/foreach}
        {foreach from=$nav_page2 key=key item=item}
  			{if $item.i == $item.page}<a href="/blog/{$id_item}/page/{$item.i}/"  class="s2button-selected">{$item.i}</a>
  			{else}<a href="/blog/{$id_item}/page/{$item.i}/"  class="s2button">{$item.i}</a>{/if}
        {/foreach}
		

				</div>
				
	
		 {literal}
 
 
 <script type="text/javascript">

var RegionMAP={};
RegionMAP[16]={};RegionMAP[16]['NAME']='Алтайский край';RegionMAP[16]['TOWN']='г.Барнаул';RegionMAP[16]['COVER']=50;RegionMAP[16]['URL']='?categoryFV=6069&regionFV=16&type_clientFV=';RegionMAP[16]['voteup']='52';RegionMAP[16]['votedown']='30';
RegionMAP[17]={};RegionMAP[17]['NAME']='Краснодарский край';RegionMAP[17]['TOWN']='г.Краснодар';RegionMAP[17]['COVER']=100;RegionMAP[17]['URL']='?categoryFV=6069&regionFV=17&type_clientFV=';RegionMAP[17]['voteup']='52';RegionMAP[17]['votedown']='30';
RegionMAP[18]={};RegionMAP[18]['NAME']='Красноярский край';RegionMAP[18]['TOWN']='г.Красноярск';RegionMAP[18]['COVER']=100;RegionMAP[18]['URL']='?categoryFV=6069&regionFV=18&type_clientFV=';RegionMAP[18]['voteup']='52';RegionMAP[18]['votedown']='30';
RegionMAP[19]={};RegionMAP[19]['NAME']='Приморский край';RegionMAP[19]['TOWN']='г.Владивосток';RegionMAP[19]['COVER']=50;RegionMAP[19]['URL']='?categoryFV=6070&regionFV=19&type_clientFV=';RegionMAP[19]['voteup']='52';RegionMAP[19]['votedown']='30';
RegionMAP[20]={};RegionMAP[20]['NAME']='Ставропольский край';RegionMAP[20]['TOWN']='г.Ставрополь';RegionMAP[20]['COVER']=100;RegionMAP[20]['URL']='?categoryFV=6069&regionFV=20&type_clientFV=';RegionMAP[20]['voteup']='52';RegionMAP[20]['votedown']='30';
RegionMAP[21]={};RegionMAP[21]['NAME']='Хабаровский край';RegionMAP[21]['TOWN']='г.Хабаровск';RegionMAP[21]['COVER']=50;RegionMAP[21]['URL']='?categoryFV=6070&regionFV=21&type_clientFV=';
RegionMAP[22]={};RegionMAP[22]['NAME']='Амурская область';RegionMAP[22]['TOWN']='г.Благовещенск';RegionMAP[22]['COVER']=50;RegionMAP[22]['URL']='?categoryFV=6070&regionFV=22&type_clientFV=';
RegionMAP[23]={};RegionMAP[23]['NAME']='Архангельская область';RegionMAP[23]['TOWN']='г.Архангельск';RegionMAP[23]['COVER']=50;RegionMAP[23]['URL']='?categoryFV=6070&regionFV=23&type_clientFV=';
RegionMAP[24]={};RegionMAP[24]['NAME']='Астраханская область';RegionMAP[24]['TOWN']='г.Астрахань';RegionMAP[24]['COVER']=50;RegionMAP[24]['URL']='?categoryFV=6070&regionFV=24&type_clientFV=';
RegionMAP[25]={};RegionMAP[25]['NAME']='Белгородская область';RegionMAP[25]['TOWN']='г.Белгород';RegionMAP[25]['COVER']=50;RegionMAP[25]['URL']='?categoryFV=6070&regionFV=25&type_clientFV=';
RegionMAP[26]={};RegionMAP[26]['NAME']='Брянская область';RegionMAP[26]['TOWN']='г.Брянск';RegionMAP[26]['COVER']=100;RegionMAP[26]['URL']='?categoryFV=6069&regionFV=26&type_clientFV=';
RegionMAP[27]={};RegionMAP[27]['NAME']='Владимирская область';RegionMAP[27]['TOWN']='г.Владимир, г.Муром';RegionMAP[27]['COVER']=50;RegionMAP[27]['URL']='?categoryFV=6070&regionFV=27&type_clientFV=';
RegionMAP[28]={};RegionMAP[28]['NAME']='Волгоградская область';RegionMAP[28]['TOWN']='г.Волгоград, г.Волжский';RegionMAP[28]['COVER']=100;RegionMAP[28]['URL']='?categoryFV=6069&regionFV=28&type_clientFV=';
RegionMAP[29]={};RegionMAP[29]['NAME']='Вологодская область';RegionMAP[29]['TOWN']='г.Вологда, г.Череповец';RegionMAP[29]['COVER']=100;RegionMAP[29]['URL']='?categoryFV=6069&regionFV=29&type_clientFV=';
RegionMAP[30]={};RegionMAP[30]['NAME']='Воронежская область';RegionMAP[30]['TOWN']='г.Воронеж';RegionMAP[30]['COVER']=50;RegionMAP[30]['URL']='?categoryFV=6070&regionFV=30&type_clientFV=';
RegionMAP[31]={};RegionMAP[31]['NAME']='Нижегородская область';RegionMAP[31]['TOWN']='г.Нижний Новгород, г.Дзержинск, г.Бор, г.Кстово';RegionMAP[31]['COVER']=100;RegionMAP[31]['URL']='?categoryFV=6069&regionFV=31&type_clientFV=';
RegionMAP[32]={};RegionMAP[32]['NAME']='Ивановская область';RegionMAP[32]['TOWN']='г.Иваново';RegionMAP[32]['COVER']=100;RegionMAP[32]['URL']='?categoryFV=6069&regionFV=32&type_clientFV=';
RegionMAP[33]={};RegionMAP[33]['NAME']='Иркутская область';RegionMAP[33]['TOWN']='г.Иркутск, г.Ангарск';RegionMAP[33]['COVER']=50;RegionMAP[33]['URL']='?categoryFV=6070&regionFV=33&type_clientFV=';
RegionMAP[35]={};RegionMAP[35]['NAME']='Калининградская область';RegionMAP[35]['TOWN']='г.Калининград';RegionMAP[35]['COVER']=100;RegionMAP[35]['URL']='?categoryFV=6069&regionFV=35&type_clientFV=';
RegionMAP[36]={};RegionMAP[36]['NAME']='Тверская область';RegionMAP[36]['TOWN']='г.Тверь';RegionMAP[36]['COVER']=50;RegionMAP[36]['URL']='?categoryFV=6070&regionFV=36&type_clientFV=';
RegionMAP[37]={};RegionMAP[37]['NAME']='Калужская область';RegionMAP[37]['TOWN']='г.Калуга';RegionMAP[37]['COVER']=50;RegionMAP[37]['URL']='?categoryFV=6070&regionFV=37&type_clientFV=';
RegionMAP[38]={};RegionMAP[38]['NAME']='Камчатский край';RegionMAP[38]['TOWN']='г.Петропавловск-Камчатский';RegionMAP[38]['COVER']=50;RegionMAP[38]['URL']='?categoryFV=6070&regionFV=38&type_clientFV=';
RegionMAP[39]={};RegionMAP[39]['NAME']='Кемеровская область';RegionMAP[39]['TOWN']='г.Кемерово, г.Новокузнецк, г.Прокопьевск, г.Киселевск';RegionMAP[39]['COVER']=50;RegionMAP[39]['URL']='?categoryFV=6070&regionFV=39&type_clientFV=';
RegionMAP[40]={};RegionMAP[40]['NAME']='Кировская область';RegionMAP[40]['TOWN']='г.Киров, г.Кирово-Чепецк, г.Слободской';RegionMAP[40]['COVER']=50;RegionMAP[40]['URL']='?categoryFV=6070&regionFV=40&type_clientFV=';
RegionMAP[41]={};RegionMAP[41]['NAME']='Костромская область';RegionMAP[41]['TOWN']='г.Кострома';RegionMAP[41]['COVER']=50;RegionMAP[41]['URL']='?categoryFV=6070&regionFV=41&type_clientFV=';
RegionMAP[42]={};RegionMAP[42]['NAME']='Самарская область';RegionMAP[42]['TOWN']='г.Самара, г.Тольятти';RegionMAP[42]['COVER']=100;RegionMAP[42]['URL']='?categoryFV=6069&regionFV=42&type_clientFV=';
RegionMAP[43]={};RegionMAP[43]['NAME']='Курганская область';RegionMAP[43]['TOWN']='г Курган';RegionMAP[43]['COVER']=50;RegionMAP[43]['URL']='?categoryFV=6070&regionFV=43&type_clientFV=';
RegionMAP[44]={};RegionMAP[44]['NAME']='Курская область';RegionMAP[44]['TOWN']='г.Курск';RegionMAP[44]['COVER']=50;RegionMAP[44]['URL']='?categoryFV=6070&regionFV=44&type_clientFV=';
RegionMAP[45]={};RegionMAP[45]['NAME']='Ленинградская область';RegionMAP[45]['TOWN']='г. Санкт-Петербург со всеми пригородами';RegionMAP[45]['COVER']=100;RegionMAP[45]['URL']='?categoryFV=6069&regionFV=45&type_clientFV=';
RegionMAP[46]={};RegionMAP[46]['NAME']='Липецкая область';RegionMAP[46]['TOWN']='г.Липецк';RegionMAP[46]['COVER']=50;RegionMAP[46]['URL']='?categoryFV=6070&regionFV=46&type_clientFV=';
RegionMAP[47]={};RegionMAP[47]['NAME']='Магаданская область';RegionMAP[47]['TOWN']='г.Магадан';RegionMAP[47]['COVER']=50;RegionMAP[47]['URL']='?categoryFV=6070&regionFV=47&type_clientFV=';
RegionMAP[48]={};RegionMAP[48]['NAME']='Московская область';RegionMAP[48]['TOWN']='г.Москва и Московская область';RegionMAP[48]['COVER']=100;RegionMAP[48]['URL']='?categoryFV=6069&regionFV=48&type_clientFV=';
RegionMAP[49]={};RegionMAP[49]['NAME']='Мурманская область';RegionMAP[49]['TOWN']='г.Мурманск';RegionMAP[49]['COVER']=50;RegionMAP[49]['URL']='?categoryFV=6070&regionFV=49&type_clientFV=';
RegionMAP[50]={};RegionMAP[50]['NAME']='Новгородская область';RegionMAP[50]['TOWN']='г.Великий Новгород';RegionMAP[50]['COVER']=50;RegionMAP[50]['URL']='?categoryFV=6070&regionFV=50&type_clientFV=';
RegionMAP[51]={};RegionMAP[51]['NAME']='Новосибирская область';RegionMAP[51]['TOWN']='г.Новосибирск, г.Бердск, г.Искитим';RegionMAP[51]['COVER']=100;RegionMAP[51]['URL']='?categoryFV=6069&regionFV=51&type_clientFV=';
RegionMAP[52]={};RegionMAP[52]['NAME']='Омская область';RegionMAP[52]['TOWN']='г.Омск';RegionMAP[52]['COVER']=50;RegionMAP[52]['URL']='?categoryFV=6070&regionFV=52&type_clientFV=';
RegionMAP[53]={};RegionMAP[53]['NAME']='Оренбургская область';RegionMAP[53]['TOWN']='г.Оренбург, г.Орск, г.Новотроицк';RegionMAP[53]['COVER']=100;RegionMAP[53]['URL']='?categoryFV=6069&regionFV=53&type_clientFV=';
RegionMAP[54]={};RegionMAP[54]['NAME']='Орловская область';RegionMAP[54]['TOWN']='г.Орел';RegionMAP[54]['COVER']=50;RegionMAP[54]['URL']='?categoryFV=6070&regionFV=54&type_clientFV=';
RegionMAP[55]={};RegionMAP[55]['NAME']='Пензенская область';RegionMAP[55]['TOWN']='г.Пенза';RegionMAP[55]['COVER']=50;RegionMAP[55]['URL']='?categoryFV=6070&regionFV=55&type_clientFV=';
RegionMAP[56]={};RegionMAP[56]['NAME']='Пермский край';RegionMAP[56]['TOWN']='г.Пермь';RegionMAP[56]['COVER']=100;RegionMAP[56]['URL']='?categoryFV=6069&regionFV=56&type_clientFV=';
 
RegionMAP[110]={};RegionMAP[110]['NAME']='Ханты-Мансийский автономный округ';RegionMAP[110]['TOWN']='г.Ханты-Мансийск';RegionMAP[110]['COVER']=50;RegionMAP[110]['URL']='?categoryFV=6070&regionFV=67&type_clientFV=';
RegionMAP[111]={};RegionMAP[111]['NAME']='Ямало-Ненецкий автономный округ';RegionMAP[111]['TOWN']='г.Салехард';RegionMAP[111]['COVER']=50;RegionMAP[111]['URL']='?categoryFV=6070&regionFV=67&type_clientFV=';
RegionMAP[112]={};RegionMAP[112]['NAME']='Житомирская область';RegionMAP[112]['TOWN']='г.Салехард';RegionMAP[112]['COVER']=50;RegionMAP[112]['URL']='?categoryFV=6070&regionFV=67&type_clientFV=';
RegionMAP[113]={};RegionMAP[113]['NAME']='Киевская область';RegionMAP[113]['TOWN']='г.Салехард';RegionMAP[113]['COVER']=100;RegionMAP[113]['URL']='?categoryFV=6070&regionFV=67&type_clientFV=';

 
</script>
 {/literal}
		
		
        