<?php /* Smarty version Smarty-3.0.8, created on 2012-11-04 15:10:41
         compiled from ".\templates\lysogor/pages/structure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27184509669512a4f63-42048796%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b979d441a6626c68ccb8ee49fae744eb34c79d9' => 
    array (
      0 => '.\\templates\\lysogor/pages/structure.tpl',
      1 => 1352034633,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27184509669512a4f63-42048796',
  'function' => 
  array (
    'cats_tree' => 
    array (
      'parameter' => 
      array (
        'parent_id' => 0,
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'has_nocache_code' => 0,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="nav"><?php if (!function_exists('smarty_template_function_cats_tree')) {
    function smarty_template_function_cats_tree($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->template_functions['cats_tree']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?><?php if (sizeof($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs'])>0){?><nav><ul<?php if ($_smarty_tpl->getVariable('level')->value==0){?> class="menu"<?php }?>><?php  $_smarty_tpl->tpl_vars['child_id'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->getVariable('parent_id')->value]['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['child_id']->key => $_smarty_tpl->tpl_vars['child_id']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['child_id']->key;
?><li id="<?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item'];?>
"<?php if ($_smarty_tpl->getVariable('level')->value==0){?> class="nav-color-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"<?php }?>><a href="<?php if ($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['url']=='/portfolio.html'){?>javascript: void(0);<?php }else{ ?><?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['url'];?>
<?php }?>"   title="" <?php if ($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['id_item']==$_smarty_tpl->getVariable('id')->value){?> class="disabled" <?php }?> <?php if ($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['blank']){?> <?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['blank'];?>
<?php }?>><span><?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['name'];?>
</span><?php if ($_smarty_tpl->getVariable('level')->value==0){?><span><?php echo $_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['name'];?>
</span><?php }?></a><?php smarty_template_function_cats_tree($_smarty_tpl,array('parent_id'=>$_smarty_tpl->tpl_vars['child_id']->value,'level'=>'level'+1));?>
<?php if ($_smarty_tpl->getVariable('data')->value[$_smarty_tpl->tpl_vars['child_id']->value]['url']=='/portfolio.html'){?><ul><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('view_cat')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?><li><a href="/portfolio/<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" title=""><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></li><?php }} ?></ul><?php }?></li><?php }} ?></ul></nav><?php if ($_smarty_tpl->getVariable('level')->value==0){?><a href="/" id="logo" title="lysogor.com"><img src="/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/logo.png" alt="lysogor.com" /></a><?php }?><?php }?><?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;}}?>
<?php smarty_template_function_cats_tree($_smarty_tpl,array('data'=>$_smarty_tpl->getVariable('cats_tree_data')->value));?>
</div>
 
