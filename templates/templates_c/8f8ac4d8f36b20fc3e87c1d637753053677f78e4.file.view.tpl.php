<?php /* Smarty version Smarty-3.0.8, created on 2012-11-04 11:02:07
         compiled from "templates/lysogor\feedback/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1846550962f0f9a0cd8-16811277%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8f8ac4d8f36b20fc3e87c1d637753053677f78e4' => 
    array (
      0 => 'templates/lysogor\\feedback/view.tpl',
      1 => 1352019638,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1846550962f0f9a0cd8-16811277',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">window.slidesJson = <?php echo $_smarty_tpl->getVariable('view_feedback')->value;?>
</script><div class="contact-form main"><div class="b-center" style="width: 330px; background: rgba(0, 0, 0, .8); padding: 10px 20px;"><div class="b-tabs"><div id="yura" class="b-tab-item contact-person style-v3"  itemscope itemtype="http://schema.org/Person"><h2 class="person-name" style="float: none; width: auto; font-size: 17px;" itemprop="name">Юрий Лысогор<small>(<span itemprop="jobTitle">фотограф</span>, <span itemprop="jobTitle">оператор</span>)</small></h2><div class="person-description" style="width: auto; font-size: 13px; float: none;"><div class="highlighter" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Житомир</span>, <span itemprop="addressLocality"> Киев</span></div><div class="highlighter"><a href="mailto:yura@lysogor.com" itemprop="email">yura@lysogor.com</a></div><div class="highlighter"><a href="http://www.lysogor.com" rel="me nofollow" itemprop="url">http://www.lysogor.com</a><br /><a href="http://lysogor.blogspot.com/" rel="me nofollow" itemprop="url">http://lysogor.blogspot.com/</a></div><div class="highlighter"><span itemprop="telephone">+38(063)603-42-42</span></div></div></div><div id="tanya" class="b-tab-item contact-person style-v3"  itemscope itemtype="http://schema.org/Person"><h2 class="person-name" style="float: none; width: auto; font-size: 17px;" itemprop="name">Татьяна Лысогор<small>(<span itemprop="jobTitle">фотограф</span>)</small></h2><div class="person-description" style="width: auto; font-size: 13px; float: none;"><div class="highlighter" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="addressLocality">Житомир</span>, <span itemprop="addressLocality"> Киев</span></div><div class="highlighter"><a href="mailto:tanya@lysogor.com" itemprop="email">tanya@lysogor.com</a></div><div class="highlighter"><a href="http://www.lysogor.com" rel="me nofollow" itemprop="url">http://www.lysogor.com</a><br /><a href="http://vkontakte.ru/lysogor_tanya" rel="me nofollow" itemprop="url">http://vkontakte.ru/lysogor_tanya</a></div><div class="highlighter"><span itemprop="telephone">+38(050)463-24-24</span></div></div></div><div class="c-form"><?php echo $_smarty_tpl->getVariable('form')->value;?>
</div></div></div></div><?php if (!empty($_smarty_tpl->getVariable('text',null,true,false)->value)){?><a href="javascript: void(0)" class="lysogor-popup-link"><img src="/templates/<?php echo $_smarty_tpl->getVariable('theme')->value;?>
/images/i.png" /></a><div class="lysogor-popup"><a href="javascript: void(0);" class="close">x</a><div class="lysogor-popup-content"><?php echo $_smarty_tpl->getVariable('text')->value;?>
</div></div><?php }?><script type="text/javascript">window.info = true;</script>